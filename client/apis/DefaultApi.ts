// TODO: better import syntax?
import { BaseAPIRequestFactory, RequiredError } from './baseapi';
import {Configuration} from '../configuration';
import { RequestContext, HttpMethod, ResponseContext, HttpFile} from '../http/http';
import {ObjectSerializer} from '../models/ObjectSerializer';
import {ApiException} from './exception';
import {isCodeInRange} from '../util';

import { PlEduAghDomainJourneyJourneysResponse } from '../models/PlEduAghDomainJourneyJourneysResponse';
import { PlEduAghDomainRulesMessagesGetRulesResponse } from '../models/PlEduAghDomainRulesMessagesGetRulesResponse';
import { PlEduAghDomainRulesRules } from '../models/PlEduAghDomainRulesRules';
import { PlEduAghDomainUserOrdersBookCommand } from '../models/PlEduAghDomainUserOrdersBookCommand';
import { PlEduAghDomainUserOrdersPaymentCommand } from '../models/PlEduAghDomainUserOrdersPaymentCommand';
import { PlEduAghDomainUserOrdersUserOrdersResponse } from '../models/PlEduAghDomainUserOrdersUserOrdersResponse';
import { PlEduAghDomainUserPreferencesPreferences } from '../models/PlEduAghDomainUserPreferencesPreferences';
import { PlEduAghDomainUserPreferencesPreferencesCommand } from '../models/PlEduAghDomainUserPreferencesPreferencesCommand';
import { PlEduAghNetworkNullResponse } from '../models/PlEduAghNetworkNullResponse';

/**
 * no description
 */
export class DefaultApiRequestFactory extends BaseAPIRequestFactory {
	
    /**
     */
    public async getRulesGet(options?: Configuration): Promise<RequestContext> {
		let config = options || this.configuration;
		
		// Path Params
    	const localVarPath = '/get_rules';

		// Make Request Context
    	const requestContext = config.baseServer.makeRequestContext(localVarPath, HttpMethod.GET);
        requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8")

        // Query Params
	
		// Header Params
	
		// Form Params


		// Body Params

        let authMethod = null;
        // Apply auth methods
        authMethod = config.authMethods["jwtAuth"]
        if (authMethod) {
            await authMethod.applySecurityAuthentication(requestContext);
        }

        return requestContext;
    }

    /**
     * @param startStation 
     * @param endStation 
     * @param startTimeRange 
     * @param endTimeRange 
     * @param isDirect 
     */
    public async journeyGet(startStation: string, endStation: string, startTimeRange: string, endTimeRange: string, isDirect: boolean, options?: Configuration): Promise<RequestContext> {
		let config = options || this.configuration;
		
        // verify required parameter 'startStation' is not null or undefined
        if (startStation === null || startStation === undefined) {
            throw new RequiredError('Required parameter startStation was null or undefined when calling journeyGet.');
        }

		
        // verify required parameter 'endStation' is not null or undefined
        if (endStation === null || endStation === undefined) {
            throw new RequiredError('Required parameter endStation was null or undefined when calling journeyGet.');
        }

		
        // verify required parameter 'startTimeRange' is not null or undefined
        if (startTimeRange === null || startTimeRange === undefined) {
            throw new RequiredError('Required parameter startTimeRange was null or undefined when calling journeyGet.');
        }

		
        // verify required parameter 'endTimeRange' is not null or undefined
        if (endTimeRange === null || endTimeRange === undefined) {
            throw new RequiredError('Required parameter endTimeRange was null or undefined when calling journeyGet.');
        }

		
        // verify required parameter 'isDirect' is not null or undefined
        if (isDirect === null || isDirect === undefined) {
            throw new RequiredError('Required parameter isDirect was null or undefined when calling journeyGet.');
        }

		
		// Path Params
    	const localVarPath = '/journey';

		// Make Request Context
    	const requestContext = config.baseServer.makeRequestContext(localVarPath, HttpMethod.GET);
        requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8")

        // Query Params
        if (startStation !== undefined) {
        	requestContext.setQueryParam("startStation", ObjectSerializer.serialize(startStation, "string", ""));
        }
        if (endStation !== undefined) {
        	requestContext.setQueryParam("endStation", ObjectSerializer.serialize(endStation, "string", ""));
        }
        if (startTimeRange !== undefined) {
        	requestContext.setQueryParam("startTimeRange", ObjectSerializer.serialize(startTimeRange, "string", ""));
        }
        if (endTimeRange !== undefined) {
        	requestContext.setQueryParam("endTimeRange", ObjectSerializer.serialize(endTimeRange, "string", ""));
        }
        if (isDirect !== undefined) {
        	requestContext.setQueryParam("isDirect", ObjectSerializer.serialize(isDirect, "boolean", ""));
        }
	
		// Header Params
	
		// Form Params


		// Body Params

        let authMethod = null;
        // Apply auth methods
        authMethod = config.authMethods["jwtAuth"]
        if (authMethod) {
            await authMethod.applySecurityAuthentication(requestContext);
        }

        return requestContext;
    }

    /**
     */
    public async preferencesGetGet(options?: Configuration): Promise<RequestContext> {
		let config = options || this.configuration;
		
		// Path Params
    	const localVarPath = '/preferences/get';

		// Make Request Context
    	const requestContext = config.baseServer.makeRequestContext(localVarPath, HttpMethod.GET);
        requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8")

        // Query Params
	
		// Header Params
	
		// Form Params


		// Body Params

        let authMethod = null;
        // Apply auth methods
        authMethod = config.authMethods["jwtAuth"]
        if (authMethod) {
            await authMethod.applySecurityAuthentication(requestContext);
        }

        return requestContext;
    }

    /**
     * @param id 
     * @param plEduAghDomainUserPreferencesPreferencesCommand 
     */
    public async preferencesStorePost(id: string, plEduAghDomainUserPreferencesPreferencesCommand?: PlEduAghDomainUserPreferencesPreferencesCommand, options?: Configuration): Promise<RequestContext> {
		let config = options || this.configuration;
		
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new RequiredError('Required parameter id was null or undefined when calling preferencesStorePost.');
        }

		
		
		// Path Params
    	const localVarPath = '/preferences/store';

		// Make Request Context
    	const requestContext = config.baseServer.makeRequestContext(localVarPath, HttpMethod.POST);
        requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8")

        // Query Params
        if (id !== undefined) {
        	requestContext.setQueryParam("id", ObjectSerializer.serialize(id, "string", ""));
        }
	
		// Header Params
	
		// Form Params


		// Body Params
        const contentType = ObjectSerializer.getPreferredMediaType([
            "application/json"
        ]);
        requestContext.setHeaderParam("Content-Type", contentType);
        const serializedBody = ObjectSerializer.stringify(
            ObjectSerializer.serialize(plEduAghDomainUserPreferencesPreferencesCommand, "PlEduAghDomainUserPreferencesPreferencesCommand", ""),
            contentType
        );
        requestContext.setBody(serializedBody);

        let authMethod = null;
        // Apply auth methods
        authMethod = config.authMethods["jwtAuth"]
        if (authMethod) {
            await authMethod.applySecurityAuthentication(requestContext);
        }

        return requestContext;
    }

    /**
     * @param id 
     */
    public async stationsGet(id: string, options?: Configuration): Promise<RequestContext> {
		let config = options || this.configuration;
		
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new RequiredError('Required parameter id was null or undefined when calling stationsGet.');
        }

		
		// Path Params
    	const localVarPath = '/stations';

		// Make Request Context
    	const requestContext = config.baseServer.makeRequestContext(localVarPath, HttpMethod.GET);
        requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8")

        // Query Params
        if (id !== undefined) {
        	requestContext.setQueryParam("id", ObjectSerializer.serialize(id, "string", ""));
        }
	
		// Header Params
	
		// Form Params


		// Body Params

        let authMethod = null;
        // Apply auth methods
        authMethod = config.authMethods["jwtAuth"]
        if (authMethod) {
            await authMethod.applySecurityAuthentication(requestContext);
        }

        return requestContext;
    }

    /**
     * @param plEduAghDomainRulesRules 
     */
    public async storeRulesPost(plEduAghDomainRulesRules?: PlEduAghDomainRulesRules, options?: Configuration): Promise<RequestContext> {
		let config = options || this.configuration;
		
		
		// Path Params
    	const localVarPath = '/store_rules';

		// Make Request Context
    	const requestContext = config.baseServer.makeRequestContext(localVarPath, HttpMethod.POST);
        requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8")

        // Query Params
	
		// Header Params
	
		// Form Params


		// Body Params
        const contentType = ObjectSerializer.getPreferredMediaType([
            "application/json"
        ]);
        requestContext.setHeaderParam("Content-Type", contentType);
        const serializedBody = ObjectSerializer.stringify(
            ObjectSerializer.serialize(plEduAghDomainRulesRules, "PlEduAghDomainRulesRules", ""),
            contentType
        );
        requestContext.setBody(serializedBody);

        let authMethod = null;
        // Apply auth methods
        authMethod = config.authMethods["jwtAuth"]
        if (authMethod) {
            await authMethod.applySecurityAuthentication(requestContext);
        }

        return requestContext;
    }

    /**
     * @param id 
     * @param plEduAghDomainUserOrdersBookCommand 
     */
    public async userBookPost(id: string, plEduAghDomainUserOrdersBookCommand?: PlEduAghDomainUserOrdersBookCommand, options?: Configuration): Promise<RequestContext> {
		let config = options || this.configuration;
		
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new RequiredError('Required parameter id was null or undefined when calling userBookPost.');
        }

		
		
		// Path Params
    	const localVarPath = '/user/book';

		// Make Request Context
    	const requestContext = config.baseServer.makeRequestContext(localVarPath, HttpMethod.POST);
        requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8")

        // Query Params
        if (id !== undefined) {
        	requestContext.setQueryParam("id", ObjectSerializer.serialize(id, "string", ""));
        }
	
		// Header Params
	
		// Form Params


		// Body Params
        const contentType = ObjectSerializer.getPreferredMediaType([
            "application/json"
        ]);
        requestContext.setHeaderParam("Content-Type", contentType);
        const serializedBody = ObjectSerializer.stringify(
            ObjectSerializer.serialize(plEduAghDomainUserOrdersBookCommand, "PlEduAghDomainUserOrdersBookCommand", ""),
            contentType
        );
        requestContext.setBody(serializedBody);

        let authMethod = null;
        // Apply auth methods
        authMethod = config.authMethods["jwtAuth"]
        if (authMethod) {
            await authMethod.applySecurityAuthentication(requestContext);
        }

        return requestContext;
    }

    /**
     * @param id 
     * @param body 
     */
    public async userCancelPost(id: string, body?: any, options?: Configuration): Promise<RequestContext> {
		let config = options || this.configuration;
		
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new RequiredError('Required parameter id was null or undefined when calling userCancelPost.');
        }

		
		
		// Path Params
    	const localVarPath = '/user/cancel';

		// Make Request Context
    	const requestContext = config.baseServer.makeRequestContext(localVarPath, HttpMethod.POST);
        requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8")

        // Query Params
        if (id !== undefined) {
        	requestContext.setQueryParam("id", ObjectSerializer.serialize(id, "string", ""));
        }
	
		// Header Params
	
		// Form Params


		// Body Params
        const contentType = ObjectSerializer.getPreferredMediaType([
            "application/json"
        ]);
        requestContext.setHeaderParam("Content-Type", contentType);
        const serializedBody = ObjectSerializer.stringify(
            ObjectSerializer.serialize(body, "any", ""),
            contentType
        );
        requestContext.setBody(serializedBody);

        let authMethod = null;
        // Apply auth methods
        authMethod = config.authMethods["jwtAuth"]
        if (authMethod) {
            await authMethod.applySecurityAuthentication(requestContext);
        }

        return requestContext;
    }

    /**
     */
    public async userOrdersGet(options?: Configuration): Promise<RequestContext> {
		let config = options || this.configuration;
		
		// Path Params
    	const localVarPath = '/user/orders';

		// Make Request Context
    	const requestContext = config.baseServer.makeRequestContext(localVarPath, HttpMethod.GET);
        requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8")

        // Query Params
	
		// Header Params
	
		// Form Params


		// Body Params

        let authMethod = null;
        // Apply auth methods
        authMethod = config.authMethods["jwtAuth"]
        if (authMethod) {
            await authMethod.applySecurityAuthentication(requestContext);
        }

        return requestContext;
    }

    /**
     * @param id 
     * @param plEduAghDomainUserOrdersPaymentCommand 
     */
    public async userPaymentPost(id: string, plEduAghDomainUserOrdersPaymentCommand?: PlEduAghDomainUserOrdersPaymentCommand, options?: Configuration): Promise<RequestContext> {
		let config = options || this.configuration;
		
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new RequiredError('Required parameter id was null or undefined when calling userPaymentPost.');
        }

		
		
		// Path Params
    	const localVarPath = '/user/payment';

		// Make Request Context
    	const requestContext = config.baseServer.makeRequestContext(localVarPath, HttpMethod.POST);
        requestContext.setHeaderParam("Accept", "application/json, */*;q=0.8")

        // Query Params
        if (id !== undefined) {
        	requestContext.setQueryParam("id", ObjectSerializer.serialize(id, "string", ""));
        }
	
		// Header Params
	
		// Form Params


		// Body Params
        const contentType = ObjectSerializer.getPreferredMediaType([
            "application/json"
        ]);
        requestContext.setHeaderParam("Content-Type", contentType);
        const serializedBody = ObjectSerializer.stringify(
            ObjectSerializer.serialize(plEduAghDomainUserOrdersPaymentCommand, "PlEduAghDomainUserOrdersPaymentCommand", ""),
            contentType
        );
        requestContext.setBody(serializedBody);

        let authMethod = null;
        // Apply auth methods
        authMethod = config.authMethods["jwtAuth"]
        if (authMethod) {
            await authMethod.applySecurityAuthentication(requestContext);
        }

        return requestContext;
    }

}



export class DefaultApiResponseProcessor {

    /**
     * Unwraps the actual response sent by the server from the response context and deserializes the response content
     * to the expected objects
     *
     * @params response Response returned by the server for a request to getRulesGet
     * @throws ApiException if the response code was not in [200, 299]
     */
     public async getRulesGet(response: ResponseContext): Promise<PlEduAghDomainRulesMessagesGetRulesResponse > {
        const contentType = ObjectSerializer.normalizeMediaType(response.headers["content-type"]);
        if (isCodeInRange("200", response.httpStatusCode)) {
            const body: PlEduAghDomainRulesMessagesGetRulesResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghDomainRulesMessagesGetRulesResponse", ""
            ) as PlEduAghDomainRulesMessagesGetRulesResponse;
            return body;
        }

        // Work around for missing responses in specification, e.g. for petstore.yaml
        if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
            const body: PlEduAghDomainRulesMessagesGetRulesResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghDomainRulesMessagesGetRulesResponse", ""
            ) as PlEduAghDomainRulesMessagesGetRulesResponse;
            return body;
        }

        let body = response.body || "";
    	throw new ApiException<string>(response.httpStatusCode, "Unknown API Status Code!\nBody: \"" + body + "\"");
    }
			
    /**
     * Unwraps the actual response sent by the server from the response context and deserializes the response content
     * to the expected objects
     *
     * @params response Response returned by the server for a request to journeyGet
     * @throws ApiException if the response code was not in [200, 299]
     */
     public async journeyGet(response: ResponseContext): Promise<PlEduAghDomainJourneyJourneysResponse > {
        const contentType = ObjectSerializer.normalizeMediaType(response.headers["content-type"]);
        if (isCodeInRange("400", response.httpStatusCode)) {
            throw new ApiException<string>(response.httpStatusCode, "Bad Request");
        }
        if (isCodeInRange("200", response.httpStatusCode)) {
            const body: PlEduAghDomainJourneyJourneysResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghDomainJourneyJourneysResponse", ""
            ) as PlEduAghDomainJourneyJourneysResponse;
            return body;
        }

        // Work around for missing responses in specification, e.g. for petstore.yaml
        if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
            const body: PlEduAghDomainJourneyJourneysResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghDomainJourneyJourneysResponse", ""
            ) as PlEduAghDomainJourneyJourneysResponse;
            return body;
        }

        let body = response.body || "";
    	throw new ApiException<string>(response.httpStatusCode, "Unknown API Status Code!\nBody: \"" + body + "\"");
    }
			
    /**
     * Unwraps the actual response sent by the server from the response context and deserializes the response content
     * to the expected objects
     *
     * @params response Response returned by the server for a request to preferencesGetGet
     * @throws ApiException if the response code was not in [200, 299]
     */
     public async preferencesGetGet(response: ResponseContext): Promise<PlEduAghDomainUserPreferencesPreferences > {
        const contentType = ObjectSerializer.normalizeMediaType(response.headers["content-type"]);
        if (isCodeInRange("200", response.httpStatusCode)) {
            const body: PlEduAghDomainUserPreferencesPreferences = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghDomainUserPreferencesPreferences", ""
            ) as PlEduAghDomainUserPreferencesPreferences;
            return body;
        }

        // Work around for missing responses in specification, e.g. for petstore.yaml
        if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
            const body: PlEduAghDomainUserPreferencesPreferences = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghDomainUserPreferencesPreferences", ""
            ) as PlEduAghDomainUserPreferencesPreferences;
            return body;
        }

        let body = response.body || "";
    	throw new ApiException<string>(response.httpStatusCode, "Unknown API Status Code!\nBody: \"" + body + "\"");
    }
			
    /**
     * Unwraps the actual response sent by the server from the response context and deserializes the response content
     * to the expected objects
     *
     * @params response Response returned by the server for a request to preferencesStorePost
     * @throws ApiException if the response code was not in [200, 299]
     */
     public async preferencesStorePost(response: ResponseContext): Promise<PlEduAghNetworkNullResponse > {
        const contentType = ObjectSerializer.normalizeMediaType(response.headers["content-type"]);
        if (isCodeInRange("200", response.httpStatusCode)) {
            const body: PlEduAghNetworkNullResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghNetworkNullResponse", ""
            ) as PlEduAghNetworkNullResponse;
            return body;
        }

        // Work around for missing responses in specification, e.g. for petstore.yaml
        if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
            const body: PlEduAghNetworkNullResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghNetworkNullResponse", ""
            ) as PlEduAghNetworkNullResponse;
            return body;
        }

        let body = response.body || "";
    	throw new ApiException<string>(response.httpStatusCode, "Unknown API Status Code!\nBody: \"" + body + "\"");
    }
			
    /**
     * Unwraps the actual response sent by the server from the response context and deserializes the response content
     * to the expected objects
     *
     * @params response Response returned by the server for a request to stationsGet
     * @throws ApiException if the response code was not in [200, 299]
     */
     public async stationsGet(response: ResponseContext): Promise<Array<string> > {
        const contentType = ObjectSerializer.normalizeMediaType(response.headers["content-type"]);
        if (isCodeInRange("200", response.httpStatusCode)) {
            const body: Array<string> = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "Array<string>", ""
            ) as Array<string>;
            return body;
        }

        // Work around for missing responses in specification, e.g. for petstore.yaml
        if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
            const body: Array<string> = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "Array<string>", ""
            ) as Array<string>;
            return body;
        }

        let body = response.body || "";
    	throw new ApiException<string>(response.httpStatusCode, "Unknown API Status Code!\nBody: \"" + body + "\"");
    }
			
    /**
     * Unwraps the actual response sent by the server from the response context and deserializes the response content
     * to the expected objects
     *
     * @params response Response returned by the server for a request to storeRulesPost
     * @throws ApiException if the response code was not in [200, 299]
     */
     public async storeRulesPost(response: ResponseContext): Promise<any > {
        const contentType = ObjectSerializer.normalizeMediaType(response.headers["content-type"]);
        if (isCodeInRange("200", response.httpStatusCode)) {
            const body: any = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "any", ""
            ) as any;
            return body;
        }

        // Work around for missing responses in specification, e.g. for petstore.yaml
        if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
            const body: any = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "any", ""
            ) as any;
            return body;
        }

        let body = response.body || "";
    	throw new ApiException<string>(response.httpStatusCode, "Unknown API Status Code!\nBody: \"" + body + "\"");
    }
			
    /**
     * Unwraps the actual response sent by the server from the response context and deserializes the response content
     * to the expected objects
     *
     * @params response Response returned by the server for a request to userBookPost
     * @throws ApiException if the response code was not in [200, 299]
     */
     public async userBookPost(response: ResponseContext): Promise<PlEduAghDomainUserOrdersUserOrdersResponse > {
        const contentType = ObjectSerializer.normalizeMediaType(response.headers["content-type"]);
        if (isCodeInRange("200", response.httpStatusCode)) {
            const body: PlEduAghDomainUserOrdersUserOrdersResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghDomainUserOrdersUserOrdersResponse", ""
            ) as PlEduAghDomainUserOrdersUserOrdersResponse;
            return body;
        }

        // Work around for missing responses in specification, e.g. for petstore.yaml
        if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
            const body: PlEduAghDomainUserOrdersUserOrdersResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghDomainUserOrdersUserOrdersResponse", ""
            ) as PlEduAghDomainUserOrdersUserOrdersResponse;
            return body;
        }

        let body = response.body || "";
    	throw new ApiException<string>(response.httpStatusCode, "Unknown API Status Code!\nBody: \"" + body + "\"");
    }
			
    /**
     * Unwraps the actual response sent by the server from the response context and deserializes the response content
     * to the expected objects
     *
     * @params response Response returned by the server for a request to userCancelPost
     * @throws ApiException if the response code was not in [200, 299]
     */
     public async userCancelPost(response: ResponseContext): Promise<PlEduAghNetworkNullResponse > {
        const contentType = ObjectSerializer.normalizeMediaType(response.headers["content-type"]);
        if (isCodeInRange("200", response.httpStatusCode)) {
            const body: PlEduAghNetworkNullResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghNetworkNullResponse", ""
            ) as PlEduAghNetworkNullResponse;
            return body;
        }

        // Work around for missing responses in specification, e.g. for petstore.yaml
        if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
            const body: PlEduAghNetworkNullResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghNetworkNullResponse", ""
            ) as PlEduAghNetworkNullResponse;
            return body;
        }

        let body = response.body || "";
    	throw new ApiException<string>(response.httpStatusCode, "Unknown API Status Code!\nBody: \"" + body + "\"");
    }
			
    /**
     * Unwraps the actual response sent by the server from the response context and deserializes the response content
     * to the expected objects
     *
     * @params response Response returned by the server for a request to userOrdersGet
     * @throws ApiException if the response code was not in [200, 299]
     */
     public async userOrdersGet(response: ResponseContext): Promise<PlEduAghDomainUserOrdersUserOrdersResponse > {
        const contentType = ObjectSerializer.normalizeMediaType(response.headers["content-type"]);
        if (isCodeInRange("200", response.httpStatusCode)) {
            const body: PlEduAghDomainUserOrdersUserOrdersResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghDomainUserOrdersUserOrdersResponse", ""
            ) as PlEduAghDomainUserOrdersUserOrdersResponse;
            return body;
        }

        // Work around for missing responses in specification, e.g. for petstore.yaml
        if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
            const body: PlEduAghDomainUserOrdersUserOrdersResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghDomainUserOrdersUserOrdersResponse", ""
            ) as PlEduAghDomainUserOrdersUserOrdersResponse;
            return body;
        }

        let body = response.body || "";
    	throw new ApiException<string>(response.httpStatusCode, "Unknown API Status Code!\nBody: \"" + body + "\"");
    }
			
    /**
     * Unwraps the actual response sent by the server from the response context and deserializes the response content
     * to the expected objects
     *
     * @params response Response returned by the server for a request to userPaymentPost
     * @throws ApiException if the response code was not in [200, 299]
     */
     public async userPaymentPost(response: ResponseContext): Promise<PlEduAghNetworkNullResponse > {
        const contentType = ObjectSerializer.normalizeMediaType(response.headers["content-type"]);
        if (isCodeInRange("200", response.httpStatusCode)) {
            const body: PlEduAghNetworkNullResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghNetworkNullResponse", ""
            ) as PlEduAghNetworkNullResponse;
            return body;
        }

        // Work around for missing responses in specification, e.g. for petstore.yaml
        if (response.httpStatusCode >= 200 && response.httpStatusCode <= 299) {
            const body: PlEduAghNetworkNullResponse = ObjectSerializer.deserialize(
                ObjectSerializer.parse(await response.body.text(), contentType),
                "PlEduAghNetworkNullResponse", ""
            ) as PlEduAghNetworkNullResponse;
            return body;
        }

        let body = response.body || "";
    	throw new ApiException<string>(response.httpStatusCode, "Unknown API Status Code!\nBody: \"" + body + "\"");
    }
			
}
