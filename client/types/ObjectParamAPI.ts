import { ResponseContext, RequestContext, HttpFile } from '../http/http';
import * as models from '../models/all';
import { Configuration} from '../configuration'

import { DomainTrainTrain } from '../models/DomainTrainTrain';
import { PlEduAghDomainHistoricalInfo } from '../models/PlEduAghDomainHistoricalInfo';
import { PlEduAghDomainJourneyBookedSeatDto } from '../models/PlEduAghDomainJourneyBookedSeatDto';
import { PlEduAghDomainJourneyJourneyLegDto } from '../models/PlEduAghDomainJourneyJourneyLegDto';
import { PlEduAghDomainJourneyJourneyResponse } from '../models/PlEduAghDomainJourneyJourneyResponse';
import { PlEduAghDomainJourneyJourneysResponse } from '../models/PlEduAghDomainJourneyJourneysResponse';
import { PlEduAghDomainJourneyLegStopDto } from '../models/PlEduAghDomainJourneyLegStopDto';
import { PlEduAghDomainRaptorSegment } from '../models/PlEduAghDomainRaptorSegment';
import { PlEduAghDomainRulesGlobalRule } from '../models/PlEduAghDomainRulesGlobalRule';
import { PlEduAghDomainRulesGlobalRuleDisabledStop } from '../models/PlEduAghDomainRulesGlobalRuleDisabledStop';
import { PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled } from '../models/PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled';
import { PlEduAghDomainRulesGlobalRuleOnePersonInCompartment } from '../models/PlEduAghDomainRulesGlobalRuleOnePersonInCompartment';
import { PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled } from '../models/PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled';
import { PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled } from '../models/PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled';
import { PlEduAghDomainRulesGlobalRuleSpaceBetween } from '../models/PlEduAghDomainRulesGlobalRuleSpaceBetween';
import { PlEduAghDomainRulesMessagesGetRulesResponse } from '../models/PlEduAghDomainRulesMessagesGetRulesResponse';
import { PlEduAghDomainRulesRules } from '../models/PlEduAghDomainRulesRules';
import { PlEduAghDomainRulesTrainTrainRule } from '../models/PlEduAghDomainRulesTrainTrainRule';
import { PlEduAghDomainRulesTrainTrainRuleDisabledSeats } from '../models/PlEduAghDomainRulesTrainTrainRuleDisabledSeats';
import { PlEduAghDomainTrainBitmapCoord } from '../models/PlEduAghDomainTrainBitmapCoord';
import { PlEduAghDomainTrainBitmapField } from '../models/PlEduAghDomainTrainBitmapField';
import { PlEduAghDomainTrainBitmapFieldCorridor } from '../models/PlEduAghDomainTrainBitmapFieldCorridor';
import { PlEduAghDomainTrainBitmapFieldSeat } from '../models/PlEduAghDomainTrainBitmapFieldSeat';
import { PlEduAghDomainTrainBitmapFieldUnknownField } from '../models/PlEduAghDomainTrainBitmapFieldUnknownField';
import { PlEduAghDomainTrainBitmapFieldWall } from '../models/PlEduAghDomainTrainBitmapFieldWall';
import { PlEduAghDomainTrainBitmapFieldWindow } from '../models/PlEduAghDomainTrainBitmapFieldWindow';
import { PlEduAghDomainTrainCarCar } from '../models/PlEduAghDomainTrainCarCar';
import { PlEduAghDomainUserOrdersBookCommand } from '../models/PlEduAghDomainUserOrdersBookCommand';
import { PlEduAghDomainUserOrdersPaymentCommand } from '../models/PlEduAghDomainUserOrdersPaymentCommand';
import { PlEduAghDomainUserOrdersUserOrderResponse } from '../models/PlEduAghDomainUserOrdersUserOrderResponse';
import { PlEduAghDomainUserOrdersUserOrdersResponse } from '../models/PlEduAghDomainUserOrdersUserOrdersResponse';
import { PlEduAghDomainUserPreferencesPreferences } from '../models/PlEduAghDomainUserPreferencesPreferences';
import { PlEduAghDomainUserPreferencesPreferencesCommand } from '../models/PlEduAghDomainUserPreferencesPreferencesCommand';
import { PlEduAghDomainUserPreferencesWithoutTransfers } from '../models/PlEduAghDomainUserPreferencesWithoutTransfers';
import { PlEduAghNetworkNullResponse } from '../models/PlEduAghNetworkNullResponse';

import { ObservableDefaultApi } from "./ObservableAPI";
import { DefaultApiRequestFactory, DefaultApiResponseProcessor} from "../apis/DefaultApi";

export interface DefaultApiGetRulesGetRequest {
}

export interface DefaultApiJourneyGetRequest {
    /**
     * 
     * @type string
     * @memberof DefaultApijourneyGet
     */
    startStation: string
    /**
     * 
     * @type string
     * @memberof DefaultApijourneyGet
     */
    endStation: string
    /**
     * 
     * @type string
     * @memberof DefaultApijourneyGet
     */
    startTimeRange: string
    /**
     * 
     * @type string
     * @memberof DefaultApijourneyGet
     */
    endTimeRange: string
    /**
     * 
     * @type boolean
     * @memberof DefaultApijourneyGet
     */
    isDirect: boolean
}

export interface DefaultApiPreferencesGetGetRequest {
}

export interface DefaultApiPreferencesStorePostRequest {
    /**
     * 
     * @type string
     * @memberof DefaultApipreferencesStorePost
     */
    id: string
    /**
     * 
     * @type PlEduAghDomainUserPreferencesPreferencesCommand
     * @memberof DefaultApipreferencesStorePost
     */
    plEduAghDomainUserPreferencesPreferencesCommand?: PlEduAghDomainUserPreferencesPreferencesCommand
}

export interface DefaultApiStationsGetRequest {
    /**
     * 
     * @type string
     * @memberof DefaultApistationsGet
     */
    id: string
}

export interface DefaultApiStoreRulesPostRequest {
    /**
     * 
     * @type PlEduAghDomainRulesRules
     * @memberof DefaultApistoreRulesPost
     */
    plEduAghDomainRulesRules?: PlEduAghDomainRulesRules
}

export interface DefaultApiUserBookPostRequest {
    /**
     * 
     * @type string
     * @memberof DefaultApiuserBookPost
     */
    id: string
    /**
     * 
     * @type PlEduAghDomainUserOrdersBookCommand
     * @memberof DefaultApiuserBookPost
     */
    plEduAghDomainUserOrdersBookCommand?: PlEduAghDomainUserOrdersBookCommand
}

export interface DefaultApiUserCancelPostRequest {
    /**
     * 
     * @type string
     * @memberof DefaultApiuserCancelPost
     */
    id: string
    /**
     * 
     * @type any
     * @memberof DefaultApiuserCancelPost
     */
    body?: any
}

export interface DefaultApiUserOrdersGetRequest {
}

export interface DefaultApiUserPaymentPostRequest {
    /**
     * 
     * @type string
     * @memberof DefaultApiuserPaymentPost
     */
    id: string
    /**
     * 
     * @type PlEduAghDomainUserOrdersPaymentCommand
     * @memberof DefaultApiuserPaymentPost
     */
    plEduAghDomainUserOrdersPaymentCommand?: PlEduAghDomainUserOrdersPaymentCommand
}


export class ObjectDefaultApi {
    private api: ObservableDefaultApi

    public constructor(configuration: Configuration, requestFactory?: DefaultApiRequestFactory, responseProcessor?: DefaultApiResponseProcessor) {
        this.api = new ObservableDefaultApi(configuration, requestFactory, responseProcessor);
	}

    /**
     * @param param the request object
     */
    public getRulesGet(param: DefaultApiGetRulesGetRequest, options?: Configuration): Promise<PlEduAghDomainRulesMessagesGetRulesResponse> {
        return this.api.getRulesGet( options).toPromise();
    }
	
    /**
     * @param param the request object
     */
    public journeyGet(param: DefaultApiJourneyGetRequest, options?: Configuration): Promise<PlEduAghDomainJourneyJourneysResponse> {
        return this.api.journeyGet(param.startStation, param.endStation, param.startTimeRange, param.endTimeRange, param.isDirect,  options).toPromise();
    }
	
    /**
     * @param param the request object
     */
    public preferencesGetGet(param: DefaultApiPreferencesGetGetRequest, options?: Configuration): Promise<PlEduAghDomainUserPreferencesPreferences> {
        return this.api.preferencesGetGet( options).toPromise();
    }
	
    /**
     * @param param the request object
     */
    public preferencesStorePost(param: DefaultApiPreferencesStorePostRequest, options?: Configuration): Promise<PlEduAghNetworkNullResponse> {
        return this.api.preferencesStorePost(param.id, param.plEduAghDomainUserPreferencesPreferencesCommand,  options).toPromise();
    }
	
    /**
     * @param param the request object
     */
    public stationsGet(param: DefaultApiStationsGetRequest, options?: Configuration): Promise<Array<string>> {
        return this.api.stationsGet(param.id,  options).toPromise();
    }
	
    /**
     * @param param the request object
     */
    public storeRulesPost(param: DefaultApiStoreRulesPostRequest, options?: Configuration): Promise<any> {
        return this.api.storeRulesPost(param.plEduAghDomainRulesRules,  options).toPromise();
    }
	
    /**
     * @param param the request object
     */
    public userBookPost(param: DefaultApiUserBookPostRequest, options?: Configuration): Promise<PlEduAghDomainUserOrdersUserOrdersResponse> {
        return this.api.userBookPost(param.id, param.plEduAghDomainUserOrdersBookCommand,  options).toPromise();
    }
	
    /**
     * @param param the request object
     */
    public userCancelPost(param: DefaultApiUserCancelPostRequest, options?: Configuration): Promise<PlEduAghNetworkNullResponse> {
        return this.api.userCancelPost(param.id, param.body,  options).toPromise();
    }
	
    /**
     * @param param the request object
     */
    public userOrdersGet(param: DefaultApiUserOrdersGetRequest, options?: Configuration): Promise<PlEduAghDomainUserOrdersUserOrdersResponse> {
        return this.api.userOrdersGet( options).toPromise();
    }
	
    /**
     * @param param the request object
     */
    public userPaymentPost(param: DefaultApiUserPaymentPostRequest, options?: Configuration): Promise<PlEduAghNetworkNullResponse> {
        return this.api.userPaymentPost(param.id, param.plEduAghDomainUserOrdersPaymentCommand,  options).toPromise();
    }
	

}



