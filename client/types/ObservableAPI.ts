import { ResponseContext, RequestContext, HttpFile } from '../http/http';
import * as models from '../models/all';
import { Configuration} from '../configuration'
import { Observable, of, from } from '../rxjsStub';
import {mergeMap, map} from  '../rxjsStub';

import { DomainTrainTrain } from '../models/DomainTrainTrain';
import { PlEduAghDomainHistoricalInfo } from '../models/PlEduAghDomainHistoricalInfo';
import { PlEduAghDomainJourneyBookedSeatDto } from '../models/PlEduAghDomainJourneyBookedSeatDto';
import { PlEduAghDomainJourneyJourneyLegDto } from '../models/PlEduAghDomainJourneyJourneyLegDto';
import { PlEduAghDomainJourneyJourneyResponse } from '../models/PlEduAghDomainJourneyJourneyResponse';
import { PlEduAghDomainJourneyJourneysResponse } from '../models/PlEduAghDomainJourneyJourneysResponse';
import { PlEduAghDomainJourneyLegStopDto } from '../models/PlEduAghDomainJourneyLegStopDto';
import { PlEduAghDomainRaptorSegment } from '../models/PlEduAghDomainRaptorSegment';
import { PlEduAghDomainRulesGlobalRule } from '../models/PlEduAghDomainRulesGlobalRule';
import { PlEduAghDomainRulesGlobalRuleDisabledStop } from '../models/PlEduAghDomainRulesGlobalRuleDisabledStop';
import { PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled } from '../models/PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled';
import { PlEduAghDomainRulesGlobalRuleOnePersonInCompartment } from '../models/PlEduAghDomainRulesGlobalRuleOnePersonInCompartment';
import { PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled } from '../models/PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled';
import { PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled } from '../models/PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled';
import { PlEduAghDomainRulesGlobalRuleSpaceBetween } from '../models/PlEduAghDomainRulesGlobalRuleSpaceBetween';
import { PlEduAghDomainRulesMessagesGetRulesResponse } from '../models/PlEduAghDomainRulesMessagesGetRulesResponse';
import { PlEduAghDomainRulesRules } from '../models/PlEduAghDomainRulesRules';
import { PlEduAghDomainRulesTrainTrainRule } from '../models/PlEduAghDomainRulesTrainTrainRule';
import { PlEduAghDomainRulesTrainTrainRuleDisabledSeats } from '../models/PlEduAghDomainRulesTrainTrainRuleDisabledSeats';
import { PlEduAghDomainTrainBitmapCoord } from '../models/PlEduAghDomainTrainBitmapCoord';
import { PlEduAghDomainTrainBitmapField } from '../models/PlEduAghDomainTrainBitmapField';
import { PlEduAghDomainTrainBitmapFieldCorridor } from '../models/PlEduAghDomainTrainBitmapFieldCorridor';
import { PlEduAghDomainTrainBitmapFieldSeat } from '../models/PlEduAghDomainTrainBitmapFieldSeat';
import { PlEduAghDomainTrainBitmapFieldUnknownField } from '../models/PlEduAghDomainTrainBitmapFieldUnknownField';
import { PlEduAghDomainTrainBitmapFieldWall } from '../models/PlEduAghDomainTrainBitmapFieldWall';
import { PlEduAghDomainTrainBitmapFieldWindow } from '../models/PlEduAghDomainTrainBitmapFieldWindow';
import { PlEduAghDomainTrainCarCar } from '../models/PlEduAghDomainTrainCarCar';
import { PlEduAghDomainUserOrdersBookCommand } from '../models/PlEduAghDomainUserOrdersBookCommand';
import { PlEduAghDomainUserOrdersPaymentCommand } from '../models/PlEduAghDomainUserOrdersPaymentCommand';
import { PlEduAghDomainUserOrdersUserOrderResponse } from '../models/PlEduAghDomainUserOrdersUserOrderResponse';
import { PlEduAghDomainUserOrdersUserOrdersResponse } from '../models/PlEduAghDomainUserOrdersUserOrdersResponse';
import { PlEduAghDomainUserPreferencesPreferences } from '../models/PlEduAghDomainUserPreferencesPreferences';
import { PlEduAghDomainUserPreferencesPreferencesCommand } from '../models/PlEduAghDomainUserPreferencesPreferencesCommand';
import { PlEduAghDomainUserPreferencesWithoutTransfers } from '../models/PlEduAghDomainUserPreferencesWithoutTransfers';
import { PlEduAghNetworkNullResponse } from '../models/PlEduAghNetworkNullResponse';

import { DefaultApiRequestFactory, DefaultApiResponseProcessor} from "../apis/DefaultApi";
export class ObservableDefaultApi {
    private requestFactory: DefaultApiRequestFactory;
    private responseProcessor: DefaultApiResponseProcessor;
    private configuration: Configuration;

    public constructor(
        configuration: Configuration,
        requestFactory?: DefaultApiRequestFactory,
        responseProcessor?: DefaultApiResponseProcessor
    ) {
        this.configuration = configuration;
        this.requestFactory = requestFactory || new DefaultApiRequestFactory(configuration);
        this.responseProcessor = responseProcessor || new DefaultApiResponseProcessor();
    }

    /**
     */
    public getRulesGet(options?: Configuration): Observable<PlEduAghDomainRulesMessagesGetRulesResponse> {
    	const requestContextPromise = this.requestFactory.getRulesGet(options);

		// build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    	for (let middleware of this.configuration.middleware) {
    		middlewarePreObservable = middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => middleware.pre(ctx)));
    	}

    	return middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))).
	    	pipe(mergeMap((response: ResponseContext) => {
	    		let middlewarePostObservable = of(response);
	    		for (let middleware of this.configuration.middleware) {
	    			middlewarePostObservable = middlewarePostObservable.pipe(mergeMap((rsp: ResponseContext) => middleware.post(rsp)));
	    		}
	    		return middlewarePostObservable.pipe(map((rsp: ResponseContext) => this.responseProcessor.getRulesGet(rsp)));
	    	}));
    }
	
    /**
     * @param startStation 
     * @param endStation 
     * @param startTimeRange 
     * @param endTimeRange 
     * @param isDirect 
     */
    public journeyGet(startStation: string, endStation: string, startTimeRange: string, endTimeRange: string, isDirect: boolean, options?: Configuration): Observable<PlEduAghDomainJourneyJourneysResponse> {
    	const requestContextPromise = this.requestFactory.journeyGet(startStation, endStation, startTimeRange, endTimeRange, isDirect, options);

		// build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    	for (let middleware of this.configuration.middleware) {
    		middlewarePreObservable = middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => middleware.pre(ctx)));
    	}

    	return middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))).
	    	pipe(mergeMap((response: ResponseContext) => {
	    		let middlewarePostObservable = of(response);
	    		for (let middleware of this.configuration.middleware) {
	    			middlewarePostObservable = middlewarePostObservable.pipe(mergeMap((rsp: ResponseContext) => middleware.post(rsp)));
	    		}
	    		return middlewarePostObservable.pipe(map((rsp: ResponseContext) => this.responseProcessor.journeyGet(rsp)));
	    	}));
    }
	
    /**
     */
    public preferencesGetGet(options?: Configuration): Observable<PlEduAghDomainUserPreferencesPreferences> {
    	const requestContextPromise = this.requestFactory.preferencesGetGet(options);

		// build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    	for (let middleware of this.configuration.middleware) {
    		middlewarePreObservable = middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => middleware.pre(ctx)));
    	}

    	return middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))).
	    	pipe(mergeMap((response: ResponseContext) => {
	    		let middlewarePostObservable = of(response);
	    		for (let middleware of this.configuration.middleware) {
	    			middlewarePostObservable = middlewarePostObservable.pipe(mergeMap((rsp: ResponseContext) => middleware.post(rsp)));
	    		}
	    		return middlewarePostObservable.pipe(map((rsp: ResponseContext) => this.responseProcessor.preferencesGetGet(rsp)));
	    	}));
    }
	
    /**
     * @param id 
     * @param plEduAghDomainUserPreferencesPreferencesCommand 
     */
    public preferencesStorePost(id: string, plEduAghDomainUserPreferencesPreferencesCommand?: PlEduAghDomainUserPreferencesPreferencesCommand, options?: Configuration): Observable<PlEduAghNetworkNullResponse> {
    	const requestContextPromise = this.requestFactory.preferencesStorePost(id, plEduAghDomainUserPreferencesPreferencesCommand, options);

		// build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    	for (let middleware of this.configuration.middleware) {
    		middlewarePreObservable = middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => middleware.pre(ctx)));
    	}

    	return middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))).
	    	pipe(mergeMap((response: ResponseContext) => {
	    		let middlewarePostObservable = of(response);
	    		for (let middleware of this.configuration.middleware) {
	    			middlewarePostObservable = middlewarePostObservable.pipe(mergeMap((rsp: ResponseContext) => middleware.post(rsp)));
	    		}
	    		return middlewarePostObservable.pipe(map((rsp: ResponseContext) => this.responseProcessor.preferencesStorePost(rsp)));
	    	}));
    }
	
    /**
     * @param id 
     */
    public stationsGet(id: string, options?: Configuration): Observable<Array<string>> {
    	const requestContextPromise = this.requestFactory.stationsGet(id, options);

		// build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    	for (let middleware of this.configuration.middleware) {
    		middlewarePreObservable = middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => middleware.pre(ctx)));
    	}

    	return middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))).
	    	pipe(mergeMap((response: ResponseContext) => {
	    		let middlewarePostObservable = of(response);
	    		for (let middleware of this.configuration.middleware) {
	    			middlewarePostObservable = middlewarePostObservable.pipe(mergeMap((rsp: ResponseContext) => middleware.post(rsp)));
	    		}
	    		return middlewarePostObservable.pipe(map((rsp: ResponseContext) => this.responseProcessor.stationsGet(rsp)));
	    	}));
    }
	
    /**
     * @param plEduAghDomainRulesRules 
     */
    public storeRulesPost(plEduAghDomainRulesRules?: PlEduAghDomainRulesRules, options?: Configuration): Observable<any> {
    	const requestContextPromise = this.requestFactory.storeRulesPost(plEduAghDomainRulesRules, options);

		// build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    	for (let middleware of this.configuration.middleware) {
    		middlewarePreObservable = middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => middleware.pre(ctx)));
    	}

    	return middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))).
	    	pipe(mergeMap((response: ResponseContext) => {
	    		let middlewarePostObservable = of(response);
	    		for (let middleware of this.configuration.middleware) {
	    			middlewarePostObservable = middlewarePostObservable.pipe(mergeMap((rsp: ResponseContext) => middleware.post(rsp)));
	    		}
	    		return middlewarePostObservable.pipe(map((rsp: ResponseContext) => this.responseProcessor.storeRulesPost(rsp)));
	    	}));
    }
	
    /**
     * @param id 
     * @param plEduAghDomainUserOrdersBookCommand 
     */
    public userBookPost(id: string, plEduAghDomainUserOrdersBookCommand?: PlEduAghDomainUserOrdersBookCommand, options?: Configuration): Observable<PlEduAghDomainUserOrdersUserOrdersResponse> {
    	const requestContextPromise = this.requestFactory.userBookPost(id, plEduAghDomainUserOrdersBookCommand, options);

		// build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    	for (let middleware of this.configuration.middleware) {
    		middlewarePreObservable = middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => middleware.pre(ctx)));
    	}

    	return middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))).
	    	pipe(mergeMap((response: ResponseContext) => {
	    		let middlewarePostObservable = of(response);
	    		for (let middleware of this.configuration.middleware) {
	    			middlewarePostObservable = middlewarePostObservable.pipe(mergeMap((rsp: ResponseContext) => middleware.post(rsp)));
	    		}
	    		return middlewarePostObservable.pipe(map((rsp: ResponseContext) => this.responseProcessor.userBookPost(rsp)));
	    	}));
    }
	
    /**
     * @param id 
     * @param body 
     */
    public userCancelPost(id: string, body?: any, options?: Configuration): Observable<PlEduAghNetworkNullResponse> {
    	const requestContextPromise = this.requestFactory.userCancelPost(id, body, options);

		// build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    	for (let middleware of this.configuration.middleware) {
    		middlewarePreObservable = middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => middleware.pre(ctx)));
    	}

    	return middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))).
	    	pipe(mergeMap((response: ResponseContext) => {
	    		let middlewarePostObservable = of(response);
	    		for (let middleware of this.configuration.middleware) {
	    			middlewarePostObservable = middlewarePostObservable.pipe(mergeMap((rsp: ResponseContext) => middleware.post(rsp)));
	    		}
	    		return middlewarePostObservable.pipe(map((rsp: ResponseContext) => this.responseProcessor.userCancelPost(rsp)));
	    	}));
    }
	
    /**
     */
    public userOrdersGet(options?: Configuration): Observable<PlEduAghDomainUserOrdersUserOrdersResponse> {
    	const requestContextPromise = this.requestFactory.userOrdersGet(options);

		// build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    	for (let middleware of this.configuration.middleware) {
    		middlewarePreObservable = middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => middleware.pre(ctx)));
    	}

    	return middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))).
	    	pipe(mergeMap((response: ResponseContext) => {
	    		let middlewarePostObservable = of(response);
	    		for (let middleware of this.configuration.middleware) {
	    			middlewarePostObservable = middlewarePostObservable.pipe(mergeMap((rsp: ResponseContext) => middleware.post(rsp)));
	    		}
	    		return middlewarePostObservable.pipe(map((rsp: ResponseContext) => this.responseProcessor.userOrdersGet(rsp)));
	    	}));
    }
	
    /**
     * @param id 
     * @param plEduAghDomainUserOrdersPaymentCommand 
     */
    public userPaymentPost(id: string, plEduAghDomainUserOrdersPaymentCommand?: PlEduAghDomainUserOrdersPaymentCommand, options?: Configuration): Observable<PlEduAghNetworkNullResponse> {
    	const requestContextPromise = this.requestFactory.userPaymentPost(id, plEduAghDomainUserOrdersPaymentCommand, options);

		// build promise chain
    let middlewarePreObservable = from<RequestContext>(requestContextPromise);
    	for (let middleware of this.configuration.middleware) {
    		middlewarePreObservable = middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => middleware.pre(ctx)));
    	}

    	return middlewarePreObservable.pipe(mergeMap((ctx: RequestContext) => this.configuration.httpApi.send(ctx))).
	    	pipe(mergeMap((response: ResponseContext) => {
	    		let middlewarePostObservable = of(response);
	    		for (let middleware of this.configuration.middleware) {
	    			middlewarePostObservable = middlewarePostObservable.pipe(mergeMap((rsp: ResponseContext) => middleware.post(rsp)));
	    		}
	    		return middlewarePostObservable.pipe(map((rsp: ResponseContext) => this.responseProcessor.userPaymentPost(rsp)));
	    	}));
    }
	

}



