import { ResponseContext, RequestContext, HttpFile } from '../http/http';
import * as models from '../models/all';
import { Configuration} from '../configuration'

import { DomainTrainTrain } from '../models/DomainTrainTrain';
import { PlEduAghDomainHistoricalInfo } from '../models/PlEduAghDomainHistoricalInfo';
import { PlEduAghDomainJourneyBookedSeatDto } from '../models/PlEduAghDomainJourneyBookedSeatDto';
import { PlEduAghDomainJourneyJourneyLegDto } from '../models/PlEduAghDomainJourneyJourneyLegDto';
import { PlEduAghDomainJourneyJourneyResponse } from '../models/PlEduAghDomainJourneyJourneyResponse';
import { PlEduAghDomainJourneyJourneysResponse } from '../models/PlEduAghDomainJourneyJourneysResponse';
import { PlEduAghDomainJourneyLegStopDto } from '../models/PlEduAghDomainJourneyLegStopDto';
import { PlEduAghDomainRaptorSegment } from '../models/PlEduAghDomainRaptorSegment';
import { PlEduAghDomainRulesGlobalRule } from '../models/PlEduAghDomainRulesGlobalRule';
import { PlEduAghDomainRulesGlobalRuleDisabledStop } from '../models/PlEduAghDomainRulesGlobalRuleDisabledStop';
import { PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled } from '../models/PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled';
import { PlEduAghDomainRulesGlobalRuleOnePersonInCompartment } from '../models/PlEduAghDomainRulesGlobalRuleOnePersonInCompartment';
import { PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled } from '../models/PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled';
import { PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled } from '../models/PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled';
import { PlEduAghDomainRulesGlobalRuleSpaceBetween } from '../models/PlEduAghDomainRulesGlobalRuleSpaceBetween';
import { PlEduAghDomainRulesMessagesGetRulesResponse } from '../models/PlEduAghDomainRulesMessagesGetRulesResponse';
import { PlEduAghDomainRulesRules } from '../models/PlEduAghDomainRulesRules';
import { PlEduAghDomainRulesTrainTrainRule } from '../models/PlEduAghDomainRulesTrainTrainRule';
import { PlEduAghDomainRulesTrainTrainRuleDisabledSeats } from '../models/PlEduAghDomainRulesTrainTrainRuleDisabledSeats';
import { PlEduAghDomainTrainBitmapCoord } from '../models/PlEduAghDomainTrainBitmapCoord';
import { PlEduAghDomainTrainBitmapField } from '../models/PlEduAghDomainTrainBitmapField';
import { PlEduAghDomainTrainBitmapFieldCorridor } from '../models/PlEduAghDomainTrainBitmapFieldCorridor';
import { PlEduAghDomainTrainBitmapFieldSeat } from '../models/PlEduAghDomainTrainBitmapFieldSeat';
import { PlEduAghDomainTrainBitmapFieldUnknownField } from '../models/PlEduAghDomainTrainBitmapFieldUnknownField';
import { PlEduAghDomainTrainBitmapFieldWall } from '../models/PlEduAghDomainTrainBitmapFieldWall';
import { PlEduAghDomainTrainBitmapFieldWindow } from '../models/PlEduAghDomainTrainBitmapFieldWindow';
import { PlEduAghDomainTrainCarCar } from '../models/PlEduAghDomainTrainCarCar';
import { PlEduAghDomainUserOrdersBookCommand } from '../models/PlEduAghDomainUserOrdersBookCommand';
import { PlEduAghDomainUserOrdersPaymentCommand } from '../models/PlEduAghDomainUserOrdersPaymentCommand';
import { PlEduAghDomainUserOrdersUserOrderResponse } from '../models/PlEduAghDomainUserOrdersUserOrderResponse';
import { PlEduAghDomainUserOrdersUserOrdersResponse } from '../models/PlEduAghDomainUserOrdersUserOrdersResponse';
import { PlEduAghDomainUserPreferencesPreferences } from '../models/PlEduAghDomainUserPreferencesPreferences';
import { PlEduAghDomainUserPreferencesPreferencesCommand } from '../models/PlEduAghDomainUserPreferencesPreferencesCommand';
import { PlEduAghDomainUserPreferencesWithoutTransfers } from '../models/PlEduAghDomainUserPreferencesWithoutTransfers';
import { PlEduAghNetworkNullResponse } from '../models/PlEduAghNetworkNullResponse';
import { ObservableDefaultApi } from './ObservableAPI';


import { DefaultApiRequestFactory, DefaultApiResponseProcessor} from "../apis/DefaultApi";
export class PromiseDefaultApi {
    private api: ObservableDefaultApi

    public constructor(
        configuration: Configuration,
        requestFactory?: DefaultApiRequestFactory,
        responseProcessor?: DefaultApiResponseProcessor
    ) {
        this.api = new ObservableDefaultApi(configuration, requestFactory, responseProcessor);
    }

    /**
     */
    public getRulesGet(options?: Configuration): Promise<PlEduAghDomainRulesMessagesGetRulesResponse> {
    	const result = this.api.getRulesGet(options);
        return result.toPromise();
    }
	
    /**
     * @param startStation 
     * @param endStation 
     * @param startTimeRange 
     * @param endTimeRange 
     * @param isDirect 
     */
    public journeyGet(startStation: string, endStation: string, startTimeRange: string, endTimeRange: string, isDirect: boolean, options?: Configuration): Promise<PlEduAghDomainJourneyJourneysResponse> {
    	const result = this.api.journeyGet(startStation, endStation, startTimeRange, endTimeRange, isDirect, options);
        return result.toPromise();
    }
	
    /**
     */
    public preferencesGetGet(options?: Configuration): Promise<PlEduAghDomainUserPreferencesPreferences> {
    	const result = this.api.preferencesGetGet(options);
        return result.toPromise();
    }
	
    /**
     * @param id 
     * @param plEduAghDomainUserPreferencesPreferencesCommand 
     */
    public preferencesStorePost(id: string, plEduAghDomainUserPreferencesPreferencesCommand?: PlEduAghDomainUserPreferencesPreferencesCommand, options?: Configuration): Promise<PlEduAghNetworkNullResponse> {
    	const result = this.api.preferencesStorePost(id, plEduAghDomainUserPreferencesPreferencesCommand, options);
        return result.toPromise();
    }
	
    /**
     * @param id 
     */
    public stationsGet(id: string, options?: Configuration): Promise<Array<string>> {
    	const result = this.api.stationsGet(id, options);
        return result.toPromise();
    }
	
    /**
     * @param plEduAghDomainRulesRules 
     */
    public storeRulesPost(plEduAghDomainRulesRules?: PlEduAghDomainRulesRules, options?: Configuration): Promise<any> {
    	const result = this.api.storeRulesPost(plEduAghDomainRulesRules, options);
        return result.toPromise();
    }
	
    /**
     * @param id 
     * @param plEduAghDomainUserOrdersBookCommand 
     */
    public userBookPost(id: string, plEduAghDomainUserOrdersBookCommand?: PlEduAghDomainUserOrdersBookCommand, options?: Configuration): Promise<PlEduAghDomainUserOrdersUserOrdersResponse> {
    	const result = this.api.userBookPost(id, plEduAghDomainUserOrdersBookCommand, options);
        return result.toPromise();
    }
	
    /**
     * @param id 
     * @param body 
     */
    public userCancelPost(id: string, body?: any, options?: Configuration): Promise<PlEduAghNetworkNullResponse> {
    	const result = this.api.userCancelPost(id, body, options);
        return result.toPromise();
    }
	
    /**
     */
    public userOrdersGet(options?: Configuration): Promise<PlEduAghDomainUserOrdersUserOrdersResponse> {
    	const result = this.api.userOrdersGet(options);
        return result.toPromise();
    }
	
    /**
     * @param id 
     * @param plEduAghDomainUserOrdersPaymentCommand 
     */
    public userPaymentPost(id: string, plEduAghDomainUserOrdersPaymentCommand?: PlEduAghDomainUserOrdersPaymentCommand, options?: Configuration): Promise<PlEduAghNetworkNullResponse> {
    	const result = this.api.userPaymentPost(id, plEduAghDomainUserOrdersPaymentCommand, options);
        return result.toPromise();
    }
	

}



