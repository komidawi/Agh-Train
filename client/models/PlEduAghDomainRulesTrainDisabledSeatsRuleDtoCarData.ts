/**
 * Agh.Train
 * AghTrainApi
 *
 * OpenAPI spec version: 0.0.1
 * Contact: agh@no-reply.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { HttpFile } from '../http/http';

export class PlEduAghDomainRulesTrainDisabledSeatsRuleDtoCarData {
    'carId': number;
    'disabledSeatNumbers': Array<number>;

    static readonly discriminator: string | undefined = undefined;

    static readonly attributeTypeMap: Array<{name: string, baseName: string, type: string, format: string}> = [
        {
            "name": "carId",
            "baseName": "carId",
            "type": "number",
            "format": "int32"
        },
        {
            "name": "disabledSeatNumbers",
            "baseName": "disabledSeatNumbers",
            "type": "Array<number>",
            "format": "int32"
        }    ];

    static getAttributeTypeMap() {
        return PlEduAghDomainRulesTrainDisabledSeatsRuleDtoCarData.attributeTypeMap;
    }
    
    public constructor() {
    }
}

