/**
 * Agh.Train
 * AghTrainApi
 *
 * OpenAPI spec version: 0.0.1
 * Contact: agh@no-reply.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { HttpFile } from '../http/http';

export class PlEduAghDomainJourneyBookedSeatDto {
    'carId': number;
    'carType': PlEduAghDomainJourneyBookedSeatDtoCarTypeEnum;
    'seatId': number;

    static readonly discriminator: string | undefined = undefined;

    static readonly attributeTypeMap: Array<{name: string, baseName: string, type: string, format: string}> = [
        {
            "name": "carId",
            "baseName": "carId",
            "type": "number",
            "format": "int32"
        },
        {
            "name": "carType",
            "baseName": "carType",
            "type": "PlEduAghDomainJourneyBookedSeatDtoCarTypeEnum",
            "format": ""
        },
        {
            "name": "seatId",
            "baseName": "seatId",
            "type": "number",
            "format": "int32"
        }    ];

    static getAttributeTypeMap() {
        return PlEduAghDomainJourneyBookedSeatDto.attributeTypeMap;
    }
    
    public constructor() {
    }
}


export type PlEduAghDomainJourneyBookedSeatDtoCarTypeEnum = "COMPARTMENT_FIRST_CLASS" | "COMPARTMENT_SECOND_CLASS" ;

