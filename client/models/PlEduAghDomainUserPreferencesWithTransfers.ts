/**
 * Agh.Train
 * AghTrainApi
 *
 * OpenAPI spec version: 0.0.1
 * Contact: agh@no-reply.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { HttpFile } from '../http/http';

export class PlEduAghDomainUserPreferencesWithTransfers {
    'directOnly': boolean;

    static readonly discriminator: string | undefined = undefined;

    static readonly attributeTypeMap: Array<{name: string, baseName: string, type: string, format: string}> = [
        {
            "name": "directOnly",
            "baseName": "directOnly",
            "type": "boolean",
            "format": ""
        }    ];

    static getAttributeTypeMap() {
        return PlEduAghDomainUserPreferencesWithTransfers.attributeTypeMap;
    }
    
    public constructor() {
    }
}

