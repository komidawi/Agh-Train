/**
 * Agh.Train
 * AghTrainApi
 *
 * OpenAPI spec version: 0.0.1
 * Contact: agh@no-reply.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { DomainRulesTrainTrainRuleDisabledSeats } from './DomainRulesTrainTrainRuleDisabledSeats';
import { PlEduAghDomainRulesTrainDisabledSeatsRuleDto } from './PlEduAghDomainRulesTrainDisabledSeatsRuleDto';
import { HttpFile } from '../http/http';

export class DomainRulesTrainTrainRule {
    'type'?: string;

    static readonly discriminator: string | undefined = "type";

    static readonly attributeTypeMap: Array<{name: string, baseName: string, type: string, format: string}> = [
        {
            "name": "type",
            "baseName": "type",
            "type": "string",
            "format": "string"
        }    ];

    static getAttributeTypeMap() {
        return DomainRulesTrainTrainRule.attributeTypeMap;
    }
    
    public constructor() {
        this.type = "DomainRulesTrainTrainRule";
    }
}

