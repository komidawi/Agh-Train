export * from './DomainTrainTrain';
export * from './PlEduAghDomainHistoricalInfo';
export * from './PlEduAghDomainJourneyBookedSeatDto';
export * from './PlEduAghDomainJourneyJourneyLegDto';
export * from './PlEduAghDomainJourneyJourneyResponse';
export * from './PlEduAghDomainJourneyJourneysResponse';
export * from './PlEduAghDomainJourneyLegStopDto';
export * from './PlEduAghDomainRaptorSegment';
export * from './PlEduAghDomainRulesGlobalRule';
export * from './PlEduAghDomainRulesGlobalRuleDisabledStop';
export * from './PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled';
export * from './PlEduAghDomainRulesGlobalRuleOnePersonInCompartment';
export * from './PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled';
export * from './PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled';
export * from './PlEduAghDomainRulesGlobalRuleSpaceBetween';
export * from './PlEduAghDomainRulesMessagesGetRulesResponse';
export * from './PlEduAghDomainRulesRules';
export * from './PlEduAghDomainRulesTrainTrainRule';
export * from './PlEduAghDomainRulesTrainTrainRuleDisabledSeats';
export * from './PlEduAghDomainTrainBitmapCoord';
export * from './PlEduAghDomainTrainBitmapField';
export * from './PlEduAghDomainTrainBitmapFieldCorridor';
export * from './PlEduAghDomainTrainBitmapFieldSeat';
export * from './PlEduAghDomainTrainBitmapFieldUnknownField';
export * from './PlEduAghDomainTrainBitmapFieldWall';
export * from './PlEduAghDomainTrainBitmapFieldWindow';
export * from './PlEduAghDomainTrainCarCar';
export * from './PlEduAghDomainUserOrdersBookCommand';
export * from './PlEduAghDomainUserOrdersPaymentCommand';
export * from './PlEduAghDomainUserOrdersUserOrderResponse';
export * from './PlEduAghDomainUserOrdersUserOrdersResponse';
export * from './PlEduAghDomainUserPreferencesPreferences';
export * from './PlEduAghDomainUserPreferencesPreferencesCommand';
export * from './PlEduAghDomainUserPreferencesWithoutTransfers';
export * from './PlEduAghNetworkNullResponse';

import { DomainTrainTrain   , DomainTrainTrainTypeEnum   } from './DomainTrainTrain';
import { PlEduAghDomainHistoricalInfo } from './PlEduAghDomainHistoricalInfo';
import { PlEduAghDomainJourneyBookedSeatDto , PlEduAghDomainJourneyBookedSeatDtoCarTypeEnum    } from './PlEduAghDomainJourneyBookedSeatDto';
import { PlEduAghDomainJourneyJourneyLegDto } from './PlEduAghDomainJourneyJourneyLegDto';
import { PlEduAghDomainJourneyJourneyResponse } from './PlEduAghDomainJourneyJourneyResponse';
import { PlEduAghDomainJourneyJourneysResponse } from './PlEduAghDomainJourneyJourneysResponse';
import { PlEduAghDomainJourneyLegStopDto } from './PlEduAghDomainJourneyLegStopDto';
import { PlEduAghDomainRaptorSegment } from './PlEduAghDomainRaptorSegment';
import { PlEduAghDomainRulesGlobalRule , PlEduAghDomainRulesGlobalRuleLocationsEnum     } from './PlEduAghDomainRulesGlobalRule';
import { PlEduAghDomainRulesGlobalRuleDisabledStop } from './PlEduAghDomainRulesGlobalRuleDisabledStop';
import { PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled } from './PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled';
import { PlEduAghDomainRulesGlobalRuleOnePersonInCompartment } from './PlEduAghDomainRulesGlobalRuleOnePersonInCompartment';
import { PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled, PlEduAghDomainRulesGlobalRuleSeatLocationsDisabledLocationsEnum    } from './PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled';
import { PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled } from './PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled';
import { PlEduAghDomainRulesGlobalRuleSpaceBetween } from './PlEduAghDomainRulesGlobalRuleSpaceBetween';
import { PlEduAghDomainRulesMessagesGetRulesResponse } from './PlEduAghDomainRulesMessagesGetRulesResponse';
import { PlEduAghDomainRulesRules } from './PlEduAghDomainRulesRules';
import { PlEduAghDomainRulesTrainTrainRule } from './PlEduAghDomainRulesTrainTrainRule';
import { PlEduAghDomainRulesTrainTrainRuleDisabledSeats } from './PlEduAghDomainRulesTrainTrainRuleDisabledSeats';
import { PlEduAghDomainTrainBitmapCoord } from './PlEduAghDomainTrainBitmapCoord';
import { PlEduAghDomainTrainBitmapField   , PlEduAghDomainTrainBitmapFieldDirectionEnum   , PlEduAghDomainTrainBitmapFieldLocationEnum   , PlEduAghDomainTrainBitmapFieldPropertiesEnum   } from './PlEduAghDomainTrainBitmapField';
import { PlEduAghDomainTrainBitmapFieldCorridor } from './PlEduAghDomainTrainBitmapFieldCorridor';
import { PlEduAghDomainTrainBitmapFieldSeat , PlEduAghDomainTrainBitmapFieldSeatDirectionEnum    , PlEduAghDomainTrainBitmapFieldSeatLocationEnum   , PlEduAghDomainTrainBitmapFieldSeatPropertiesEnum    } from './PlEduAghDomainTrainBitmapFieldSeat';
import { PlEduAghDomainTrainBitmapFieldUnknownField } from './PlEduAghDomainTrainBitmapFieldUnknownField';
import { PlEduAghDomainTrainBitmapFieldWall } from './PlEduAghDomainTrainBitmapFieldWall';
import { PlEduAghDomainTrainBitmapFieldWindow } from './PlEduAghDomainTrainBitmapFieldWindow';
import { PlEduAghDomainTrainCarCar, PlEduAghDomainTrainCarCarCarTypeEnum     } from './PlEduAghDomainTrainCarCar';
import { PlEduAghDomainUserOrdersBookCommand } from './PlEduAghDomainUserOrdersBookCommand';
import { PlEduAghDomainUserOrdersPaymentCommand } from './PlEduAghDomainUserOrdersPaymentCommand';
import { PlEduAghDomainUserOrdersUserOrderResponse  , PlEduAghDomainUserOrdersUserOrderResponseStatusEnum   } from './PlEduAghDomainUserOrdersUserOrderResponse';
import { PlEduAghDomainUserOrdersUserOrdersResponse } from './PlEduAghDomainUserOrdersUserOrdersResponse';
import { PlEduAghDomainUserPreferencesPreferences } from './PlEduAghDomainUserPreferencesPreferences';
import { PlEduAghDomainUserPreferencesPreferencesCommand } from './PlEduAghDomainUserPreferencesPreferencesCommand';
import { PlEduAghDomainUserPreferencesWithoutTransfers } from './PlEduAghDomainUserPreferencesWithoutTransfers';
import { PlEduAghNetworkNullResponse } from './PlEduAghNetworkNullResponse';

/* tslint:disable:no-unused-variable */
let primitives = [
                    "string",
                    "boolean",
                    "double",
                    "integer",
                    "long",
                    "float",
                    "number",
                    "any"
                 ];

const supportedMediaTypes: { [mediaType: string]: number } = {
  "application/json": Infinity,
  "application/octet-stream": 0
}

                 
let enumsMap: Set<string> = new Set<string>([
    "DomainTrainTrainTypeEnum",
    "PlEduAghDomainJourneyBookedSeatDtoCarTypeEnum",
    "PlEduAghDomainRulesGlobalRuleLocationsEnum",
    "PlEduAghDomainRulesGlobalRuleSeatLocationsDisabledLocationsEnum",
    "PlEduAghDomainTrainBitmapFieldDirectionEnum",
    "PlEduAghDomainTrainBitmapFieldLocationEnum",
    "PlEduAghDomainTrainBitmapFieldPropertiesEnum",
    "PlEduAghDomainTrainBitmapFieldSeatDirectionEnum",
    "PlEduAghDomainTrainBitmapFieldSeatLocationEnum",
    "PlEduAghDomainTrainBitmapFieldSeatPropertiesEnum",
    "PlEduAghDomainTrainCarCarCarTypeEnum",
    "PlEduAghDomainUserOrdersUserOrderResponseStatusEnum",
]);

let typeMap: {[index: string]: any} = {
    "DomainTrainTrain": DomainTrainTrain,
    "PlEduAghDomainHistoricalInfo": PlEduAghDomainHistoricalInfo,
    "PlEduAghDomainJourneyBookedSeatDto": PlEduAghDomainJourneyBookedSeatDto,
    "PlEduAghDomainJourneyJourneyLegDto": PlEduAghDomainJourneyJourneyLegDto,
    "PlEduAghDomainJourneyJourneyResponse": PlEduAghDomainJourneyJourneyResponse,
    "PlEduAghDomainJourneyJourneysResponse": PlEduAghDomainJourneyJourneysResponse,
    "PlEduAghDomainJourneyLegStopDto": PlEduAghDomainJourneyLegStopDto,
    "PlEduAghDomainRaptorSegment": PlEduAghDomainRaptorSegment,
    "PlEduAghDomainRulesGlobalRule": PlEduAghDomainRulesGlobalRule,
    "PlEduAghDomainRulesGlobalRuleDisabledStop": PlEduAghDomainRulesGlobalRuleDisabledStop,
    "PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled": PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled,
    "PlEduAghDomainRulesGlobalRuleOnePersonInCompartment": PlEduAghDomainRulesGlobalRuleOnePersonInCompartment,
    "PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled": PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled,
    "PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled": PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled,
    "PlEduAghDomainRulesGlobalRuleSpaceBetween": PlEduAghDomainRulesGlobalRuleSpaceBetween,
    "PlEduAghDomainRulesMessagesGetRulesResponse": PlEduAghDomainRulesMessagesGetRulesResponse,
    "PlEduAghDomainRulesRules": PlEduAghDomainRulesRules,
    "PlEduAghDomainRulesTrainTrainRule": PlEduAghDomainRulesTrainTrainRule,
    "PlEduAghDomainRulesTrainTrainRuleDisabledSeats": PlEduAghDomainRulesTrainTrainRuleDisabledSeats,
    "PlEduAghDomainTrainBitmapCoord": PlEduAghDomainTrainBitmapCoord,
    "PlEduAghDomainTrainBitmapField": PlEduAghDomainTrainBitmapField,
    "PlEduAghDomainTrainBitmapFieldCorridor": PlEduAghDomainTrainBitmapFieldCorridor,
    "PlEduAghDomainTrainBitmapFieldSeat": PlEduAghDomainTrainBitmapFieldSeat,
    "PlEduAghDomainTrainBitmapFieldUnknownField": PlEduAghDomainTrainBitmapFieldUnknownField,
    "PlEduAghDomainTrainBitmapFieldWall": PlEduAghDomainTrainBitmapFieldWall,
    "PlEduAghDomainTrainBitmapFieldWindow": PlEduAghDomainTrainBitmapFieldWindow,
    "PlEduAghDomainTrainCarCar": PlEduAghDomainTrainCarCar,
    "PlEduAghDomainUserOrdersBookCommand": PlEduAghDomainUserOrdersBookCommand,
    "PlEduAghDomainUserOrdersPaymentCommand": PlEduAghDomainUserOrdersPaymentCommand,
    "PlEduAghDomainUserOrdersUserOrderResponse": PlEduAghDomainUserOrdersUserOrderResponse,
    "PlEduAghDomainUserOrdersUserOrdersResponse": PlEduAghDomainUserOrdersUserOrdersResponse,
    "PlEduAghDomainUserPreferencesPreferences": PlEduAghDomainUserPreferencesPreferences,
    "PlEduAghDomainUserPreferencesPreferencesCommand": PlEduAghDomainUserPreferencesPreferencesCommand,
    "PlEduAghDomainUserPreferencesWithoutTransfers": PlEduAghDomainUserPreferencesWithoutTransfers,
    "PlEduAghNetworkNullResponse": PlEduAghNetworkNullResponse,
}

export class ObjectSerializer {
    public static findCorrectType(data: any, expectedType: string) {
        if (data == undefined) {
            return expectedType;
        } else if (primitives.indexOf(expectedType.toLowerCase()) !== -1) {
            return expectedType;
        } else if (expectedType === "Date") {
            return expectedType;
        } else {
            if (enumsMap.has(expectedType)) {
                return expectedType;
            }

            if (!typeMap[expectedType]) {
                return expectedType; // w/e we don't know the type
            }

            // Check the discriminator
            let discriminatorProperty = typeMap[expectedType].discriminator;
            if (discriminatorProperty == null) {
                return expectedType; // the type does not have a discriminator. use it.
            } else {
                if (data[discriminatorProperty]) {
                    var discriminatorType = data[discriminatorProperty];
                    if(typeMap[discriminatorType]){
                        return discriminatorType; // use the type given in the discriminator
                    } else {
                        return expectedType; // discriminator did not map to a type
                    }
                } else {
                    return expectedType; // discriminator was not present (or an empty string)
                }
            }
        }
    }

    public static serialize(data: any, type: string, format: string) {
        if (data == undefined) {
            return data;
        } else if (primitives.indexOf(type.toLowerCase()) !== -1) {
            return data;
        } else if (type.lastIndexOf("Array<", 0) === 0) { // string.startsWith pre es6
            let subType: string = type.replace("Array<", ""); // Array<Type> => Type>
            subType = subType.substring(0, subType.length - 1); // Type> => Type
            let transformedData: any[] = [];
            for (let index in data) {
                let date = data[index];
                transformedData.push(ObjectSerializer.serialize(date, subType, format));
            }
            return transformedData;
        } else if (type === "Date") {
            if (format == "date") {
                let month = data.getMonth()+1
                month = month < 10 ? "0" + month.toString() : month.toString()
                let day = data.getDate();
                day = day < 10 ? "0" + day.toString() : day.toString();

                return data.getFullYear() + "-" + month + "-" + day;
            } else {
                return data.toISOString();
            }
        } else {
            if (enumsMap.has(type)) {
                return data;
            }
            if (!typeMap[type]) { // in case we dont know the type
                return data;
            }
            
            // Get the actual type of this object
            type = this.findCorrectType(data, type);

            // get the map for the correct type.
            let attributeTypes = typeMap[type].getAttributeTypeMap();
            let instance: {[index: string]: any} = {};
            for (let index in attributeTypes) {
                let attributeType = attributeTypes[index];
                instance[attributeType.baseName] = ObjectSerializer.serialize(data[attributeType.name], attributeType.type, attributeType.format);
            }
            return instance;
        }
    }

    public static deserialize(data: any, type: string, format: string) {
        // polymorphism may change the actual type.
        type = ObjectSerializer.findCorrectType(data, type);
        if (data == undefined) {
            return data;
        } else if (primitives.indexOf(type.toLowerCase()) !== -1) {
            return data;
        } else if (type.lastIndexOf("Array<", 0) === 0) { // string.startsWith pre es6
            let subType: string = type.replace("Array<", ""); // Array<Type> => Type>
            subType = subType.substring(0, subType.length - 1); // Type> => Type
            let transformedData: any[] = [];
            for (let index in data) {
                let date = data[index];
                transformedData.push(ObjectSerializer.deserialize(date, subType, format));
            }
            return transformedData;
        } else if (type === "Date") {
            return new Date(data);
        } else {
            if (enumsMap.has(type)) {// is Enum
                return data;
            }

            if (!typeMap[type]) { // dont know the type
                return data;
            }
            let instance = new typeMap[type]();
            let attributeTypes = typeMap[type].getAttributeTypeMap();
            for (let index in attributeTypes) {
                let attributeType = attributeTypes[index];
                instance[attributeType.name] = ObjectSerializer.deserialize(data[attributeType.baseName], attributeType.type, attributeType.format);
            }
            return instance;
        }
    }


    /**
     * Normalize media type
     *
     * We currently do not handle any media types attributes, i.e. anything
     * after a semicolon. All content is assumed to be UTF-8 compatible.
     */
    public static normalizeMediaType(mediaType: string | undefined): string | undefined {
        if (mediaType === undefined) {
            return undefined;
        }
        return mediaType.split(";")[0].trim().toLowerCase();
    }

    /**
     * From a list of possible media types, choose the one we can handle best.
     *
     * The order of the given media types does not have any impact on the choice
     * made.
     */
    public static getPreferredMediaType(mediaTypes: Array<string>): string {
        /** According to OAS 3 we should default to json */
        if (!mediaTypes) {
            return "application/json";
        }

        const normalMediaTypes = mediaTypes.map(this.normalizeMediaType);
        let selectedMediaType: string | undefined = undefined;
        let selectedRank: number = -Infinity;
        for (const mediaType of normalMediaTypes) {
            if (supportedMediaTypes[mediaType!] > selectedRank) {
                selectedMediaType = mediaType;
                selectedRank = supportedMediaTypes[mediaType!];
            }
        }

        if (selectedMediaType === undefined) {
            throw new Error("None of the given media types are supported: " + mediaTypes.join(", "));
        }

        return selectedMediaType!;
    }

    /**
     * Convert data to a string according the given media type
     */
    public static stringify(data: any, mediaType: string): string {
        if (mediaType === "application/json") {
            return JSON.stringify(data);
        }

        throw new Error("The mediaType " + mediaType + " is not supported by ObjectSerializer.stringify.");
    }

    /**
     * Parse data from a string according to the given media type
     */
    public static parse(rawData: string, mediaType: string | undefined) {
        if (mediaType === undefined) {
            throw new Error("Cannot parse content. No Content-Type defined.");
        }

        if (mediaType === "application/json") {
            return JSON.parse(rawData);
        }

        throw new Error("The mediaType " + mediaType + " is not supported by ObjectSerializer.parse.");
    }
}
