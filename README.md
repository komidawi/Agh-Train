# System for optimizing seat allocation on trains

## Bachelor of Science Thesis Project

### Summary

The goal of the project was to design and implement a configurable system to optimize seat allocation on trains. The system was expected to allow easy configuration of seat allocation - a use case example would be passenger seating restrictions associated with the COVID-19 pandemic outbreak. Another important functionality was optimization of the seat assignment process for efficient use of logistical resources and increased passenger comfort.

More details can be found on the [presentation][presentation] or [Bachelor's thesis body.][thesis-body]

### Main functionalities

#### Optimization

* In case of lack of available seats for the whole journey, split it into parts and assign seats to multiple sections. It will allow a passenger to spend the whole journey on *some* seat instead of having an uncomfortable standing ticket. This way most available seats are utilized at any time.

#### For Traveler
* Select starting and destination stations to search for a route
* View details of a given journey
* Make a seat reservation for the trip
* View a summary of the reservation
* Cancel the reservation
* Configure trip search preferences

#### For Operator
* Configure rules for the seat allocation algorithm

#### Additional functional requirements
* Load train and timetable data
* Calculate travel prices
* Mark special seats
* Handle user logging


## High-level system diagram

System consists of two main components - backend running on the server side and the frontend, which is used to display the user interface through a web browser. Those components also cooperate
with other external systems. Figure 1 shows a high-level
diagram of the system.
<br />

<div align="center">
<figure>
<img src="./docs/img/HighLevelSystemDiagram.png" alt="High-level system diagram image">
<figcaption align="center"><b>Figure 1. High-level system diagram.</b></figcaption>
</figure>
</div>


## Used technologies

### Backend
* Kotlin
* Ktor
* Ktor-OpenAPI-Generator
* Kotest
* Mockk
* JWT
* RAPTOR <sup>[[1](#references)]</sup> <sup>[[2](#references)]</sup>
* jGraphT
* Jackson
* Gradle

### Frontend
* TypeScript
* Create React App
* BlueprintJS
* Material UI
* auth0-react
* swagger-ui-react


## Code Quality

Project has high code quality standards verified by SonarQube metrics (backend part).

[![pipeline status](https://gitlab.com/komidawi/Agh-Train/badges/master/pipeline.svg)](https://gitlab.com/komidawi/Agh-Train/-/commits/master)

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=komidawi_Agh-Train&metric=coverage)](https://sonarcloud.io/dashboard?id=komidawi_Agh-Train)

[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=komidawi_Agh-Train&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=komidawi_Agh-Train) [![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=komidawi_Agh-Train&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=komidawi_Agh-Train) [![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=komidawi_Agh-Train&metric=security_rating)](https://sonarcloud.io/dashboard?id=komidawi_Agh-Train)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=komidawi_Agh-Train&metric=bugs)](https://sonarcloud.io/dashboard?id=komidawi_Agh-Train) [![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=komidawi_Agh-Train&metric=code_smells)](https://sonarcloud.io/dashboard?id=komidawi_Agh-Train) [![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=komidawi_Agh-Train&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=komidawi_Agh-Train)

[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=komidawi_Agh-Train&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=komidawi_Agh-Train) [![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=komidawi_Agh-Train&metric=sqale_index)](https://sonarcloud.io/dashboard?id=komidawi_Agh-Train)

## Authors

**@komidawi** - main responsibility: backend

selection of technology and initialization of backend development environment, selection of tools for project management, data structure for train car models design, independent application of rules solution design, particular rules implementation: exclusion of seats from window/middle/corridor, exclusion of single seat

**@pixellos** - main responsibility: frontend 

frontend technology selection and development environment initialization, CI/CD environment management, user interface design, support of authorization and authentication, particular rules: configurable distance between passengers, persons not facing each other, only one person in a compartment

**@bubydevelopment** - main responsibility: algorithms

market research, risk analysis, selection of route search algorithms, design and implementation of seat assignment algorithms, design of fare calculation and reservation service, particular rules: single station exclusion rule

## More information

Comprehensive information describing all details of the project  available on:
* Thesis defence [presentation][presentation]
* Bachelor's thesis [full body][thesis-body]

## Screenshots

<div align="center">

<figure>
<img src="./docs/img/screenshots/1_start_screen.png" alt="Start screen" width="600px">
<figcaption><b>Figure 2. Start screen.</b></figcaption>
</figure>
<br/>

<figure>
<img src="./docs/img/screenshots/2_find_connection_panel.png" alt="Start screen" width="600px">
<figcaption><b>Figure 3. Connection finder panel.</b></figcaption>
</figure>
<br/>

<figure>
<img src="./docs/img/screenshots/3_available_journeys.png" alt="Available journeys" width="600px">
<figcaption><b>Figure 4. Available journeys view.</b></figcaption>
</figure>
<br/>

<figure>
<img src="./docs/img/screenshots/4_journey_steps.png" alt="Steps of the journey" width="600px">
<figcaption><b>Figure 5. Steps of an example journey.</b></figcaption>
</figure>
<br/>

<figure>
<img src="./docs/img/screenshots/5_rules_management_panel.png" alt="Rules management panel" width="600px">
<figcaption><b>Figure 6. Rules management panel.</b></figcaption>
</figure>
<br/>

<figure>
<img src="./docs/img/screenshots/6_car_diagnostic_view.png" alt="Car diagnostic view" width="600px">
<figcaption><b>Figure 7. Car diagnostic view fragment.</b></figcaption>
</figure>
<br/>

</div>

## References

[1] D. Delling, T. Pajor and R. F. Werneck. *Round-Based Public Transit Routing.* 2012. <a name="raptor-microsoft">https://www.microsoft.com/en-us/research/wp-content/uploads/2012/01/raptor_alenex.pdf</a>

[2] Simple Java implementation of the Raptor algorithm by raoulvdberge <a name="raptor-java">https://github.com/raoulvdberge/raptor</a>




[presentation]: docs/thesis-defence-presentation-pl.pdf
[thesis-body]: docs/D._Komisarczuk,_M._Popielarz,_P._Wróbel._System_optymalizujący_przydzielanie_miejsc_w_pociągach,_AGH_2021.pdf
