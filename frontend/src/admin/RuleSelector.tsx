import * as React from "react";
import { Button, Checkbox, Icon, InputGroup } from "@blueprintjs/core";
import {
  PlEduAghDomainRulesTrainTrainRule,
  PlEduAghDomainRulesMessagesGetRulesResponse,
  PlEduAghDomainRulesGlobalRuleDisabledStop,
  PlEduAghDomainRulesGlobalRule,
  PlEduAghDomainRulesRules,
  PlEduAghDomainRulesGlobalRuleSpaceBetween,
  PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled,
  PlEduAghDomainRulesGlobalRuleSeatLocationsDisabledLocationsEnum,
  PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled,
  PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled,
  PlEduAghDomainRulesGlobalRuleOnePersonInCompartment,
  PlEduAghDomainRulesTrainTrainRuleDisabledSeats
} from "client";
import { clone, cloneRuleFactory, handleNumberChange, handleStringChange, toggle } from "./helpers";


export function RuleSelector({
  r,
  parentIndex,
  setUserOrders,
  k: key
}: {
  r: PlEduAghDomainRulesTrainTrainRule | PlEduAghDomainRulesGlobalRule;
  parentIndex: number;
  setUserOrders: React.Dispatch<React.SetStateAction<PlEduAghDomainRulesMessagesGetRulesResponse | undefined>>;
  k: keyof PlEduAghDomainRulesRules;
}): JSX.Element {
  const cloneByTrainRule = cloneRuleFactory(setUserOrders, parentIndex);
  const cloneGlobalRule = cloneRuleFactory(setUserOrders, parentIndex, 'globalRules');
  const Control = (() => {
    if (r instanceof PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled) {
      return <div>
        Odstęp pomiędzy pasażerami
      </div>;
    }
    if (r instanceof PlEduAghDomainRulesGlobalRuleOnePersonInCompartment) {
      return <div>
        Jedna osoba w przedziale
      </div>;
    }
    if (r instanceof PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled) {
      return <div>
        Wyłączone miejsca na przeciw
      </div>;
    }
    else if (r instanceof PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled) {
      return <div>
        Wyłączone Miejsca
        {(['CORRIDOR', 'MIDDLE', 'WINDOW'] as PlEduAghDomainRulesGlobalRuleSeatLocationsDisabledLocationsEnum[])
          .map(x => <Checkbox
            checked={r.locations?.includes(x)}
            onChange={() => cloneGlobalRule(r, c => c.locations = toggle(c.locations, x))}
          >{x}</Checkbox>)}
      </div>;
    }
    else if (r instanceof PlEduAghDomainRulesGlobalRuleSpaceBetween) {
      return <div>Odstęp pomiędzy miejscami <InputGroup placeholder="0" defaultValue={r.requiredSpace}
        onBlur={handleStringChange(x => cloneGlobalRule(r, c => c.requiredSpace = x))} /></div>;
    }
    else if (r instanceof PlEduAghDomainRulesGlobalRuleDisabledStop)
      return <div>Disabled
        <InputGroup
          key='to'
          defaultValue={r.excluded}
          onBlur={handleStringChange(x => cloneGlobalRule(r, c => c.excluded = x))} />
      </div>;
    if (r instanceof PlEduAghDomainRulesTrainTrainRuleDisabledSeats) {
      return <div>
        <h4>Wyłączenie poszczególnych miejsc</h4>
        <Button
          icon='add'
          onClick={() => cloneByTrainRule(r, clone => {
            const seats = clone.seatIds ?? []
            seats.push(undefined as any);
            clone.seatIds = seats
            return clone
          })} />
        <InputGroup
          key='trainId'
          placeholder='Numer pociągu'
          defaultValue={r.trainId}
          style={{ maxWidth : '200px' }}
          onBlur={handleStringChange(trainId => cloneByTrainRule(r, clone => clone.trainId = trainId))} />
        <InputGroup
          key='carId'
          type='number'
          placeholder='Numer wagonu'
          defaultValue={r.carId?.toString()}
          style={{ maxWidth : '200px' }}
          onBlur={handleNumberChange(carId => cloneByTrainRule(r, clone => clone.carId = carId))} />
        <ul>
          {r?.seatIds?.map((seatId, i) =>
            <li style={{ display: 'flex' }}>
              <InputGroup
                key={i}
                type='number'
                placeholder='Numer miejsca'
                defaultValue={seatId?.toString()}
                style={{ maxWidth : '200px' }}
                onBlur={handleNumberChange(seatId => cloneByTrainRule(r, clone => clone.seatIds[i] = seatId))} />
              <Button
                icon='remove'
                onClick={() => cloneByTrainRule(r, clone => {
                  clone.seatIds = clone.seatIds?.filter((_, j) => j !== i)
                  return clone
                })} />
            </li>)
          }
        </ul>
      </div>;
    }
    return <div>Unknown</div>;
  });

  return <div key={parentIndex}>
    <Icon icon={'disable'}
      onClick={() => clone(setUserOrders, s => {
        return s.rules[key]?.splice(parentIndex, 1);
      })} />
    <Control />
  </div>;
}
