import * as React from "react";
import { Card } from "@blueprintjs/core";
import { PlEduAghDomainRulesMessagesGetRulesResponse } from "client";
import { fromState, UseStateReturnType } from "../UseState";
import { RuleSelector } from "./RuleSelector";


export function Rules({ rules }: { rules: UseStateReturnType<PlEduAghDomainRulesMessagesGetRulesResponse | undefined>; }) {
  return (
    <div>
      <Card>
        <h2>Reguły globalne</h2>
        <ul>
          {fromState(rules)?.rules?.globalRules?.map(
            (x, parentIndex) => (
              <RuleSelector r={x} parentIndex={parentIndex} k={'globalRules'} setUserOrders={rules[1]} />
            )
          )}
        </ul>
      </Card>
      <Card>
        <h2>Reguły per pociąg</h2>
        <ul>
          {fromState(rules)?.rules?.byTrain?.map(
            (x, parentIndex) => (
              <RuleSelector r={x} parentIndex={parentIndex} k={'byTrain'} setUserOrders={rules[1]} />
            )
          )}
        </ul>
      </Card>
    </div>
  );

}
