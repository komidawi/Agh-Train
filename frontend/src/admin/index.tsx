import * as React from "react";
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/datetime/lib/css/blueprint-datetime.css";
import "@blueprintjs/select/lib/css/blueprint-select.css";
import "@blueprintjs/table/lib/css/table.css";
import { Classes, Icon, Menu, MenuDivider, MenuItem, } from "@blueprintjs/core";
import { useApi } from "../useApiConfigurationAsync";
import { useEffect, useState } from "react";
import {
  DefaultApi, PlEduAghDomainRulesMessagesGetRulesResponse,
  PlEduAghDomainRulesGlobalRuleDisabledStop,
  PlEduAghDomainRulesRules,
  PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled,
  PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled,
  PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled,
  PlEduAghDomainRulesGlobalRuleOnePersonInCompartment,
  PlEduAghDomainRulesTrainTrainRuleDisabledSeats,
} from "client";
import { Rules } from "./Rules";
import { createRulesResponse, WithCtor } from "./helpers";

type AddMiddleRuleT = <T>(ctor: WithCtor<T>, key: keyof PlEduAghDomainRulesRules) => void;

export function App(): JSX.Element {
  const apiConfiguration = useApi();
  const [get, set] = useState<PlEduAghDomainRulesMessagesGetRulesResponse | undefined>(undefined)
  useEffect(() => {
    (async () => apiConfiguration && set(await new DefaultApi(apiConfiguration).getRulesGet()))()
  }, [apiConfiguration, set]);
  const save = React.useCallback(async () => apiConfiguration &&
    get &&
    await new DefaultApi(apiConfiguration).storeRulesPost(get.rules),
    [get, apiConfiguration])

  const addMiddleRule = React.useCallback<AddMiddleRuleT>((ctor, key) => {
    set(x => {
      let result = createRulesResponse(x);
      const newRule = new ctor();
      result.rules[key].push(newRule as any);
      return result;
    })
  }, [set])

  return (
    <div>
      <div className="flex">
        <div className="row flex-right-80">
          <LeftMenu addMiddleRule={addMiddleRule} save={save} />
          <div className="flex-right-80">
            {get && <Rules rules={[get, set]} />}
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;

function LeftMenu({ addMiddleRule, save }: { addMiddleRule: AddMiddleRuleT, save: () => Promise<any> }) {
  return (
    <Menu className={Classes.ELEVATION_1}>
      <MenuItem icon="train" text="Zarządzaj regułami" />
      <MenuItem text="Reguła globalna (algorytm)">
        <MenuItem text="Wyłączenie przystanku" onClick={() => addMiddleRule(PlEduAghDomainRulesGlobalRuleDisabledStop, 'globalRules')} />
      </MenuItem>
      <MenuItem text="Reguła globalna (pociąg)">
        <MenuItem text="Osoba w przedziale" onClick={() => addMiddleRule(PlEduAghDomainRulesGlobalRuleOnePersonInCompartment, 'globalRules')} />
        <MenuItem text="Odstęp pomiędzy pasażerami" onClick={() => addMiddleRule(PlEduAghDomainRulesGlobalRuleHorizontalSpaceBetweenDisabled, 'globalRules')} />
        <MenuItem text="Wyłączenie miejsc na przeciw" onClick={() => addMiddleRule(PlEduAghDomainRulesGlobalRuleSeatVisAVisDisabled, 'globalRules')} />
        <MenuItem text="Wyłączenie siedzenia" onClick={() => addMiddleRule(PlEduAghDomainRulesGlobalRuleSeatLocationsDisabled, 'globalRules')} />
        <MenuItem text="Wyłączenie konkretnego miejsca" onClick={() => addMiddleRule(PlEduAghDomainRulesTrainTrainRuleDisabledSeats, 'byTrain')} />
      </MenuItem>
      <MenuItem onClick={save} icon="saved" text="Zapisz" />
      <MenuDivider />
      <MenuItem icon="error" text="Komunikaty" />
      <MenuDivider />
      <MenuItem icon="cog" labelElement={<Icon icon="share" />} text="Ustawienia..." />
      <MenuItem icon="log-out" text="Wyloguj" />
    </Menu>);
}

