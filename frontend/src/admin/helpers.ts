import * as React from "react";
import {
  PlEduAghDomainRulesMessagesGetRulesResponse,
  PlEduAghDomainRulesRules
} from "client";
import { cloneDeepWith, xor } from "lodash"

export function cloneRuleFactory(
  setUserOrders: React.Dispatch<React.SetStateAction<PlEduAghDomainRulesMessagesGetRulesResponse | undefined>>,
  parentIndex: number,
  key: keyof PlEduAghDomainRulesRules = 'byTrain'
) {
  return <T>(
    // Only for type inference purposes
    instance: T,
    update: (item: T) => void) => clone(
      setUserOrders,
      s => {
        let lcs = (s.rules[key][parentIndex] as T);
        debugger;
        update(lcs);
      }
    );
}

export const toggle = <T,>(array: T[], item: T) => xor(array, [item])

export function clone<T>(
  setUserOrders: React.Dispatch<React.SetStateAction<T | undefined>>,
  updater: (x: T) => void) {
  return setUserOrders((s) => {
    const copied = cloneDeepWith(s);
    if (copied) {
      updater(copied);
    }
    return copied;
  });
}

export function handleStringChange(handler: (value: string) => void) {
  return (event: React.FormEvent<HTMLElement>) => handler((event.target as HTMLInputElement).value);
}

export function handleNumberChange(handler: (value: number) => void) {
  return (event: React.FormEvent<HTMLElement>) => handler(parseInt((event.target as HTMLInputElement).value));
}


export function handleBoolChange(handler: (value: boolean) => void) {
  return (event: React.FormEvent<HTMLElement>) => handler((event.target as HTMLInputElement).checked);
}

export type WithCtor<T> = {
  new(): T
}

export function createRulesResponse(x: PlEduAghDomainRulesMessagesGetRulesResponse | undefined) {
  let result = new PlEduAghDomainRulesMessagesGetRulesResponse();
  let rules = result.rules = new PlEduAghDomainRulesRules();
  rules.globalRules = x?.rules.globalRules ?? [];
  rules.byTrain = x?.rules.byTrain ?? [];
  return result;
}
