
export const ADMIN = "/admin";
export const TRAVELER = "/traveler";
export const BOOKING = "/booking";
export const SWAGGER = "/swagger";
export const HOME = "/";
