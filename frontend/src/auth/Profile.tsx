import React, { useEffect, useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";

export const Profile = () => {
  const { user, isAuthenticated, isLoading } = useAuth0();
  if (isLoading) {
    return <div>Powoli... Jak polskie koleje...</div>;
  }
  const role = GetUserRole();

  return (
    isAuthenticated && user !== undefined ? (
      <div style={{display: 'flex', flex: '1 1', flexDirection: 'row', justifyContent: 'center', alignItems:'center'}}>
        <img src={user.picture} alt={user.name} className="responsive" style={{ marginRight: "20px" }} />
        <h4>{role} </h4>
        <span>  {user.email}</span>
      </div>
    ) : <div></div>
  );
};


export function GetUserRole() {
  const { getIdTokenClaims } = useAuth0();

  const [get, set] = useState(null as null | 'Admin' | 'User');
  useEffect(() => {
    getIdTokenClaims().then(x => {
      const roles = x?.['https://any-namespace/roles'] as string[];
      const role = roles?.indexOf('Administrator') !== -1 ? 'Admin' : 'User';
      set(role);
    })
  });
  
  return get;
}