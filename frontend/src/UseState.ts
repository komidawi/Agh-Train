export type UseStateReturnType<T> =[T, React.Dispatch<React.SetStateAction<T>>]

export function fromState<T>(data: UseStateReturnType<T>) {
  return data[0];
}

export function setState<T>(state: UseStateReturnType<T>) {
  return state[1];
}

export function toState<T>(data: UseStateReturnType<T>, value: React.SetStateAction<T>) {
  return data[1](value);
}