import { useAuth0 } from '@auth0/auth0-react';
import { useEffect, useMemo } from 'react';
import { useState } from 'react';
import { createConfiguration, DefaultApi, ServerConfiguration } from 'client';

export function useApi() {
  const { getAccessTokenSilently, isAuthenticated } = useAuth0();
  const [token, setToken] = useState('');
  useEffect(() => {
    getAccessTokenSilently().then(x => setToken(x));
  }, [isAuthenticated, getAccessTokenSilently]);
  const config = useMemo(() => isAuthenticated &&
    token &&
    createConfiguration({
      authMethods: {
        jwtAuth: { tokenProvider: { getToken: async () => token } }
      },
      promiseMiddleware: [
        {
          pre: async (ctx) => {
            ctx.setHeaderParam("Authorization", 'Bearer ' + token);
            return ctx;
          },
          post: async x => x
        }
      ],
      baseServer: new ServerConfiguration("http://localhost:8080", {}),
    }), [token, isAuthenticated]);
  return config;
}

export function useDefaultEndpoint() {
  const api = useApi();
  return useMemo(() => api && new DefaultApi(api), [api]);
}
