
export function formatDateString(s: string | undefined) {
  const formatter = Intl.DateTimeFormat('pl-PL', { dateStyle: 'medium', timeStyle: 'medium' });
  return s && formatter.format(new Date(s));
}
