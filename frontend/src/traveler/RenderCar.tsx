import * as React from "react";
import {
  PlEduAghDomainJourneyBookedSeatDto,
  PlEduAghDomainTrainCarCar
} from "client";


export function RenderCar({
  car,
  booked,
}: {
  car: PlEduAghDomainTrainCarCar;
  booked: PlEduAghDomainJourneyBookedSeatDto[];
}) {
  return (
    <table style={{ display: "flex" }}>
      <td>
        <tr>{car.carType}</tr>
        <tr>{car.id}</tr>
      </td>
      {car.fieldMatrix.map((y, yi) => (
        <td key={yi}>
          {y.map((x, xi) => {
            const taken = booked.find(
              (b) => b.carId === car.id && b.seatId === x.number
            );
            return (
              <tr
                style={{
                  backgroundColor: !x.enabled ? "red" : (taken ? "green" : undefined),
                }}
                key={xi}
              >
                {x.letterCode}
              </tr>
            );
          })}
        </td>
      ))}
    </table>
  );
}
