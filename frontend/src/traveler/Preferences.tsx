import * as React from "react";

import { Collapse, Label, Checkbox, Card } from "@blueprintjs/core";
import './index.css'
import { useState, useEffect, useCallback } from "react";
import { useDefaultEndpoint } from "../useApiConfigurationAsync";
import { PlEduAghDomainUserPreferencesPreferences } from "client";
import 'react-vertical-timeline-component/style.min.css';
import { clone } from "../admin/helpers";
import { handleBooleanChange } from "@blueprintjs/docs-theme";
import { isFunction } from "@blueprintjs/core/lib/esm/common/utils";

export type GetterAndSetter<T> = {
  data: T;
  setData: React.Dispatch<React.SetStateAction<T | undefined>>;
}

export function usePreferencesFromServer(setData: React.Dispatch<React.SetStateAction<PlEduAghDomainUserPreferencesPreferences | undefined>>) {
  const endpoint = useDefaultEndpoint();
  useEffect(() => {
    endpoint && endpoint.preferencesGetGet().then(x => setData(() => x));
  }, [endpoint, setData]);
}

export function useSetPreferencesToServer(
  setData: React.Dispatch<React.SetStateAction<PlEduAghDomainUserPreferencesPreferences | undefined>>,
  data: PlEduAghDomainUserPreferencesPreferences | undefined
) {
  const endpoint = useDefaultEndpoint();
  return useCallback<typeof setData>(action => {
    const d = isFunction(action) ? action(data) : data;
    d && endpoint && endpoint.preferencesStorePost('', { preferences: d });
    setData(action);
  }, [setData, endpoint, data]);
}

export function DefaultPreferences() {
  const [get, set] = useState<PlEduAghDomainUserPreferencesPreferences | undefined>();
  const setPreferences = useSetPreferencesToServer(set, get);
  usePreferencesFromServer(set);

  return <Preferences data={get} setData={setPreferences} />
}

export function Preferences({ data, setData }: GetterAndSetter<PlEduAghDomainUserPreferencesPreferences | undefined>) {
  const [isOpen, setOpen] = useState(true);

  return <div className="flex">
    <div className="col">
      <Label onClick={() => setOpen(x => !x)}>
        {isOpen ? 'Zamknij preferencje' : 'Otwórz preferencje'}
      </Label>
      <div>
      </div>
      <Collapse isOpen={isOpen}>
        <PreferenceSelector setData={setData} data={data} />
      </Collapse>
    </div>
  </div>
}

export function PreferenceSelector({ data, setData }: GetterAndSetter<PlEduAghDomainUserPreferencesPreferences | undefined>): JSX.Element {

  if (!data) {
    return <span>Loading...</span>
  }

  return <div>
    <Card>
      <h2>Przejazdy</h2>
      <div style={{ display: 'flex' }}>
        <span>Tylko bezpośrednie</span>
        <Checkbox
          key='direct'
          type='checkbox'
          defaultChecked={data.withoutTransfers.enabled}
          onClick={handleBooleanChange(x => clone(setData, s => s.withoutTransfers.enabled = x))}
        />
      </div>
    </Card>
  </div>
}