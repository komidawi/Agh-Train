

import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/datetime/lib/css/blueprint-datetime.css";
import "@blueprintjs/select/lib/css/blueprint-select.css";
import "@blueprintjs/table/lib/css/table.css";
import { ITreeNode, } from "@blueprintjs/core";

export const INITIAL_STATE: ITreeNode[] = [
  {
    id: 0,
    hasCaret: false,
    icon: "select",
    label: "Przywróć do wartości domyślnych",
  },
  {
    id: 1,
    icon: "timeline-area-chart",
    isExpanded: true,
    label: "Domyślne fragmentowanie trasy",
    hasCaret: true,
    childNodes: [
      {
        id: 101,
        icon: 'tag',
        label: "Bez przesiadek"
      },
      {
        id: 103,
        icon: 'tag',
        label: "Z przesiadkami"
      },
      {
        id: 104,
        icon: 'tag',
        isSelected: true,
        label: "Brak preferencji"
      }
    ]
  },
  {
    id: 2,
    hasCaret: true,
    icon: "badge",
    label: "Opcje bagażu",
    isExpanded: true,
    childNodes: [
      {
        id: 202,
        icon: 'ban-circle',
        label: "Rower"
      },
      {
        id: 203,
        icon: 'ban-circle',
        label: "Walizka do 5 Kg"
      },
      {
        id: 203,
        icon: 'ban-circle',
        label: "Walizka do 20 Kg"
      },
      {
        id: 204,
        icon: 'ban-circle',
        label: "Bagaż wielkowymiarowy"
      },
      {
        id: 204,
        icon: 'ban-circle',
        isSelected: true,
        label: "Brak"
      },
     
    ],
  },

  {
    id: 5,
    hasCaret: true,
    icon: "barcode",
    label: "Domyślny rodzaj miejsca",
    isExpanded: true,
    childNodes: [
      {
        id: 501,
        icon: 'people',
        isSelected: true,
        label: "Stojące"
      },
      {
        id: 502,
        icon: 'join-table',
        label: "Siedzące"
      },
      {
        id: 503,
        icon: 'walk',
        isSelected: true,
        label: "Przy przejściu"
      },
      {
        id: 504,
        icon: 'box',
        label: "Przy oknie"
      },
      {
        id: 505,
        icon: 'code-block',
        label: "W wagonie z przedziałami"
      },
      {
        id: 506,
        icon: 'compressed',
        isSelected: true,
        label: "W wagonie bez przedziałów"
      },

      {
        id: 507,
        icon: 'selection',
        label: "Wybierz konkretne miejsca"
      },
     
    ],
  },
];

