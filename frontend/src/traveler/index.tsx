import * as React from "react";
import {useState} from "react";

import {Button, Card, Classes, DialogStep, InputGroup, Label, MultistepDialog} from "@blueprintjs/core";
import {DateRange, DateRangePicker} from "@blueprintjs/datetime";
import './index.css'
import {useApi} from "../useApiConfigurationAsync";
import {
  DefaultApi,
  PlEduAghDomainJourneyJourneyResponse,
  PlEduAghDomainJourneyJourneysResponse,
  PlEduAghDomainUserPreferencesPreferences
} from "client";
import {StopChain} from './StopChain';
import 'react-vertical-timeline-component/style.min.css';
import {Select} from "@blueprintjs/select";
import {OpenForPaymentHook} from "../user/PaymentContext";
import {first, last} from "lodash";
import {formatDateString} from "./formatDateString";
import {Preferences, usePreferencesFromServer} from "./Preferences";

export const App = () => {
  const [data, setData] = useState(undefined as PlEduAghDomainJourneyJourneysResponse | undefined);
  const [selected, setSelected] = useState(undefined as PlEduAghDomainJourneyJourneyResponse | undefined);
  const [from, setFrom] = useState('');
  const [to, setTo] = useState('');
  const [preferences, setPreferences] = useState(undefined as PlEduAghDomainUserPreferencesPreferences | undefined);
  usePreferencesFromServer(setPreferences);

  const [stations, setStations] = useState([] as string[]);
  const [date, setDate] = useState(undefined as DateRange | undefined);
  const apiConfiguration = useApi();

  React.useEffect(() => {
    apiConfiguration && new DefaultApi(apiConfiguration)
      .stationsGet('').then(setStations);
  }, [apiConfiguration])

  const checkConnections = React.useCallback(
    async () =>
      apiConfiguration &&
      setData(
        await new DefaultApi(apiConfiguration)
          .journeyGet(from, to, date?.[0]?.toISOString() ?? '', date?.[1]?.toISOString() ?? '', preferences?.withoutTransfers?.enabled ?? false)
      ),
    [
      apiConfiguration,
      from,
      to,
      date,
      preferences
    ])

  const openPay = OpenForPaymentHook()

  const doReservation = React.useCallback(
    async () => {
      const response = selected && apiConfiguration && await new DefaultApi(apiConfiguration).userBookPost('', { journey: selected });
      if (response) {
        const orders = response.orders;
        const last = orders[orders.length - 1];
        setData(undefined);
        openPay({ paymentId: last.id, amount: last.journey.cost });
      }
    },
    [
      apiConfiguration, selected, openPay
    ])

  const firstStage = <div style={{
    display: "grid",
    gridTemplateColumns: "repeat(auto-fit, minmax(280px, 4fr))",
    gridAutoRows: "85px"
  }}>
    {data?.journeys.map(j => {
      const departureStops = first(j.legs)?.stops;
      const departure = first(departureStops);
      const arrivalStops = last(j.legs)?.stops;
      const arrival = last(arrivalStops);
      const isSelected = selected === j;
      return <Card
        style={{ backgroundColor: isSelected ? 'whitesmoke' : undefined }}
        interactive={true}
        title={isSelected ? 'Wybrano' : ''}
        onClick={() => setSelected(j)}>
        <div>
          Z <b>{departure?.station}</b> o <b>{formatDateString(departure?.departureTime)}</b>
        </div>
        <div>
          Do <b>{arrival?.station}</b> o <b>{formatDateString(arrival?.arrivalTime)}</b>
        </div>
        <div>
          Cena <b>{j.cost.toFixed(2)} PLN</b>
        </div>
      </Card>
    }
    )}
  </div>

  const modal =
    <MultistepDialog
      usePortal={true}
      isOpen={!!data}
      style={{ width: "75%" }}
      onClose={() => {setData(undefined) ; setSelected(undefined); }}
      backButtonProps={{ text: 'Wstecz' }}
      nextButtonProps={{ text: 'Dalej', disabled: !!!selected }}
      finalButtonProps={{ text: 'Zapłać', onClick: () => doReservation() }}
    >
      <DialogStep
        id="select"
        panel={firstStage}
        title="Wybierz podróż"
      />
      <DialogStep
        id="Szczegóły"
        panel={<div>
          {selected && <StopChain journey={selected} />}
        </div>}
        title="Sprawdź szczegóły"
      />
    </MultistepDialog>;


  return (
    <div>
      {modal}
      <img alt="travel" src={`https://cdn.pixabay.com/photo/2017/09/18/17/11/rail-2762520_960_720.jpg`} style={{ width: "100%", height: "300px", objectFit: 'cover', opacity: "50%" }} />
      <div className="flex row">
        <div className="flex col">
          <h2>Wyszukaj połączenie</h2>
          <Select
            itemPredicate={(q, i) => i ? i.indexOf(q) >= 0 : false}
            items={stations}
            itemRenderer={x => <li onClick={() => setFrom(x)}>{x}</li>}
            onItemSelect={x => { setFrom(x) }}>
            <InputGroup
              style={{ width: "400px", marginBottom: "5px" }}
              large={true}
              defaultValue={from}
              onChange={(x: React.ChangeEvent<HTMLInputElement>) => setFrom(x.target.value)}
              placeholder="Skąd"
            />
          </Select>

          <Select
            itemPredicate={(q, i) => i.indexOf(q) >= 0}
            items={stations}
            itemRenderer={x => <li onClick={() => setTo(x)}>{x}</li>}
            onItemSelect={x => { setTo(x) }}>
            <InputGroup
              style={{ width: "400px", marginBottom: "5px" }}
              large={true}
              defaultValue={to}
              onChange={(x: React.ChangeEvent<HTMLInputElement>) => setTo(x.target.value)}
              placeholder="Dokąd"
            />
          </Select>
          <Button onClick={checkConnections}>
            Wyszukaj
          </Button>
          <div className="flex row" style={{ paddingTop: "20px" }}>
            <Label>
              <p className="center">
                Data odjazdu
                </p>
              <DateRangePicker
                minDate={new Date()}
                onChange={setDate}
                allowSingleDayRange={true}
                shortcuts={false}
                className={Classes.ELEVATION_1}
                timePrecision={'minute'}
              />
            </Label>
          </div>
        </div>
        {preferences ? <Preferences data={preferences} setData={setPreferences} /> : "Ładowanie..."}
      </div>
    </div>
  );
};

export default App;
