import * as React from "react";
import {
  PlEduAghDomainJourneyJourneyLegDto,
  PlEduAghDomainJourneyJourneyResponse,
  PlEduAghDomainRaptorSegment,
} from "client";
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";
import { formatDateString } from "./formatDateString";
import { useState } from "react";
import { Alert, Classes } from "@blueprintjs/core";
import { RenderCar } from "./RenderCar";

export function StopChain({
  journey,
}: {
  journey: PlEduAghDomainJourneyJourneyResponse;
}) {
  const [selectedTrain, setSelectedTrain] = useState(
    null as null | PlEduAghDomainRaptorSegment[]
  );
  const close = React.useCallback(
    () => setSelectedTrain(null),
    [setSelectedTrain]
  );
  const select = React.useCallback(
    (
      ...[from, to, leg]: [string, string, PlEduAghDomainJourneyJourneyLegDto]
    ) => {
      const fromIndex = leg.segments.findIndex(
        (x) => x.from.stationName === from
      );
      const toIndex = leg.segments.findIndex((x) => x.from.stationName === to);
      const data = leg.segments.slice(fromIndex, toIndex);
      // if (fromIndex !== -1 && toIndex !== -1) // Commented for diagnostic purposes, when last element is clicked all seats are visible
      setSelectedTrain(data);
    },
    [setSelectedTrain]
  );
  return (
    <>
      <Alert
        canEscapeKeyCancel={true}
        canOutsideClickCancel={true}
        isOpen={selectedTrain != null}
        className={Classes.OVERLAY_SCROLL_CONTAINER}
        onClose={close}
        confirmButtonText="Zamknij"
      >
        {selectedTrain?.map((s) => (
          <div>
            {s.train.cars.map((x) => (
              <RenderCar
                car={x}
                booked={
                  journey.legs.find((j) => j.trainName === s.train.name)
                    ?.seats ?? []
                }
              />
            ))}
          </div>
        ))}
      </Alert>
      <VerticalTimeline layout="1-column">
        {journey.legs.map((leg) => {
          const randomColor =
            "#" + (leg.trainName.charCodeAt(2) * 16777215).toString(16);
          return leg.stops.map((stop, i, arr) => (
            <VerticalTimelineElement
              iconOnClick={() => select(stop.station, arr[i + 1]?.station, leg)}
              className="vertical-timeline-element--work"
              iconStyle={{ background: randomColor, color: "#fff" }}
            >
              {i === 0 && (
                <>
                  <h4>Pociąg: {leg.trainName}</h4>
                  <b>Miejsca</b>
                  <ul>
                    {leg.seats.map((l) => (
                      <li>
                        Miejsce numer <b>{l.seatId}</b> w wagonie{" "}
                        <b>{l.carId}</b>{" "}
                      </li>
                    ))}
                  </ul>
                </>
              )}

              <h3>{stop.station}</h3>
              <h4>{formatDateString(stop.arrivalTime)}</h4>
              <h4>{formatDateString(stop.departureTime)}</h4>
            </VerticalTimelineElement>
          ));
        })}
      </VerticalTimeline>
    </>
  );
}
