import * as React from "react";

import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/datetime/lib/css/blueprint-datetime.css";
import "@blueprintjs/select/lib/css/blueprint-select.css";
import "@blueprintjs/table/lib/css/table.css";
import { Card, Elevation } from "@blueprintjs/core";

import {LoginButton} from "./auth/Button";

export const App = () => (
  <div>
    <div className="flex">
      <div className="col">
        <div className="flex" style={{ paddingBottom: "20px" }} >
          <Card interactive={true} elevation={Elevation.TWO} >
            Z Nami zawsze dojedziesz do celu
          </Card>
        </div>
        <Card interactive={true} elevation={Elevation.TWO} style={{ paddingBottom: "20px" }}>
          <img alt="header" src={`https://images.pexels.com/photos/72594/japan-train-railroad-railway-72594.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=9404`} />
        </Card>
        <LoginButton />
      </div>
    </div>
  </div>
);

export default App;