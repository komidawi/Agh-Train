import React from "react";
import SwaggerUI from "swagger-ui-react";
import { useApi } from "./useApiConfigurationAsync";
import {
  TokenProvider,
} from "client";
import "swagger-ui-react/swagger-ui.css";

function isTokenProvider(o?: any): o is TokenProvider {
  return o != null && ("getToken" as keyof TokenProvider) in o;
}

function getToken(o?: any): Promise<string> | string | undefined {
  if (o != null && "tokenProvider" in o) {
    const tokenProvider = o.tokenProvider;
    if (isTokenProvider(tokenProvider)) {
      return tokenProvider.getToken();
    }
  }
}

export function SwaggerUi(p: any) {
  const apiConfiguration = useApi();

  return apiConfiguration ? (
    <SwaggerUI
      url="http://localhost:8080/openapi.json"
      requestInterceptor={async (x) => {
        const token = await getToken(apiConfiguration.authMethods.jwtAuth);
        x.headers["authorization"] = "Bearer " + token;
        return x;
      }}
    />
  ) : (
    <h2>Token missing</h2>
  );
}
