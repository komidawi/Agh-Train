import * as React from "react";

import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/datetime/lib/css/blueprint-datetime.css";
import "@blueprintjs/select/lib/css/blueprint-select.css";
import "@blueprintjs/table/lib/css/table.css";
import { Button, Card, Classes, Icon, Label, Menu, MenuDivider, MenuItem, Position, Spinner, Tooltip, } from "@blueprintjs/core";
import './index.css'
import { useState } from "react";
import { DefaultPreferences } from "./../traveler/Preferences"

enum ReservationStatus {
  Past,
  Paid,
  Waiting,
  AbortedByUser,
  Delayed
}

type Reservation = {
  number: string,
  startStation: string,
  endStation: string,
  departureDate: Date,
  status: ReservationStatus
}

const Details = (p: { reservation: Reservation }) => {

  return <div>
    <Card interactive={true}>Number: <h3>{p?.reservation?.number}</h3></Card>
    <Card interactive={true}>Stacja początkowa: <h3>{p?.reservation?.startStation}</h3></Card>
    <Card interactive={true}>Stacja końcowa: <h3>{p?.reservation?.endStation}</h3></Card>
    <Card interactive={true}>Data odjazdu: <h3>{p?.reservation?.departureDate?.toString()}</h3></Card>
    <Card interactive={true}><Button icon="warning-sign" intent="danger" >Anuluj</Button></Card>
  </div>

}

export const App = () => {
  const [selected, setSelected] = useState(null as (Reservation | null));

  return (
    <div>

      <div className="flex row-top" >
        <div className="flex col" style={{ width: "200px" }}>
          <Menu className={Classes.ELEVATION_1}>
            <MenuItem icon="train" text="Zarządzaj rezerwacjami" />
            <MenuItem icon="walk" text="Zarządzaj podróżą" />
            <MenuDivider />
            <MenuItem icon="error" text="Komunikaty" />
            <MenuDivider />
            <MenuItem icon="cog" labelElement={<Icon icon="share" />} text="Ustawienia..." />
            <MenuItem icon="log-out" text="Wyloguj" />
          </Menu>
        </div>
        <div className="flex col">
          <Label>Rezerwacje</Label>
          <Tooltip content="Kliknij aby zobaczyć szczegóły" position={Position.RIGHT}>
            <table className="bp3-html-table bp3-html-table-striped bp3-interactive">
              <thead>
                <tr>
                  <th>Numer rezerwacji</th>
                  <th>Stacja początkowa</th>
                  <th>Stacja docelowa</th>
                  <th>Data odjazdu</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                <tr onClick={() => {
                  setSelected({
                    departureDate: new Date(), endStation: "RZE-02", startStation: "KRK-01", number: "0000", status: ReservationStatus.Past
                  })
                }}>
                  <td>00000</td>
                  <td>KRK-01</td>
                  <td>RZE-02</td>
                  <td>25 grudnia 2020</td>
                  <td>Odbyte</td>
                </tr>
                <tr>
                  <td>00001</td>
                  <td>KRK-02</td>
                  <td>KAT-02</td>
                  <td>25 stycznia 2021</td>
                  <td>Zapłacone</td>
                </tr>
                <tr>
                  <td>00002</td>
                  <td>KAT-03</td>
                  <td>WAR-02</td>
                  <td>27 stycznia 2021</td>
                  <td>Oczekująca płatność</td>
                </tr>
              </tbody>
            </table>
          </Tooltip>
          <div>
            <h2>Domyślne preferencje</h2>
            <DefaultPreferences />
          </div>
        </div>

        <div className="flex">
          {
            selected ? <Details reservation={selected} /> : <Spinner />
          }
        </div>
      </div>
    </div>
  );
};

export default App;
