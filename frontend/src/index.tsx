import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/datetime/lib/css/blueprint-datetime.css";
import "@blueprintjs/select/lib/css/blueprint-select.css";
import "@blueprintjs/table/lib/css/table.css";

import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import { Auth0Provider } from "@auth0/auth0-react";
import * as H from 'history';
import { OrdersContextProvider } from './user/OrdersContext'
import { PaymentMethodProvider } from './user/PaymentContext'
import { ConfigureRoutes } from "./ConfigureRoutes";

export const history = H.createBrowserHistory();

ReactDOM.render(
  <React.StrictMode>
    <Auth0Provider
      domain="dev-mattp.eu.auth0.com"
      clientId="jUMMOp4wfBjW0iq6t7glJsd80MCCKa7X"
      audience="http://localhost:8080/"
      scope="read:current_user update:current_user_metadata openid"
      redirectUri={window.location.origin}
      cacheLocation="localstorage"
      useRefreshTokens={true}
    >
      <OrdersContextProvider>
        <PaymentMethodProvider>
          <ConfigureRoutes />
        </PaymentMethodProvider>
      </OrdersContextProvider>
    </Auth0Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();

