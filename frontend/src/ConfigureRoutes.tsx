import React from "react";
import App from "./App";
import Admin from "./admin";
import Traveler from "./traveler";
import Booking from "./booking";
import { Router, Route, Switch } from "react-router-dom";
import { BOOKING, ADMIN, TRAVELER, HOME, SWAGGER } from './paths';
import { TopNavBar } from './NavbarExample';
import { GetUserRole } from './auth/Profile';
import { history } from "./index";
import { SwaggerUi } from "./SwaggerUi";


export function ConfigureRoutes() {
  const user = GetUserRole();

  return <Router history={history}>
    <TopNavBar />
    <Switch>
      {user === 'Admin' && <Route path={ADMIN}>
        <Admin />
      </Route>}
      <Route path={SWAGGER}>
        <SwaggerUi />
      </Route>
      <Route path={TRAVELER}>
        <Traveler />
      </Route>
      <Route path={BOOKING}>
        <Booking />
      </Route>
      <Route path={HOME}>
        <App />
      </Route>
   
    </Switch>
  </Router>;
}
