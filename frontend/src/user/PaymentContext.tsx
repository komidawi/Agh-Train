import { createContext, PropsWithChildren, useCallback, useContext } from "react";
import React, { useState } from "react";

import { Button, Card, Dialog, InputGroup } from "@blueprintjs/core";
import "@repay/react-credit-card/dist/react-credit-card.css";
import { ReactCreditCard } from "@repay/react-credit-card";
import { useDefaultEndpoint } from "../useApiConfigurationAsync";

const paymentInitial = {
  paymentId: 'Id',
  amount: 0,
  isOpen: false
};
type CombinedPaymentProvider = { get: typeof paymentInitial, set: React.Dispatch<React.SetStateAction<typeof paymentInitial>> }
export const PaymentContext = createContext({ get: paymentInitial } as CombinedPaymentProvider)

export function PaymentMethodProvider(props: PropsWithChildren<{}>) {
  const [get, set] = useState(paymentInitial)
  const paymentId = get.paymentId;
  const [values, setValues] = React.useState({
    name: "",
    number: "",
    expiration: "",
    cvc: ""
  });
  const endpoint = useDefaultEndpoint();
  const save = React.useCallback(async () => endpoint && endpoint.userPaymentPost('', {
    orderId: paymentId,
    autorizationCode: values.name + values.number + values.expiration + values.cvc,
  }
  ), [endpoint, paymentId, values])


  return (
    <PaymentContext.Provider value={{ get, set }}>
      <Dialog
        icon="info-sign"
        isOpen={get.isOpen}
        onClose={() => set(x => ({ ...x, isOpen: false }))}
        title={`Zapłać za: ${get.paymentId}`}
      >
        <Card>
          <InputGroup
            defaultValue={values.name}
            onChange={e => setValues(x => ({ ...x, name: e.target.value }))}
            placeholder="Imię i nazwisko"
            type={"text"}
          />
          <InputGroup
            defaultValue={values.number}
            onChange={e => setValues(x => ({ ...x, number: e.target.value }))}
            placeholder="Numer Karty"
            type={"number"}
          />
          <InputGroup
            defaultValue={values.expiration}
            onChange={e => setValues(x => ({ ...x, expiration: e.target.value }))}
            placeholder="MM/YY"
            type={"text"}
          />
          <InputGroup
            defaultValue={values.cvc}
            onChange={e => setValues(x => ({ ...x, cvc: e.target.value }))}
            placeholder="CVC"
            type={"text"}
          />
          <div style={{
            marginTop: "25px",
            display: 'flex',
            justifyContent: "center",
            alignItems: "center"
          }}>
            <ReactCreditCard  {...values} hasShadow={true} hasRadialGradient={true} />
          </div>
          <Button onClick={() => save() && set(paymentInitial)}>
            Zapłać {get.amount.toFixed(2)} PLN
          </Button>
        </Card>
      </Dialog>
      {props.children}
    </PaymentContext.Provider>
  )
}

export function OpenForPaymentHook() {
  const { set } = useContext(PaymentContext);
  return useCallback(({ paymentId, amount }: { paymentId: string, amount: number }) => set(x => ({ isOpen: true, paymentId, amount })), [set]);
}