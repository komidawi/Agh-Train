import React, {useCallback, useContext, useState} from "react";

import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/datetime/lib/css/blueprint-datetime.css";
import "@blueprintjs/select/lib/css/blueprint-select.css";
import "@blueprintjs/table/lib/css/table.css";

import {Button, Card, Drawer, Icon} from "@blueprintjs/core";
import {StopChain} from "../traveler/StopChain";
import {OrdersContext, RefreshOrdersHook} from "./OrdersContext";
import {OpenForPaymentHook} from "./PaymentContext";
import {useDefaultEndpoint} from "../useApiConfigurationAsync";

export function UserOrdersInner() {
  const [version, setVersion] = useState(0);
  const ctx = useContext(OrdersContext);
  RefreshOrdersHook(version);
  const openFor = OpenForPaymentHook();

  const endpoint = useDefaultEndpoint();
  const cancel = useCallback(async (id: string) => {
    if (endpoint) {
      await endpoint.userCancelPost(id, {});
      setVersion(x => x + 1)
    }
  }, [endpoint, setVersion])

  if (ctx.get.orders.length) {
    return <div >
      <h2 className="bp3-heading">Rezerwacja</h2>
      {
        ctx.get.orders.map((x, i) => <div>
          <Card key={i}>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <Icon icon='tick' />
              <StopChain journey={x.journey} />
              <h3> {x.status} </h3>
              <h4> {x.journey.cost.toFixed(2)} PLN</h4>
            
              {x.status === 'PAID' &&  <Button>Pokaż</Button> }
              {x.status === 'STARTED' && <Button onClick={() => openFor({ paymentId: x.id, amount: x.journey.cost })}>Opłać</Button> }
              {['STARTED', 'PAID'].includes(x.status) && <Button onClick={() => cancel(x.id)}>Anuluj</Button> }
            </div>
          </Card>
        </div>)
      }
    </div>
  }
  else {
    return <h2>Brak rezerwacji</h2>
  }

}

export function UserOrders() {
  const ctx = useContext(OrdersContext);

  return <Drawer isOpen={ctx.get.isOpen} style={{ overflowY: 'scroll' }} onClose={() => ctx.set(x => ({ ...x, isOpen: false }))}>
    <h1 className="bp3-heading">Rezerwacje</h1>
    <UserOrdersInner />
  </Drawer>
}