import { createContext, PropsWithChildren, useContext } from "react";
import { PlEduAghDomainUserOrdersUserOrderResponse } from "client";
import { useEffect } from "react";
import React, { useState } from "react";

import { useApi } from "../useApiConfigurationAsync";

import { DefaultApi } from "client";

const initial = {
  orders: [] as Array<PlEduAghDomainUserOrdersUserOrderResponse>,
  isOpen: false
};
type CombinedProvider = { get: typeof initial, set: React.Dispatch<React.SetStateAction<typeof initial>> }

export const OrdersContext = createContext({ get: initial } as CombinedProvider)

export const OrdersContextProvider = (props: PropsWithChildren<{}>) => {
  const [get, set] = useState(initial)

  return (
    <OrdersContext.Provider value={{ get, set }}>
      {props.children}
    </OrdersContext.Provider>
  )
}

export function RefreshOrdersHook(refreshOn = 0) {
  const apiConfiguration = useApi();
  const { set } = useContext(OrdersContext);
  useEffect(() => {
    (async () => {
      if (apiConfiguration) {
        const orders = await new DefaultApi(apiConfiguration).userOrdersGet();
        set(x => ({ ...x, orders: orders.orders }));
      }
    })()
  }, [apiConfiguration, set, refreshOn]);
}