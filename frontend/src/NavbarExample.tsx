/*
 * Copyright 2017 Palantir Technologies, Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as React from "react";

import {Alignment, Button, Classes, Navbar, NavbarDivider, NavbarGroup, NavbarHeading} from "@blueprintjs/core";
import logo from './logo.png';
import {GetUserRole, Profile} from './auth/Profile';
import {ADMIN, BOOKING, HOME, SWAGGER, TRAVELER} from './paths'
import {useHistory} from "react-router-dom";
import {UserOrders} from "./user/UserOrders";
import {OrdersContext} from "./user/OrdersContext";

export interface INavbarExampleState {
  alignRight: boolean;
}
export const TopNavBar = () => {
  let history = useHistory();
  const ctx = React.useContext(OrdersContext);
  const user = GetUserRole();

  return (
    <><Navbar>
      <NavbarGroup align={Alignment.LEFT}>
        <img alt="res" src={logo} className="responsive" />
        <NavbarHeading>Agh Train</NavbarHeading>
        <NavbarDivider />
        <Button onClick={() => history.push(HOME)} className={Classes.MINIMAL} icon="home" />
        {user === 'Admin' && <Button onClick={() => history.push(SWAGGER)} className={Classes.MINIMAL} icon="airplane" text="API" />}
        {user === 'Admin' && <Button onClick={() => history.push(ADMIN)} className={Classes.MINIMAL} icon="route" text="Reguły" />}
        <Button onClick={() => history.push(TRAVELER)} className={Classes.MINIMAL} icon="train" text="Rozkłady" />
        <Button onClick={() => history.push(BOOKING)} className={Classes.MINIMAL} icon="help" text="Pomoc" />
        <Button onClick={() => ctx.set(x => ({ ...x, isOpen: true }))} className={Classes.MINIMAL} icon="badge" text="Rezerwacje" />
        <NavbarDivider />
        <Profile />
      </NavbarGroup>
    </Navbar>
      <UserOrders />
    </>
  );
}