package app

import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

// Common LocalDateTime<>Date type conversions

fun Date.toLocalDateTime(): LocalDateTime =
    this.toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDateTime()

fun LocalDateTime.toDate(): Date =
    Date.from(
        this.atZone(ZoneId.systemDefault())
            .toInstant()
    )