package app

import domain.historical.StationsProvider
import domain.historical.TrainsLoader
import domain.historical.TrainsLoaderImpl
import domain.journey.JourneyService
import domain.journey.JourneyServiceImpl
import domain.journey.cost.JourneyCostCalculator
import domain.journey.cost.JourneyCostCalculatorImpl
import domain.journey.cost.SegmentCostProvider
import domain.journey.cost.SegmentCostProviderImpl
import domain.raptor.RaptorAlgorithmFactoryImpl
import domain.raptor.TrainTripFactoryImpl
import domain.raptor.preprocessing.TrainTripsProcessingStepsFactoryImpl
import domain.rules.RulesService
import domain.rules.RulesServiceImpl
import domain.train.TrainFactoryImpl
import domain.train.bitmap.*
import domain.train.car.CarFactory
import domain.train.car.CarFactoryImpl
import domain.train.repository.CarRepository
import domain.train.repository.InMemoryCarRepositoryImpl
import domain.user.PerUserService
import domain.user.orders.ReservationService
import domain.user.orders.ReservationServiceImpl
import domain.user.preferences.Preferences

object DI {
    val reservationService: ReservationService = ReservationServiceImpl()
    val preferencesService = PerUserService<Preferences>()
    val csvParser: CSVParser = CSVParserImpl()
    val seatMapperImpl = SeatMapperImpl()
    val simpleFieldMapper = SimpleFieldMapper()
    val fieldMapper: FieldMapper = FieldMapperImpl()
    val fieldFactory: FieldFactory = FieldFactoryImpl(listOf(seatMapperImpl, simpleFieldMapper, fieldMapper))
    val fieldMatrixMapper: FieldMatrixMapper = FieldMatrixMapperImpl(fieldFactory)
    val fieldMatrixFactory: FieldMatrixFactory = FieldMatrixFactoryImpl(csvParser, fieldMatrixMapper)
    private val trainsLoaderImpl = TrainsLoaderImpl()
    val trainsLoader: TrainsLoader = trainsLoaderImpl
    val stationsProvider: StationsProvider = trainsLoaderImpl
    val rulesService: RulesService = RulesServiceImpl()
    val carRepository: CarRepository = InMemoryCarRepositoryImpl()
    val carFactory: CarFactory = CarFactoryImpl(fieldMatrixFactory)
    val trainFactory = TrainFactoryImpl(carFactory)
    val trainTripFactory = TrainTripFactoryImpl(trainFactory)
    private val raptorAlgorithmFactory = RaptorAlgorithmFactoryImpl()
    private val trainTripsProcessingStepsFactory = TrainTripsProcessingStepsFactoryImpl()
    private val segmentCostProvider: SegmentCostProvider = SegmentCostProviderImpl()
    private val journeyCostCalculator: JourneyCostCalculator = JourneyCostCalculatorImpl(segmentCostProvider)
    val journeyService: JourneyService =
        JourneyServiceImpl(
            trainsLoader,
            trainTripFactory,
            raptorAlgorithmFactory,
            trainTripsProcessingStepsFactory,
            reservationService,
            journeyCostCalculator
        )
}