package app

import com.fasterxml.jackson.core.util.DefaultIndenter
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.papsign.ktor.openapigen.OpenAPIGen
import domain.journey.guardNotNull
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import network.jwt.jwtAudience
import network.jwt.jwtIssuer
import network.jwt.jwtRealm
import network.jwt.makeJwkProvider

fun <T> T.toJson(): String = Installers.objectMapper
    .guardNotNull("Object mapper is not initialized")
    .writeValueAsString(this)


object Installers {
    var objectMapper = ObjectMapper();
    fun Application.installJwt() {
        install(Authentication) {
            jwt {
                realm = jwtRealm
                verifier(makeJwkProvider(), jwtIssuer)
                validate { credentials ->
                    if (credentials.payload.audience.contains(jwtAudience))
                        JWTPrincipal(credentials.payload)
                    else
                        null
                }
            }
        }
    }

    fun Application.installJackson() {
        install(ContentNegotiation) {
            jackson {
                enable(SerializationFeature.INDENT_OUTPUT)
                disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)

                setDefaultPrettyPrinter(DefaultPrettyPrinter().apply {
                    indentArraysWith(DefaultPrettyPrinter.FixedSpaceIndenter.instance)
                    indentObjectsWith(DefaultIndenter("  ", "\n"))
                })
                registerModule(JavaTimeModule())
                objectMapper = this
            }
        }
    }

    fun Application.installCors() {
        this.install(CORS) {
            method(HttpMethod.Options)
            header(HttpHeaders.XForwardedProto)
            header(HttpHeaders.Authorization)
            anyHost()
            allowCredentials = true
            allowNonSimpleContentTypes = true
        }
    }

    fun Application.installOpenApiGen() {
        this.install(OpenAPIGen)
        {
            info {
                version = "0.0.1"
                title = "Agh.Train"
                description = "AghTrainApi"
                contact {
                    name = "No niestety nie prowadzimy supportu"
                    email = "agh@no-reply.com"
                }
            }
            server("http://localhost:8080/") {
                description = "Agh.Train"
            }
        }
    }
}
