package network

import com.fasterxml.jackson.databind.ObjectMapper
import com.papsign.ktor.openapigen.openAPIGen
import io.ktor.application.*
import java.io.File

object OpenApiSerializer {

    private const val filePath = "generated/OpenApi.json"

    fun serializeToFile(application: Application) {
        val apiJson = prepareApi(application)
        writeFile(apiJson)
    }

    private fun prepareApi(application: Application): String {
        val generatedApi = application.openAPIGen.api.serialize()
        val mapper = ObjectMapper().writerWithDefaultPrettyPrinter()
        return mapper.writeValueAsString(generatedApi)
    }

    private fun writeFile(apiJson: String) {
        File(filePath).apply {
            parentFile.mkdirs()
            bufferedWriter().use { it.write(apiJson) }
        }
    }
}