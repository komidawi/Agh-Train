package network

import app.DI
import com.papsign.ktor.openapigen.annotations.parameters.QueryParam
import com.papsign.ktor.openapigen.openAPIGen
import com.papsign.ktor.openapigen.route.apiRouting
import com.papsign.ktor.openapigen.route.path.auth.OpenAPIAuthenticatedRoute
import com.papsign.ktor.openapigen.route.path.normal.NormalOpenAPIRoute
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import network.jwt.JwtProvider
import domain.historical.load as loadHistoricalModule
import domain.journey.load as loadJourneysModule
import domain.rules.load as loadRulesModule
import domain.user.orders.load as loadOrdersModule
import domain.user.preferences.load as loadPreferencesModule

data class NullRequest(@QueryParam("", allowEmptyValues = true) val id: String)

data class NullResponse(val isOk: Boolean = true)

object Routing {

    fun Application.configureRouting() {
        configureBusinessRouting()
        configureApiRouting()
    }

    private fun Application.exposePipelineExceptions() {
        receivePipeline.intercept(ApplicationReceivePipeline.Before) {
            withContext(Dispatchers.IO) {
                val proceed = proceed()
                print(proceed)
            }
        }
    }

    private fun Application.configureBusinessRouting() {
        exposePipelineExceptions()
        apiRouting {
            auth {
                loadPreferencesModule(DI.preferencesService)
                loadOrdersModule(DI.reservationService)
                loadHistoricalModule(DI.stationsProvider)
                loadJourneysModule(DI.journeyService, DI.rulesService)
                loadRulesModule(DI.rulesService)
            }
        }
    }

    private inline fun NormalOpenAPIRoute.auth(route: OpenAPIAuthenticatedRoute<JWTPrincipal>.() -> Unit): OpenAPIAuthenticatedRoute<JWTPrincipal> {
        val authenticatedKtorRoute = this.ktorRoute.authenticate { }
        val openAPIAuthenticatedRoute =
            OpenAPIAuthenticatedRoute(authenticatedKtorRoute, this.provider.child(), authProvider = JwtProvider())
        return openAPIAuthenticatedRoute.apply {
            route()
        }
    }

    private fun Application.configureApiRouting() {
        routing {
            get("/openapi.json") {
                call.respond(application.openAPIGen.api.serialize())
            }
            get("/") {
                call.respondRedirect("/swagger-ui/index.html?url=/openapi.json", true)
            }

        }
    }
}
