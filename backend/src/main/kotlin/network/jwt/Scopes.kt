package network.jwt

import com.papsign.ktor.openapigen.model.Described

enum class Scopes(override val description: String) : Described {
    Profile("Some scope")
}