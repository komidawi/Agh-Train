package network.jwt

import com.auth0.jwk.JwkProvider
import com.auth0.jwk.JwkProviderBuilder
import java.util.concurrent.TimeUnit

val jwtRealm: String = "http://localhost:8080/"
val jwtAudience: String = "http://localhost:8080/"
val jwtIssuer: String = "https://dev-mattp.eu.auth0.com/"

fun makeJwkProvider(): JwkProvider =
    JwkProviderBuilder(jwtIssuer)
        .cached(10, 24, TimeUnit.HOURS)
        .rateLimited(10, 1, TimeUnit.MINUTES)
        .build()
