package domain.journey.cost

import domain.journey.Journey

fun interface JourneyCostCalculator {
    operator fun invoke(journey: Journey): Double
}