package domain.journey.cost

import app.JsonMapperInstance
import com.fasterxml.jackson.module.kotlin.readValue
import domain.journey.cost.json.CostRecords
import java.io.File
import java.nio.file.Paths

class SegmentCostProviderImpl : SegmentCostProvider {
    private val costs = run {
        val rootPath = Paths.get("").toAbsolutePath().toString()
        val costFiles = File("$rootPath/src/main/resources/cost-files").listFiles()

        costFiles?.flatMap { file ->
            val costRecords = JsonMapperInstance.readValue<CostRecords>(file)
            costRecords.costsPerCar.flatMap { carCosts ->
                carCosts.costs.map { segmentCost ->
                    SegmentCostId(
                        source = segmentCost.source,
                        destination = segmentCost.destination,
                        trainId = costRecords.trainId,
                        carType = carCosts.carType
                    ) to segmentCost.cost
                }
            }
        }?.associate { it } ?: emptyMap()
    }

    override operator fun invoke(segmentCostId: SegmentCostId): Double {
        val defaultCost = 10.20
        return costs.getOrDefault(segmentCostId, defaultCost)
    }
}