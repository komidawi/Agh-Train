package domain.journey.cost

fun interface SegmentCostProvider {
    operator fun invoke(segmentCostId: SegmentCostId): Double
}