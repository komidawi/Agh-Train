package domain.journey.cost.json

import domain.train.car.CarType

data class CarCosts(val carType: CarType, val costs: List<SegmentCost>)
