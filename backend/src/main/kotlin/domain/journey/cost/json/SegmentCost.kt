package domain.journey.cost.json

data class SegmentCost(val source: String, val destination: String, val cost: Double)
