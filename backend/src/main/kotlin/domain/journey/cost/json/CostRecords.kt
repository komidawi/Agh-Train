package domain.journey.cost.json

data class CostRecords(val trainId: String, val costsPerCar: List<CarCosts>)
