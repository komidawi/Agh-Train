package domain.journey.cost

import domain.journey.BookedSeat
import domain.journey.Journey
import domain.journey.JourneyLeg
import domain.journey.LegStop

class JourneyCostCalculatorImpl(private val segmentCostProvider: SegmentCostProvider) : JourneyCostCalculator {
    override operator fun invoke(journey: Journey) =
        journey.legs.fold(0.0) { partialCost, leg ->
            partialCost + calculateCostOfSingleLeg(leg)
        }

    private fun calculateCostOfSingleLeg(leg: JourneyLeg): Double {
        val segments = leg.stops.zipWithNext()
        val costIds = cartesianProduct(segments, leg.seats)

        return costIds.fold(0.0) { legCost, (segment, seat) ->
            val (source, destination) = segment
            legCost + segmentCostProvider(
                SegmentCostId(
                    source = source.station,
                    destination = destination.station,
                    trainId = leg.trainNumber,
                    carType = seat.carType
                )
            )
        }
    }

    private fun cartesianProduct(segments: List<Pair<LegStop, LegStop>>, seats: List<BookedSeat>) =
        segments.flatMap { segment -> seats.map { segment to it } }
}