package domain.journey.cost

import domain.train.car.CarType

data class SegmentCostId(val source: String, val destination: String, val trainId: String, val carType: CarType)
