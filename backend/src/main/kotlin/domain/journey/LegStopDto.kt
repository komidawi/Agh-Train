package domain.journey

data class LegStopDto(
    val departureTime: String?,
    val arrivalTime: String?,
    val station: String
)