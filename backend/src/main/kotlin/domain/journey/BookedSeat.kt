package domain.journey

import domain.train.car.CarType

data class BookedSeat(val carId: Int, val seatId: Int, val carType: CarType)