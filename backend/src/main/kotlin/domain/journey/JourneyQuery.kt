package domain.journey

import com.papsign.ktor.openapigen.annotations.parameters.QueryParam

data class JourneyQuery(
    @QueryParam("")
    val startStation: String,

    @QueryParam("")
    val endStation: String,

    @QueryParam("")
    val startTimeRange: String,

    @QueryParam("")
    val endTimeRange: String,

    @QueryParam("")
    val isDirect: Boolean,
)

