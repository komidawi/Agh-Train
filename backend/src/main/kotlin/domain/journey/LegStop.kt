package domain.journey

import java.time.LocalDateTime

data class LegStop(
    val departureTime: LocalDateTime?,
    val arrivalTime: LocalDateTime?,
    val station: String
)