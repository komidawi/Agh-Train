package domain.journey

import domain.raptor.Segment

data class JourneyLegDto(
    val trainName: String,
    val trainNumber: String,
    val stops: List<LegStopDto>,
    val seats: List<BookedSeatDto>,
    val segments: List<Segment>
)