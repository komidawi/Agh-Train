package domain.journey

import domain.raptor.Segment

class JourneyLeg(
    val trainName: String,
    val trainNumber: String,
    val stops: List<LegStop>,
    val seats: List<BookedSeat>,
    val segments: List<Segment>
)