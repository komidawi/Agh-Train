package domain.journey

import domain.rules.Rules

fun interface JourneyService {
    fun findJourney(query: JourneyQuery, rules: Rules): Journeys
}