package domain.journey

import com.papsign.ktor.openapigen.route.path.auth.OpenAPIAuthenticatedRoute
import com.papsign.ktor.openapigen.route.path.auth.get
import com.papsign.ktor.openapigen.route.response.respond
import com.papsign.ktor.openapigen.route.route
import com.papsign.ktor.openapigen.route.throws
import domain.rules.RulesService
import io.ktor.auth.jwt.*
import io.ktor.http.*

fun OpenAPIAuthenticatedRoute<JWTPrincipal>.load(journeyService: JourneyService, rulesService: RulesService) {
    route("journey")
        .throws(HttpStatusCode.BadRequest, Exception::class)
        .get<JourneyQuery, JourneysResponse, JWTPrincipal> { journeyQuery ->
            val allJourneys = journeyService.findJourney(journeyQuery, rulesService.getRules())
            respond(allJourneys.toResponse())
        }
}