package domain.journey

import com.papsign.ktor.openapigen.annotations.Response

@Response()
class JourneysResponse(
    val journeys: List<JourneyResponse>
)