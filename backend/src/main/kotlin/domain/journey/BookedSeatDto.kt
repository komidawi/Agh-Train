package domain.journey

import domain.train.car.CarType

data class BookedSeatDto(val carId: Int, val seatId: Int, val carType: CarType)