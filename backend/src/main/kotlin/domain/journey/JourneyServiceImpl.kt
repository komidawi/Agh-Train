package domain.journey

import domain.historical.Root
import domain.historical.TrainsLoader
import domain.journey.cost.JourneyCostCalculator
import domain.raptor.RaptorAlgorithmFactory
import domain.raptor.TrainTrip
import domain.raptor.TrainTripFactory
import domain.raptor.preprocessing.Reservations
import domain.raptor.preprocessing.TrainTripsProcessingStep
import domain.raptor.preprocessing.TrainTripsProcessingStepsFactory
import domain.rules.Rules
import domain.user.orders.ReservationService
import java.time.LocalDateTime

class JourneyServiceImpl(
    private val trainsLoader: TrainsLoader,
    private val trainTripFactory: TrainTripFactory,
    private val raptorAlgorithmFactory: RaptorAlgorithmFactory,
    private val processingStepsFactory: TrainTripsProcessingStepsFactory,
    private val reservationsService: ReservationService,
    private val journeyCostCalculator: JourneyCostCalculator
) : JourneyService {
    override fun findJourney(query: JourneyQuery, rules: Rules): Journeys {
        val startDateTime = query.startTimeRange.convertDateTime().guardNotNull()
        val endDateTime = query.endTimeRange.convertDateTime().guardNotNull()
        if (startDateTime.isAfter(endDateTime)) {
            throw IllegalArgumentException("Time range is incorrect!")
        }

        if (query.startStation == query.endStation) {
            return Journeys()
        }

        return getJourneysViaRaptorAlgorithm(
            query.startStation,
            query.endStation,
            startDateTime,
            endDateTime,
            query.isDirect,
            rules,
            reservationsService.getActiveReservationsBetween(startDateTime, endDateTime)
        )
    }

    private fun getJourneysViaRaptorAlgorithm(
        source: String,
        destination: String,
        startDateTime: LocalDateTime,
        endDateTime: LocalDateTime,
        isDirect: Boolean,
        rules: Rules,
        reservations: Reservations
    ): Journeys {
        // We are working with historical data, therefore, we must patch the supplied datetime.
        val shiftedStartDateTime = startDateTime.minusYears(1)
        val shiftedEndDateTime = endDateTime.minusYears(1)

        val trainsData = trainsLoader.accept(shiftedStartDateTime, shiftedEndDateTime)
        val processingSteps =
            processingStepsFactory.createProcessingSteps(source, destination, isDirect, rules, reservations)
        val trainTrips = prepareTrips(trainsData, processingSteps)

        val maxCountOfJourneys = 10
        val raptor = raptorAlgorithmFactory.create()
        val noCostJourneys =
            raptor.getKBestJourneys(trainTrips, maxCountOfJourneys, source, destination, shiftedStartDateTime)

        return assignCostToJourneys(noCostJourneys)
    }

    private fun prepareTrips(trainsData: List<Root>, steps: List<TrainTripsProcessingStep>): List<TrainTrip> {
        val trainTrips = buildTripsFromTrainsData(trainsData)
        return steps.fold(trainTrips) { trips, processingStep -> processingStep(trips) }
    }

    private fun buildTripsFromTrainsData(trainsData: List<Root>) =
        trainsData.flatMap { root ->
            root.schedules.map { schedule ->
                trainTripFactory.create(root.train_id.toString(), root.train_name, schedule)
            }
        }

    private fun assignCostToJourneys(noCostJourneys: Journeys) =
        Journeys(noCostJourneys.journeys.map {
            Journey(it.legs, journeyCostCalculator(it))
        })
}