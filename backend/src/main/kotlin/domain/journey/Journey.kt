package domain.journey

import java.time.LocalDateTime

class Journey(val legs: List<JourneyLeg>, val cost: Double = -1.0) {
    fun hasAnyStopBetween(from: LocalDateTime, to: LocalDateTime): Boolean {
        if (from.isAfter(to)) {
            return false
        }

        return legs.any { leg ->
            leg.stops.any { stop ->
                val isFirstStopInJourney = stop.arrivalTime == null
                val isArrivalTimeInRange = isFirstStopInJourney || from.isBefore(stop.arrivalTime)

                val isLastStopInJourney = stop.departureTime == null
                val isDepartureTimeInRange = isLastStopInJourney || to.isAfter(stop.departureTime)

                isArrivalTimeInRange && isDepartureTimeInRange
            }
        }
    }
}