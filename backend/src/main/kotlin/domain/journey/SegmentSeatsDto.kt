package domain.journey

import domain.raptor.Segment
import domain.train.bitmap.Coord
import domain.train.bitmap.Field
import domain.train.bitmap.TMatrix
import domain.train.car.Car

/*  Póki co zostawiam ten kod, bo może się przydać, jeśli nie będziemy chcieli słać wszystkiego jak leci,
    a jedynie rozsądny subset danych */

data class SegmentSeatsDto(val cars: List<CarDto>)

fun Segment.toSegmentSeatsDto(): SegmentSeatsDto {
    val cars = train.cars.map { it.toDto() }
    return SegmentSeatsDto(cars)
}

data class CarDto(val fieldMatrix: TMatrix<SeatDto>)

fun Car.toDto(): CarDto {
    val seatMatrix = fieldMatrix.map { list -> list.filterIsInstance<Field.Seat>().map { it.toDto() } }
    return CarDto(seatMatrix)
}

data class SeatDto(
    val number: Int,
    val coord: Coord,
    val enabled: Boolean,
    val location: Field.Seat.Location,
    val properties: List<Field.Seat.Property>
)


fun Field.Seat.toDto(): SeatDto = SeatDto(number, coord, enabled, location, properties.toList())