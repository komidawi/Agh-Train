package domain.journey

import com.papsign.ktor.openapigen.annotations.Response
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.reflect.jvm.jvmName

private val isoDate: DateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME

@Response("JourneyResponseDescription")
class JourneyResponse(
    val legs: List<JourneyLegDto>,
    val cost: Double
)

fun Journeys.toResponse() = JourneysResponse(
    journeys = journeys.map { it.toResponse() }
)

fun Journey.toResponse() = JourneyResponse(
    legs = legs.map { leg ->
        JourneyLegDto(
            trainName = leg.trainName,
            trainNumber = leg.trainNumber,
            segments = leg.segments,
            seats = leg.seats.map { BookedSeatDto(it.carId, it.seatId, it.carType) },
            stops = leg.stops.map { (departureTime, arrivalTime, station) ->
                LegStopDto(
                    departureTime = departureTime.convertDateTime(),
                    arrivalTime = arrivalTime.convertDateTime(),
                    station = station
                )
            }
        )
    },
    cost = cost
)

fun JourneyResponse.toDomain() = Journey(
    legs = legs.map {
        JourneyLeg(
            it.trainName,
            it.trainNumber,
            segments = it.segments,
            seats = it.seats.map { BookedSeat(it.carId, it.seatId, it.carType) },
            stops = it.stops.map { (departureTime, arrivalTime, station) ->
                LegStop(
                    departureTime.convertDateTime(),
                    arrivalTime.convertDateTime(),
                    station
                )
            }
        )
    },
    cost = cost
)

inline fun <reified T> T?.guardNotNull(msg: String = "Variable not be null for: " + T::class.jvmName): T =
    requireNotNull(this) { msg }

fun String?.convertDateTime(): LocalDateTime? = this?.let { LocalDateTime.parse(this, isoDate) }

fun LocalDateTime?.convertDateTime(): String? = this?.let { format(isoDate) }
