package domain.train.repository

import domain.train.car.Car
import domain.train.car.CarType

interface CarRepository {

    fun save(car: Car)

    fun get(carType: CarType): Car

}