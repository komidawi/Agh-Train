package domain.train.repository

import domain.train.car.Car
import domain.train.car.CarType

class InMemoryCarRepositoryImpl : CarRepository {

    private val cars = HashMap<CarType, Car>()

    override fun save(car: Car) {
        cars[car.carType] = car
    }

    override fun get(carType: CarType): Car =
        cars[carType] ?: throw IllegalArgumentException("Unable to find data for car type: $carType")
}