package domain.train.car

import domain.train.bitmap.FieldMatrix

data class Car(val carType: CarType, val id: Int, val fieldMatrix: FieldMatrix)

enum class CarType {
    COMPARTMENT_FIRST_CLASS,
    COMPARTMENT_SECOND_CLASS
}