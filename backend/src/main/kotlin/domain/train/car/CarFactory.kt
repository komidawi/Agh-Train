package domain.train.car

import domain.train.bitmap.FieldMatrixFactory
import java.io.File
import java.nio.file.Paths

fun interface CarFactory {
    fun create(carType: CarType, carId: Int): Car
}

class CarFactoryImpl(private val fieldMatrixFactory: FieldMatrixFactory) : CarFactory {

    private val carDataPath = Paths.get("src/main/resources/cardata").toAbsolutePath().toString()

    override fun create(carType: CarType, carId: Int): Car = when (carType) {
        CarType.COMPARTMENT_FIRST_CLASS -> {
            val dataFile = File("$carDataPath/CompartmentFirstClass.csv")
            val fieldMatrix = fieldMatrixFactory.map(dataFile)
            Car(carType, carId, fieldMatrix)
        }
        CarType.COMPARTMENT_SECOND_CLASS -> {
            val dataFile = File("$carDataPath/CompartmentSecondClass.csv")
            val fieldMatrix = fieldMatrixFactory.map(dataFile)
            Car(carType, carId, fieldMatrix)
        }
        else -> throw IllegalArgumentException("Unsupported CarType: $carType")
    }
}