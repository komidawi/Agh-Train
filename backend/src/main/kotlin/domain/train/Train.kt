package domain.train

import domain.train.car.Car

data class Train(val id: String, val name: String, val type: TrainType, val cars: List<Car>)

enum class TrainType {
    T_1130_1_OLENKA,
    T_1310_1_KRAKUS
}