package domain.train

import domain.train.car.CarFactory
import domain.train.car.CarType

fun interface TrainFactory {
    fun create(id: String, name: String): Train
}

class TrainFactoryImpl(private val carFactory: CarFactory) : TrainFactory {

    override fun create(id: String, name: String): Train =
        when (id) {
            "601" -> createOlenka(id, name)
            "2893" -> createKrakus(id, name)
            else -> createDefault(id, name)
        }

    private fun createOlenka(id: String, name: String): Train {
        val carId = 1
        val car = carFactory.create(CarType.COMPARTMENT_FIRST_CLASS, carId)
        return Train(id, name, TrainType.T_1130_1_OLENKA, listOf(car))
    }

    private fun createKrakus(id: String, name: String): Train {
        val carId = 1
        val car = carFactory.create(CarType.COMPARTMENT_SECOND_CLASS, carId)
        return Train(id, name, TrainType.T_1310_1_KRAKUS, listOf(car))
    }

    private fun createDefault(id: String, name: String): Train = createOlenka(id, name)
}
