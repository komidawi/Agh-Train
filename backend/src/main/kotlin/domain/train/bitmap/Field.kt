package domain.train.bitmap

import app.toJson
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.papsign.ktor.openapigen.annotations.type.string.example.DiscriminatorAnnotation

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type",
    visible = true,
)
@JsonSubTypes(
    JsonSubTypes.Type(Field.Seat::class),
    JsonSubTypes.Type(Field.Window::class),
    JsonSubTypes.Type(Field.Wall::class),
    JsonSubTypes.Type(Field.UnknownField::class),
    JsonSubTypes.Type(Field.Corridor::class),
)
@DiscriminatorAnnotation
sealed abstract class Field(open val letterCode: String) {
    override fun toString(): String = letterCode
    abstract fun clone(): Field

    @DiscriminatorAnnotation
    data class UnknownField(override val letterCode: String) : Field(letterCode) {
        override fun clone(): Field = this.copy()
    }

    @DiscriminatorAnnotation
    data class Window(override val letterCode: String) : Field(letterCode) {
        override fun clone(): Field = this.copy()
    }

    @DiscriminatorAnnotation
    data class Wall(override val letterCode: String) : Field(letterCode) {
        override fun clone(): Field = this.copy()
    }

    @DiscriminatorAnnotation
    data class Corridor(override val letterCode: String) : Field(letterCode) {
        override fun clone(): Field = this.copy()
    }

    @DiscriminatorAnnotation
    data class Seat(
        override val letterCode: String,
        val number: Int,
        val location: Location,
        val coord: Coord,
        val direction: Direction,
        val properties: List<Property> = listOf(),
        var enabled: Boolean = true
    ) : Field(letterCode) {
        override fun clone(): Field = this.copy()

        override fun toString(): String = this.toJson()

        enum class Location {
            WINDOW,
            MIDDLE,
            CORRIDOR,
        }

        enum class Direction {
            LEFT,
            RIGHT
        }

        enum class Property {
            POWER_SOCKET,
            RESTAURANT,
            EXTENDED_LEG_SPACE,
            ADJUSTED_FOR_DISABLED
        }
    }

}

