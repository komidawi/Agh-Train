package domain.train.bitmap

typealias TMatrix<T> = List<List<T>>
typealias FieldMatrix = TMatrix<Field>

fun <T> FieldMatrix.mapItems(mapper: (Field) -> T): TMatrix<T> =
    map { row ->
        row.map { item ->
            mapper(item)
        }
    }

public fun interface FieldMatrixMapper {
    fun map(input: ParsedStringMatrix): FieldMatrix
}

class FieldMatrixMapperImpl(private val fieldFactory: FieldFactory) : FieldMatrixMapper {

    override fun map(input: ParsedStringMatrix): FieldMatrix {
        val bitmap = input.reversed()
        val width = bitmap[0].size
        val height = bitmap.size

        val transposed = initializeMatrix(width, height)
        transpose(bitmap, transposed)

        return transposed
    }

    private fun initializeMatrix(width: Int, height: Int): MutableList<MutableList<Field>> =
        MutableList(width) {
            MutableList(height) {
                Field.UnknownField("")
            }
        }

    private fun transpose(bitmap: ParsedStringMatrix, transposed: MutableList<MutableList<Field>>) {
        bitmap.forEachIndexed { y, row ->
            row.forEachIndexed { x, description ->
                transposed[x][y] = fieldFactory.create(description, Coord(x, y))
            }
        }
    }
}

