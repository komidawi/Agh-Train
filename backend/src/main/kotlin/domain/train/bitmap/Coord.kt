package domain.train.bitmap

data class Coord(val x: Int, val y: Int)