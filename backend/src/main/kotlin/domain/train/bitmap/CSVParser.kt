package domain.train.bitmap

import java.io.File

typealias ParsedStringMatrix = List<List<String>>

class CSVParserImpl : CSVParser {

    override fun parse(file: File): ParsedStringMatrix {
        val readLines = file.readLines()
        return parseInternal(readLines)
    }

    override fun parse(input: String): ParsedStringMatrix {
        val readLines = input.split('\n')
        return parseInternal(readLines)
    }

    private fun parseInternal(lines: List<String>): ParsedStringMatrix =
        lines.map { it.split(',') }
}


interface CSVParser {
    fun parse(file: File): ParsedStringMatrix
    fun parse(input: String): ParsedStringMatrix
}