package domain.train.bitmap

import java.io.File

class FieldMatrixFactoryImpl(private val parser: CSVParser, private val fieldMatrixMapper: FieldMatrixMapper) :
    FieldMatrixFactory {

    override fun map(file: File): FieldMatrix {
        val parse = parser.parse(file)
        return fieldMatrixMapper.map(parse)
    }

    override fun map(input: String): FieldMatrix {
        val parse = parser.parse(input)
        return fieldMatrixMapper.map(parse)
    }
}

interface FieldMatrixFactory {
    fun map(file: File): FieldMatrix
    fun map(input: String): FieldMatrix
}
