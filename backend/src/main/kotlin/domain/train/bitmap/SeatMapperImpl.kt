package domain.train.bitmap

class SeatMapperImpl : FieldMapper {

    private val seatLetter: Char = 'S'

    private val locationMap: Map<Char, Field.Seat.Location> = mapOf(
        ('M' to Field.Seat.Location.MIDDLE),
        ('W' to Field.Seat.Location.WINDOW),
        ('C' to Field.Seat.Location.CORRIDOR)
    )

    private val positionMap: Map<Char, Field.Seat.Direction> = mapOf(
        ('←' to Field.Seat.Direction.LEFT),
        ('→' to Field.Seat.Direction.RIGHT)
    )

    private val propertyMap: Map<Char, Field.Seat.Property> = mapOf(
        ('⏻' to Field.Seat.Property.POWER_SOCKET),
        ('⏣' to Field.Seat.Property.RESTAURANT),
        ('ɺ' to Field.Seat.Property.EXTENDED_LEG_SPACE),
        ('♿' to Field.Seat.Property.ADJUSTED_FOR_DISABLED)
    )

    override fun tryMap(description: String, coord: Coord): Field? {
        if (!description.contains(seatLetter)) {
            return null
        }
        val location = getRequired(locationMap, description)
        val position = getRequired(positionMap, description)
        val properties = getManyOptional(propertyMap, description)

        val number = extractNumber(description)
        return Field.Seat(description, number, location, coord, position, properties.toList())
    }

    private fun extractNumber(description: String): Int {
        val regex = Regex("""\[(\d+)\]""")
        val matchResult = regex.find(description)
        return matchResult!!.groupValues[1].toInt()
    }
}