package domain.train.bitmap

import domain.journey.guardNotNull
import io.kotest.fp.firstOption
import io.kotest.fp.getOrElse

fun <T> getRequired(map: Map<Char, T>, description: String): T =
    map.filter { description.contains(it.key) }
        .values
        .firstOption()
        .getOrElse { throw IllegalStateException("Missing mapping for `$description`, must contain any of: ${map.keys}") }

fun <T> getOptional(map: Map<Char, T>, description: String, fallback: T): T =
    map.filter { description.contains(it.key) }
        .values
        .firstOption()
        .getOrElse { fallback }

fun <T> getManyOptional(map: Map<Char, T>, description: String): Set<T> =
    description.map { char -> map[char] }.filterNotNull().toSet()

open class FieldFactoryImpl(private val mappers: List<FieldMapper>) : FieldFactory {
    override fun create(description: String, coord: Coord): Field {
        return mappers.map { it.tryMap(description, coord) }
            .first { it != null }
            .guardNotNull("$description is not matching with any mapper.")
    }
}

fun interface FieldFactory {
    fun create(description: String, coord: Coord): Field
}