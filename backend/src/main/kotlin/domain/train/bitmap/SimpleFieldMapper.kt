package domain.train.bitmap

class SimpleFieldMapper : FieldMapper {
    private var typeMap = mapOf<Char, (String) -> Field>(
        ('|' to { Field.Wall(it) }),
        ('_' to { Field.Corridor(it) }),
        ('+' to { Field.Window(it) })
    )

    override fun tryMap(description: String, coord: Coord): Field? {
        val resolved = getOptional(typeMap, description, null);
        if (resolved != null) {
            return resolved(description)
        }
        return null;
    }
}