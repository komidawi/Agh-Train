package domain.train.bitmap

class FieldMapperImpl : FieldMapper {
    override fun tryMap(description: String, coord: Coord): Field {
        return Field.UnknownField(description)
    }
}