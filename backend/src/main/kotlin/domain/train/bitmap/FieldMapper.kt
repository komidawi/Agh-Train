package domain.train.bitmap

interface FieldMapper {
    fun tryMap(description: String, coord: Coord): Field?
}