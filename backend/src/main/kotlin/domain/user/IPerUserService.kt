package domain.user

interface IPerUserService<TValue> {
    fun get(user: User): TValue?
    fun set(user: User, value: TValue)
}