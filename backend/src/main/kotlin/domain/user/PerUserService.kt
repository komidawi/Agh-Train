package domain.user

class PerUserService<TValue> : IPerUserService<TValue> {
    var map: Map<User, TValue> = mapOf()

    override fun get(user: User): TValue? {
        return map[user]
    }

    override fun set(user: User, value: TValue) {
        map = (map + Pair(user, value))
    }
}