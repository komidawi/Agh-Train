package domain.user.orders

import domain.journey.Journey
import domain.user.User
import java.time.LocalDateTime

interface ReservationService {
    fun getActiveReservationsBetween(from: LocalDateTime, to: LocalDateTime): List<Journey>
    fun getUserOrders(user: User): List<UserOrderResponse>
    fun bookJourney(user: User, journey: Journey): UserOrderResponse
    fun cancelJourney(user: User, orderId: String)
    fun acceptPayment(user: User, orderId: String)
}