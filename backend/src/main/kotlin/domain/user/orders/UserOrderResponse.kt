package domain.user.orders

import domain.journey.JourneyResponse

data class UserOrderResponse(
    val journey: JourneyResponse,
    val status: PaymentStatus,
    val id: String
)