package domain.user.orders

data class UserOrdersResponse(
    val orders: List<UserOrderResponse>
)