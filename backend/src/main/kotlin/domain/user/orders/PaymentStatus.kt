package domain.user.orders

enum class PaymentStatus {
    STARTED,
    PAID,
    CANCELED,
}