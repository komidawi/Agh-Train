package domain.user.orders

import com.papsign.ktor.openapigen.annotations.parameters.QueryParam
import com.papsign.ktor.openapigen.route.path.auth.OpenAPIAuthenticatedRoute
import com.papsign.ktor.openapigen.route.path.auth.get
import com.papsign.ktor.openapigen.route.path.auth.post
import com.papsign.ktor.openapigen.route.response.respond
import com.papsign.ktor.openapigen.route.route
import domain.journey.JourneyResponse
import domain.journey.toDomain
import io.ktor.auth.jwt.*
import network.NullRequest
import network.NullResponse
import network.jwt.currentUser

data class PaymentCommand(val orderId: String, val autorizationCode: String)
class NullBody()
data class BookCommand(val journey: JourneyResponse)
data class IdRequest(@QueryParam("") val id: String)

fun OpenAPIAuthenticatedRoute<JWTPrincipal>.load(userOrders: ReservationService) {
    route("user") {
        route("orders") {
            get<OrdersQuery, UserOrdersResponse, JWTPrincipal> {
                val orders = userOrders.getUserOrders(currentUser())
                respond(UserOrdersResponse(orders))
            }
        }
        route("book") {
            post<NullRequest, UserOrdersResponse, BookCommand, JWTPrincipal> { _, r ->
                val booked = userOrders.bookJourney(currentUser(), r.journey.toDomain())
                respond(UserOrdersResponse(listOf(booked)))
            }
        }
        route("cancel") {
            post<IdRequest, NullResponse, NullBody, JWTPrincipal> { it, _ ->
                userOrders.cancelJourney(currentUser(), it.id)
                respond(NullResponse())
            }
        }
        route("payment") {
            post<NullRequest, NullResponse, PaymentCommand, JWTPrincipal> { _, body ->
                userOrders.acceptPayment(currentUser(), body.orderId)
                respond(NullResponse())
            }
        }
    }
}