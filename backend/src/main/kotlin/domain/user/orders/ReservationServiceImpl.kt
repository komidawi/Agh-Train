package domain.user.orders

import domain.journey.Journey
import domain.journey.guardNotNull
import domain.journey.toDomain
import domain.journey.toResponse
import domain.raptor.preprocessing.Reservations
import domain.user.PerUserService
import domain.user.User
import java.time.LocalDateTime
import java.util.*

class ReservationServiceImpl : ReservationService {
    private val reservations = PerUserService<List<UserOrderResponse>>()

    override fun getActiveReservationsBetween(from: LocalDateTime, to: LocalDateTime): Reservations {
        return reservations.map.flatMap { (_, userReservations) ->
            userReservations.filter { it.status == PaymentStatus.STARTED || it.status == PaymentStatus.PAID }
                .map { it.journey.toDomain() }
        }.filter { it.hasAnyStopBetween(from, to) }
    }

    override fun getUserOrders(user: User): List<UserOrderResponse> {
        return reservations.get(user) ?: listOf()
    }

    override fun bookJourney(user: User, journey: Journey): UserOrderResponse {
        val newOrder = UserOrderResponse(journey.toResponse(), PaymentStatus.STARTED, UUID.randomUUID().toString())
        val updatedOrders = getUserOrders(user) + newOrder
        reservations.set(user, updatedOrders)

        return newOrder
    }

    override fun cancelJourney(user: User, orderId: String) {
        val order = getUserOrder(user, orderId).guardNotNull("OrderId $orderId was not found for user ${user.id}")

        require(order.status != PaymentStatus.CANCELED) { "Cannot cancel cancelled journey!" }
        val firstStop = order.journey.toDomain().legs.first().stops.first()

        val historicalDepartureTime = firstStop.departureTime.guardNotNull("Departure time cannot be null!")
        val patchedDepartureTime = historicalDepartureTime.plusYears(1)

        require(patchedDepartureTime.isAfter(LocalDateTime.now())) { "Cannot cancel already started journey!" }

        val canceledOrder = order.copy(status = PaymentStatus.CANCELED)
        val updatedOrders = getUserOrders(user).filter { it.id != canceledOrder.id } + canceledOrder

        reservations.set(user, updatedOrders)
    }

    override fun acceptPayment(user: User, orderId: String) {
        val order = getUserOrder(user, orderId).guardNotNull("OrderId $orderId was not found for user ${user.id}")
        require(order.status == PaymentStatus.STARTED) { "Status needs to be started!" }

        val paidOrder = order.copy(status = PaymentStatus.PAID)
        val updatedOrders = getUserOrders(user).filter { it.id != paidOrder.id } + paidOrder

        reservations.set(user, updatedOrders)
    }

    private fun getUserOrder(user: User, orderId: String) = getUserOrders(user).firstOrNull { it.id == orderId }
}