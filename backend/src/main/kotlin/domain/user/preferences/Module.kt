package domain.user.preferences

import com.papsign.ktor.openapigen.route.path.auth.OpenAPIAuthenticatedRoute
import com.papsign.ktor.openapigen.route.path.auth.get
import com.papsign.ktor.openapigen.route.path.auth.post
import com.papsign.ktor.openapigen.route.response.respond
import com.papsign.ktor.openapigen.route.route
import domain.user.PerUserService
import io.ktor.auth.jwt.*
import network.NullRequest
import network.NullResponse
import network.jwt.currentUser

data class PreferencesCommand(
    val preferences: Preferences
)


fun OpenAPIAuthenticatedRoute<JWTPrincipal>.load(preferencesService: PerUserService<Preferences>) {
    route("preferences") {
        route("get") {
            get<PreferencesQuery, Preferences, JWTPrincipal> {
                val preferences = preferencesService.get(currentUser()) ?: Preferences(WithoutTransfers(false))
                respond(preferences)
            }
        }
        route("store") {
            post<NullRequest, NullResponse, PreferencesCommand, JWTPrincipal> { _, (preferences) ->
                preferencesService.set(currentUser(), preferences)
                respond(NullResponse())
            }
        }
    }
}
