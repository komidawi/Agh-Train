package domain.user.preferences

data class WithoutTransfers(val enabled: Boolean)

data class Preferences(
    val withoutTransfers: WithoutTransfers
)