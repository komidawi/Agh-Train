package domain.raptor

import domain.historical.Info
import domain.train.Train

data class Segment(val from: Info, val to: Info, val train: Train)