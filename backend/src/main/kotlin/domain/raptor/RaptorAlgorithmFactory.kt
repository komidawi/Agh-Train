package domain.raptor

interface RaptorAlgorithmFactory {
    fun create(): RaptorAlgorithm
}