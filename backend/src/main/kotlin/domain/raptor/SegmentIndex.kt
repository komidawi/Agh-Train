package domain.raptor

data class SegmentIndex(val value: Int)
