package domain.raptor

import com.raoulvdberge.raptor.InMemoryRaptorFactory
import com.raoulvdberge.raptor.model.*
import domain.journey.JourneyLeg
import domain.journey.Journeys
import domain.journey.LegStop
import java.time.LocalDateTime
import domain.journey.Journey as DomainJourney

typealias RaptorJourney = Journey<TrainStop>

class RaptorAlgorithmImpl : RaptorAlgorithm {

    override fun getKBestJourneys(
        trainTrips: List<TrainTrip>,
        k: Int,
        source: String,
        destination: String,
        startDateTime: LocalDateTime
    ): Journeys {
        var trips = trainTrips.toList()
        val allJourneys = mutableListOf<RaptorJourney>()
        while (allJourneys.size < k) {
            val journeys = getBestJourneys(trips, source, destination, startDateTime)
            if (journeys.isEmpty()) {
                break
            }

            allJourneys.addAll(journeys)
            trips = filterFoundJourneys(trips, journeys)
        }

        return convertJourneys(allJourneys)
    }

    private fun getBestJourneys(
        trips: List<TrainTrip>,
        source: String,
        destination: String,
        startDateTime: LocalDateTime
    ): MutableList<RaptorJourney> {
        return try {
            val dummyTransfers = mutableMapOf<TrainStop, MutableList<TransferLeg<TrainStop>>>()
            val raptor = InMemoryRaptorFactory(trips, dummyTransfers).create()
            raptor.plan(TrainStop(source), TrainStop(destination), startDateTime)
        } catch (e: Exception) {
            println("Connection does not exist! Source: $source, Destination: $destination")
            mutableListOf()
        }
    }

    private fun filterFoundJourneys(trips: List<TrainTrip>, ignoredJourneys: List<Journey<TrainStop>>) =
        trips.filterNot { trip ->
            ignoredJourneys.any { journey ->
                journey.legs.any { leg ->
                    leg as TimetableLeg<TrainStop>
                    leg.trip as TrainTrip
                    leg.trip === trip
                }
            }
        }

    private fun convertJourneys(raptorJourneys: MutableList<RaptorJourney>): Journeys {
        val journeys = raptorJourneys
            .map { journey -> DomainJourney(legs = convertLegs(journey.legs)) }

        return Journeys(journeys)
    }

    private fun convertLegs(legs: MutableList<Leg<TrainStop>>) =
        legs.map { leg ->
            val timetableLeg = leg as TimetableLeg<TrainStop>
            val trip = timetableLeg.trip as TrainTrip
            val stopTimes = trip.getStopTimesBetween(leg.origin, leg.destination)

            JourneyLeg(
                trainName = trip.trainName,
                trainNumber = trip.trainId,
                stops = convertStops(stopTimes),
                seats = trip.getSeatsToBook(),
                segments = trip.segments
            )
        }

    private fun convertStops(stopTimes: List<StopTime<TrainStop>>) =
        stopTimes.mapIndexed { index, stop ->
            // Last stop in a leg should only have arrival time.
            val departureTime = if (index != stopTimes.lastIndex) stop.departureTime else null

            // First stop in a leg should only have departure time.
            val arrivalTime = if (index != 0) stop.arrivalTime else null

            LegStop(
                departureTime = departureTime.normalize(),
                arrivalTime = arrivalTime.normalize(),
                station = stop.stop.name
            )
        }

    private fun LocalDateTime?.normalize(): LocalDateTime? {
        val raptorNullDateFormat = LocalDateTime.MIN
        if (this == null || equals(raptorNullDateFormat)) {
            return null
        }
        return this
    }
}