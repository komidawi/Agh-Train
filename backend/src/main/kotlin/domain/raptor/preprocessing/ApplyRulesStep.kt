package domain.raptor.preprocessing

import domain.raptor.TrainTrip
import domain.raptor.copy
import domain.rules.Rules

class ApplyRulesStep(private val rules: Rules) : TrainTripsProcessingStep {

    override operator fun invoke(trainTrips: List<TrainTrip>): List<TrainTrip> {
        var working = trainTrips.copy()

        rules.globalRules.forEach {
            working = it.apply(working)
        }
        rules.byTrain.forEach {
            working = it.apply(working)
        }

        return working
    }
}