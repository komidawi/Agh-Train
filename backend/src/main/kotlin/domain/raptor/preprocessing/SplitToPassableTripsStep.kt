package domain.raptor.preprocessing

import domain.raptor.Segment
import domain.raptor.SegmentIndex
import domain.raptor.TrainTrip
import domain.train.bitmap.Field

class SplitToPassableTripsStep : TrainTripsProcessingStep {
    override operator fun invoke(trainTrips: List<TrainTrip>) = trainTrips.flatMap { splitSingleTrip(it) }

    private fun splitSingleTrip(trip: TrainTrip): List<TrainTrip> {
        val segmentsPassable = trip.segments.map { isSegmentPassable(it) }
        val segmentsPassableWithSentinel = appendSentinel(segmentsPassable)
        val subTripsRanges = buildSubTripsRanges(segmentsPassableWithSentinel)

        return subTripsRanges.map { (first, lastExclusive) ->
            trip.getSubTripBetween(first, lastExclusive)
        }
    }

    private fun isSegmentPassable(segment: Segment) =
        segment.train.cars.any { car ->
            car.fieldMatrix.flatten().filterIsInstance<Field.Seat>().any { it -> it.enabled }
        }

    private fun appendSentinel(segmentsPassable: List<Boolean>): List<Boolean> {
        val passable = segmentsPassable.toMutableList()
        passable.add(false)

        return passable
    }

    private fun buildSubTripsRanges(segmentsPassable: List<Boolean>): List<Pair<SegmentIndex, SegmentIndex>> {
        val ranges = mutableListOf<Pair<SegmentIndex, SegmentIndex>>()
        var currentFirstIndex = SegmentIndex(0)

        segmentsPassable.zipWithNext().forEachIndexed { pairIndex, passablePair ->
            val nextSegmentIndex = SegmentIndex(pairIndex + 1)
            if (isEndOfSubTrip(passablePair)) {
                ranges.add(currentFirstIndex to nextSegmentIndex)
            } else if (isBeginningOfSubTrip(passablePair)) {
                currentFirstIndex = nextSegmentIndex
            }
        }

        return ranges
    }

    private fun isEndOfSubTrip(passablePair: Pair<Boolean, Boolean>): Boolean {
        return passablePair.first && !passablePair.second
    }

    private fun isBeginningOfSubTrip(passablePair: Pair<Boolean, Boolean>): Boolean {
        return !passablePair.first && passablePair.second
    }
}