package domain.raptor.preprocessing

import domain.rules.Rules

interface TrainTripsProcessingStepsFactory {
    fun createProcessingSteps(
        source: String,
        destination: String,
        isDirect: Boolean,
        rules: Rules,
        reservations: Reservations
    ): List<TrainTripsProcessingStep>
}