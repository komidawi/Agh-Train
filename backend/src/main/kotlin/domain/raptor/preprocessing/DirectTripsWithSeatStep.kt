package domain.raptor.preprocessing

import domain.raptor.AbsoluteSeatId
import domain.raptor.Segment
import domain.raptor.SegmentIndex
import domain.raptor.TrainTrip
import domain.train.bitmap.Field
import java.util.*

class DirectTripsWithSeatStep(private val source: String, private val destination: String) : TrainTripsProcessingStep {

    override operator fun invoke(trainTrips: List<TrainTrip>): List<TrainTrip> {
        val singleLegTrips = getTrimmedSingleLegTrips(trainTrips)
        return getTripsWithSeat(singleLegTrips)
    }

    private fun getTrimmedSingleLegTrips(trainTrips: List<TrainTrip>) = trainTrips.flatMap { trip ->
        val sourceIndex = trip.segments.indexOfFirst { it.from.station_name == source }
        val destinationIndex = trip.segments.indexOfLast { it.to.station_name == destination }

        if (sourceIndex != -1 && destinationIndex != -1 && sourceIndex <= destinationIndex) {
            val first = SegmentIndex(sourceIndex)
            val lastExclusive = SegmentIndex(destinationIndex + 1)
            listOf(trip.getSubTripBetween(first, lastExclusive))
        } else {
            emptyList()
        }
    }

    private fun getTripsWithSeat(singleLegTrips: List<TrainTrip>) =
        singleLegTrips.map { tryToGetSingleTripWithSeat(it) }
            .filter { it.isPresent }
            .map { it.get() }

    private fun tryToGetSingleTripWithSeat(trip: TrainTrip): Optional<TrainTrip> {
        extractSeats(trip.segments).forEach { (carId, seats) ->
            seats.forEach { seat ->
                val absoluteSeatId = AbsoluteSeatId(carId, seat.coord)
                if (isSeatAvailable(absoluteSeatId, trip.segments)) {
                    val first = SegmentIndex(0)
                    val lastExclusive = SegmentIndex(trip.segments.size)
                    val tripWithSeat = trip.getSubTripBetween(first, lastExclusive, absoluteSeatId)

                    return Optional.of(tripWithSeat)
                }
            }
        }
        return Optional.empty()
    }

    private fun extractSeats(segments: List<Segment>): List<Pair<Int, List<Field.Seat>>> {
        return segments.first().train.cars.map { car ->
            car.id to car.fieldMatrix.flatten().filterIsInstance<Field.Seat>()
        }
    }

    private fun isSeatAvailable(absoluteSeatId: AbsoluteSeatId, segments: List<Segment>): Boolean {
        val (carId, seatCoords) = absoluteSeatId
        val (x, y) = seatCoords

        return segments.map { segment ->
            val seat = segment.train.cars.find { it.id == carId }!!.fieldMatrix[x][y] as Field.Seat
            seat.enabled
        }.reduce { lhs, rhs -> lhs && rhs }
    }
}