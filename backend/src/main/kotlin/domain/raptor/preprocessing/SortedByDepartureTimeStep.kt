package domain.raptor.preprocessing

import domain.raptor.TrainTrip

class SortedByDepartureTimeStep : TrainTripsProcessingStep {
    override operator fun invoke(trainTrips: List<TrainTrip>): List<TrainTrip> {
        return trainTrips.sortedBy { it.stopTimes[0].departureTime }
    }
}