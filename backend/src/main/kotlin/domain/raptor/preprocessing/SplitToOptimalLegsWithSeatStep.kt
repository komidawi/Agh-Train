package domain.raptor.preprocessing

import domain.journey.guardNotNull
import domain.raptor.*
import domain.train.bitmap.Coord
import org.jgrapht.GraphPath
import org.jgrapht.alg.shortestpath.BellmanFordShortestPath
import org.jgrapht.graph.DefaultEdge

typealias OptimalLegsPath = GraphPath<Pair<Pair<Int, Coord>, Pair<Int, Int>>, DefaultEdge>

class SplitToOptimalLegsWithSeatStep : TrainTripsProcessingStep {
    override operator fun invoke(trainTrips: List<TrainTrip>) = trainTrips.flatMap { trip ->
        val path = getMinimalPath(trip)
        mapPathToSeparateTrips(path, trip)
    }

    private fun mapPathToSeparateTrips(
        pathWithEphemeralStart: OptimalLegsPath,
        trip: TrainTrip
    ): List<TrainTrip> {
        val vertices = pathWithEphemeralStart.vertexList.subList(1, pathWithEphemeralStart.vertexList.size)
        var nextRangeStart = 0

        return vertices.map { (absoluteSeatId, seatRange) ->
            val first = SegmentIndex(nextRangeStart)
            val lastExclusive = SegmentIndex(seatRange.second + 1)

            nextRangeStart = seatRange.second + 1
            trip.getSubTripBetween(first, lastExclusive, absoluteSeatId)
        }
    }

    private fun getMinimalPath(trip: TrainTrip): OptimalLegsPath {
        val seatAvailabilityRangesBuilder = SeatAvailabilityRangesBuilder()
        val seatAvailabilityRanges = seatAvailabilityRangesBuilder.buildSeatAvailabilityRanges(trip.segments)

        val seatAvailabilityGraphBuilder = SeatAvailabilityGraphBuilder(seatAvailabilityRanges)
        val seatAvailabilityGraph = seatAvailabilityGraphBuilder.buildGraph()

        val bellmanFordShortestPath = BellmanFordShortestPath(seatAvailabilityGraph)
        val shortestPaths = bellmanFordShortestPath.getPaths(SeatAvailabilityGraphBuilder.ephemeralStartVertex)

        val lastSegmentIndex = trip.segments.lastIndex
        val sinks = retrieveSinks(seatAvailabilityRanges, lastSegmentIndex)

        return sinks.mapNotNull { shortestPaths.getPath(it) }
            .minByOrNull { it.length }
            .guardNotNull("Logic error! Split to passable trips before optimal legs!")
    }

    private fun retrieveSinks(
        seatAvailabilityRanges: SeatAvailabilityMap,
        lastSegmentIndex: Int
    ): List<Pair<AbsoluteSeatId, SeatRange>> =
        seatAvailabilityRanges.map { (absoluteSeatId, ranges) -> Pair(absoluteSeatId, ranges.last()) }
            .filter { (_, range) -> range.second == lastSegmentIndex }
}