package domain.raptor.preprocessing

import domain.journey.BookedSeat
import domain.journey.Journey
import domain.journey.JourneyLeg
import domain.journey.guardNotNull
import domain.raptor.Segment
import domain.raptor.TrainTrip
import domain.raptor.copy
import domain.train.bitmap.Field

typealias Reservations = List<Journey>

class ApplyReservationsStep(private val reservations: Reservations) : TrainTripsProcessingStep {

    override operator fun invoke(trips: List<TrainTrip>): List<TrainTrip> {
        val working = trips.copy()
        reservations.forEach { tryApplyReservation(working, it) }

        return working
    }

    private fun tryApplyReservation(trips: List<TrainTrip>, journey: Journey) {
        journey.legs.forEach { leg ->
            trips.find { it.includesLeg(leg) }?.applyReservationFromLeg(leg)
        }
    }

    private fun TrainTrip.includesLeg(leg: JourneyLeg): Boolean {
        if (trainName != leg.trainName || trainId != leg.trainNumber) {
            return false
        }

        val foundStop = stopTimes.find { stopTime ->
            val firstStopInLeg = leg.stops.first()
            stopTime.stop.name == firstStopInLeg.station && stopTime.departureTime == firstStopInLeg.departureTime
        }

        return foundStop != null
    }

    private fun TrainTrip.applyReservationFromLeg(leg: JourneyLeg) {
        val occupiedSegmentsCount = leg.stops.size - 1
        val firstOccupiedSegmentIndex = segments.indexOfFirst { it.from.station_name == leg.stops.first().station }

        for (i in (0 until occupiedSegmentsCount)) {
            val segment = segments[firstOccupiedSegmentIndex + i]
            disableSeatsOnSegment(leg.seats, segment)
        }
    }

    private fun disableSeatsOnSegment(seats: List<BookedSeat>, segment: Segment) {
        seats.forEach { (carId, seatId) ->
            val car = segment.train.cars.find { it.id == carId }
                .guardNotNull("Cannot apply reservation! Car does not exist! CarId = $carId")

            val seat = car.fieldMatrix.flatten().filterIsInstance<Field.Seat>().find { it -> it.number == seatId }
                .guardNotNull("Cannot apply reservation! Field.Seat does not exist! SeatId = $seatId")

            seat.enabled = false
        }
    }
}