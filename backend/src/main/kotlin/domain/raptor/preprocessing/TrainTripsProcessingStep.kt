package domain.raptor.preprocessing

import domain.raptor.TrainTrip

fun interface TrainTripsProcessingStep {
    operator fun invoke(trainTrips: List<TrainTrip>): List<TrainTrip>
}