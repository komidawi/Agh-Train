package domain.raptor.preprocessing

import domain.rules.Rules

class TrainTripsProcessingStepsFactoryImpl : TrainTripsProcessingStepsFactory {
    override fun createProcessingSteps(
        source: String,
        destination: String,
        isDirect: Boolean,
        rules: Rules,
        reservations: Reservations
    ): List<TrainTripsProcessingStep> {
        val beginningSteps = listOf(ApplyRulesStep(rules), ApplyReservationsStep(reservations))
        val endSteps = listOf(SortedByDepartureTimeStep())

        val customSteps = if (isDirect)
            listOf(DirectTripsWithSeatStep(source, destination))
        else
            listOf(SplitToPassableTripsStep(), SplitToOptimalLegsWithSeatStep())

        return beginningSteps + customSteps + endSteps
    }
}