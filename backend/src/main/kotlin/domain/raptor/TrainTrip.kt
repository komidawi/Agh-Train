package domain.raptor

import com.raoulvdberge.raptor.model.StopTime
import com.raoulvdberge.raptor.model.Trip
import domain.journey.BookedSeat
import domain.journey.guardNotNull
import domain.train.bitmap.Coord
import domain.train.bitmap.Field
import domain.train.bitmap.mapItems

typealias StopTimes = List<StopTime<TrainStop>>

data class TrainTrip(
    val trainId: String,
    val trainName: String,
    private val stopTimes: StopTimes,
    val segments: List<Segment>,
    val usedSeatId: AbsoluteSeatId = AbsoluteSeatId(-1, Coord(-1, -1))
) : Trip<TrainStop> {
    override fun getStopTimes() = stopTimes.toList()

    fun getSeatsToBook(): List<BookedSeat> {
        val (carId, seatCoords) = usedSeatId
        val car = segments.first().train.cars.find { it.id == carId }.guardNotNull("Car does not exist! ID = $carId")
        val seat = car.fieldMatrix[seatCoords.x][seatCoords.y] as Field.Seat

        return listOf(BookedSeat(carId, seat.number, car.carType))
    }

    fun getStopTimesBetween(source: TrainStop, destination: TrainStop): StopTimes {
        val firstIndex = stopTimes.indexOfFirst { stopTime -> stopTime.stop == source }
        val lastIndex = stopTimes.indexOfLast { stopTime -> stopTime.stop == destination }

        if (firstIndex == -1 || lastIndex == -1) {
            throw IllegalArgumentException(
                "Either source or destination does not exist! "
                        + "Source = ${source.name}, Destination = ${destination.name}"
            )
        }

        if (firstIndex >= lastIndex) {
            throw IllegalArgumentException("Source must be before destination!")
        }

        return stopTimes.subList(firstIndex, lastIndex + 1)
    }

    fun getSubTripBetween(
        first: SegmentIndex,
        lastExclusive: SegmentIndex,
        seatId: AbsoluteSeatId = usedSeatId
    ): TrainTrip {
        if (first.value < 0 || lastExclusive.value > segments.lastIndex + 1) {
            throw IllegalArgumentException(
                "One of passed indices is out of range!" +
                        "First = ${first.value}, LastExclusive = ${lastExclusive.value}"
            )
        }

        if (first.value >= lastExclusive.value) {
            throw IllegalArgumentException("Source edge must be before destination edge!")
        }

        val subSegments = segments.subList(first.value, lastExclusive.value)
        val firstStop = TrainStop(subSegments.first().from.station_name!!)
        val lastStop = TrainStop(subSegments.last().to.station_name!!)
        val subStopTimes = getStopTimesBetween(firstStop, lastStop)

        return TrainTrip(trainId, trainName, subStopTimes, subSegments, seatId)
    }
}

fun List<TrainTrip>.copy(): List<TrainTrip> {
    return this.map {
        it.copy(
            segments = it.segments.map { segment ->
                segment.copy(train = segment.train.copy(
                    cars = segment.train.cars.map { car ->
                        car.copy(fieldMatrix = car.fieldMatrix.mapItems { item -> item.clone() })
                    }
                ))
            }
        )
    }
}