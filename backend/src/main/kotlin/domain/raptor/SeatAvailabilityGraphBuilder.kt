package domain.raptor

import domain.train.bitmap.Coord
import org.jgrapht.graph.DefaultDirectedGraph
import org.jgrapht.graph.DefaultEdge

typealias SeatAvailabilityGraph = DefaultDirectedGraph<Pair<AbsoluteSeatId, SeatRange>, DefaultEdge>

class SeatAvailabilityGraphBuilder(private val availabilityMap: SeatAvailabilityMap) {
    companion object {
        private val ephemeralSeatId = AbsoluteSeatId(-1, Coord(-1, -1))
        private val ephemeralSeatRange = SeatRange(-1, -1)
        val ephemeralStartVertex = ephemeralSeatId to ephemeralSeatRange
    }

    fun buildGraph(): SeatAvailabilityGraph {
        val graph = SeatAvailabilityGraph(DefaultEdge::class.java)

        graph.addVertex(ephemeralStartVertex)
        graph.addAvailabilityVertices()
        graph.connectEphemeralWithStartVertices()
        graph.connectRemainingVertices()

        return graph
    }

    private fun SeatAvailabilityGraph.addAvailabilityVertices() {
        availabilityMap.forEach { (absoluteSeatId, ranges) ->
            ranges.forEach { range ->
                val newVertex = absoluteSeatId to range
                addVertex(newVertex)
            }
        }
    }

    private fun SeatAvailabilityGraph.connectEphemeralWithStartVertices() {
        availabilityMap.forEach { (absoluteSeatId, ranges) ->
            val firstSegmentIndex = 0
            val firstAvailableRange = ranges.first()

            if (firstAvailableRange.first == firstSegmentIndex) {
                val startVertex = absoluteSeatId to firstAvailableRange
                addEdge(ephemeralStartVertex, startVertex)
            }
        }
    }

    private fun SeatAvailabilityGraph.connectRemainingVertices() {
        val availabilityList = availabilityMap.toList()

        for (i in (0 until availabilityList.lastIndex)) {
            for (j in (i + 1..availabilityList.lastIndex)) {
                connectVertices(availabilityList[i], availabilityList[j])
            }
        }
    }

    private fun SeatAvailabilityGraph.connectVertices(
        left: Pair<AbsoluteSeatId, List<SeatRange>>,
        right: Pair<AbsoluteSeatId, List<SeatRange>>
    ) {
        val (leftId, leftRanges) = left
        val (rightId, rightRanges) = right

        leftRanges.forEach { leftRange ->
            rightRanges.forEach { rightRange ->
                if (canBeConnected(leftRange, rightRange)) {
                    addEdge(Pair(leftId, leftRange), Pair(rightId, rightRange))
                } else if (canBeConnected(rightRange, leftRange)) {
                    addEdge(Pair(rightId, rightRange), Pair(leftId, leftRange))
                }
            }
        }
    }

    private fun canBeConnected(left: Pair<Int, Int>, right: Pair<Int, Int>): Boolean {
        val rightStartsInLeft = left.first <= right.first && right.first <= left.second
        val rightFollowsLeft = (left.second + 1) == right.first
        val rightExtendsLeft = left.second < right.second

        return (rightStartsInLeft || rightFollowsLeft) && rightExtendsLeft
    }
}