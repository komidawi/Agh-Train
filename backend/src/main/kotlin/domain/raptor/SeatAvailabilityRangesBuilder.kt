package domain.raptor

import domain.train.bitmap.Coord
import domain.train.bitmap.Field

typealias AbsoluteSeatId = Pair<Int, Coord>
typealias SeatRange = Pair<Int, Int>
typealias SeatAvailabilityMap = Map<AbsoluteSeatId, List<SeatRange>>

class SeatAvailabilityRangesBuilder {

    fun buildSeatAvailabilityRanges(segments: List<Segment>): SeatAvailabilityMap {
        return extractSeats(segments).flatMap { (carId, seats) ->
            seats.map { seat ->
                val absoluteSeatId = AbsoluteSeatId(carId, seat.coord)
                val singleSeatRanges = buildRangesForSingleSeat(absoluteSeatId, segments)

                absoluteSeatId to singleSeatRanges
            }.filter { (_, ranges) -> ranges.isNotEmpty() }
        }.associate { it }
    }

    private fun extractSeats(segments: List<Segment>): List<Pair<Int, List<Field.Seat>>> {
        return segments.first().train.cars.map { car ->
            car.id to car.fieldMatrix.flatten().filterIsInstance<Field.Seat>()
        }
    }

    private fun buildRangesForSingleSeat(absoluteSeatId: AbsoluteSeatId, segments: List<Segment>): List<SeatRange> {
        val seatSnapshotsWithoutSentinel = extractSeatSnapshots(absoluteSeatId, segments)
        val seatSnapshots = appendSentinel(seatSnapshotsWithoutSentinel.toMutableList())
        val result = mutableListOf<SeatRange>()

        var beginningOfRange: Int
        var endOfRange = -1
        while (true) {
            beginningOfRange = seatSnapshots.firstIndexOf(startIndex = endOfRange + 1, needle = true)
            if (beginningOfRange == -1) {
                break
            }

            endOfRange = seatSnapshots.firstIndexOf(startIndex = beginningOfRange + 1, needle = false)
            result.add(SeatRange(beginningOfRange, endOfRange - 1))
        }

        return result
    }

    private fun extractSeatSnapshots(absoluteSeatId: AbsoluteSeatId, segments: List<Segment>): List<Boolean> {
        val (carId, seatCoords) = absoluteSeatId
        return segments.map { segment ->
            val car = segment.train.cars.find { it.id == carId }!!
            val seat = car.fieldMatrix[seatCoords.x][seatCoords.y] as Field.Seat

            seat.enabled
        }
    }

    private fun appendSentinel(seatSnapshots: MutableList<Boolean>): List<Boolean> {
        seatSnapshots.add(false)
        return seatSnapshots
    }

    private fun List<Boolean>.firstIndexOf(startIndex: Int, needle: Boolean): Int {
        for (i in startIndex..lastIndex) {
            if (this[i] == needle) {
                return i
            }
        }
        return -1
    }
}