package domain.raptor

class TrainStop(val name: String, val id: Long = 0) {

    // Important: ID is used only during creation of roads to distinguish between two trains,
    // that travel with different speed. Do not touch id and do not add it to equals/hashCode().
    override fun toString(): String {
        return "$name[$id]"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false

        other as TrainStop
        return name == other.name
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}