package domain.raptor

import app.toLocalDateTime
import com.raoulvdberge.raptor.model.StopTime
import domain.historical.Info
import domain.historical.Schedule
import domain.train.TrainFactory
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

class TrainTripFactoryImpl(private val trainFactory: TrainFactory) : TrainTripFactory {

    override fun create(trainId: String, trainName: String, schedule: Schedule): TrainTrip {
        val stopTimes = createStopTimes(schedule)
        val segments = createSegments(trainId, trainName, schedule)
        return TrainTrip(trainId, trainName, stopTimes, segments)
    }

    private fun createStopTimes(schedule: Schedule): StopTimes {
        val listWithFirstStop = createFirstStop(schedule.info.first())
        return createRemainingStops(schedule.info, listWithFirstStop.toMutableList())
    }

    private fun createFirstStop(first: Info) = listOf(
        StopTime(
            TrainStop(name = first.station_name!!),
            first.arrival_time?.toLocalDateTime() ?: LocalDateTime.MIN,
            first.departure_time?.toLocalDateTime() ?: LocalDateTime.MIN
        )
    )

    private fun createRemainingStops(
        info: List<Info>,
        stops: MutableList<StopTime<TrainStop>>
    ): MutableList<StopTime<TrainStop>> {
        info.zipWithNext { previous, current ->
            val id = ChronoUnit.SECONDS.between(
                previous.departure_time!!.toLocalDateTime(),
                current.arrival_time!!.toLocalDateTime()
            )

            stops.add(
                StopTime(
                    TrainStop(current.station_name!!, id),
                    current.arrival_time.toLocalDateTime(),
                    current.departure_time?.toLocalDateTime() ?: LocalDateTime.MIN
                )
            )
        }

        return stops
    }

    private fun createSegments(trainId: String, trainName: String, schedule: Schedule): List<Segment> =
        schedule.info.zipWithNext { stationA, stationB ->
            val train = trainFactory.create(trainId, trainName)
            Segment(stationA, stationB, train)
        }
}