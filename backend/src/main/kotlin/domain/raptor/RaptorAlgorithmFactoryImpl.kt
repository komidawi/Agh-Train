package domain.raptor

class RaptorAlgorithmFactoryImpl : RaptorAlgorithmFactory {
    override fun create(): RaptorAlgorithm =
        RaptorAlgorithmImpl()
}