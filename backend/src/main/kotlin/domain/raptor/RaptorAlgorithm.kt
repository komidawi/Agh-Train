package domain.raptor

import domain.journey.Journeys
import java.time.LocalDateTime

interface RaptorAlgorithm {
    fun getKBestJourneys(
        trainTrips: List<TrainTrip>,
        k: Int,
        source: String,
        destination: String,
        startDateTime: LocalDateTime
    ): Journeys
}