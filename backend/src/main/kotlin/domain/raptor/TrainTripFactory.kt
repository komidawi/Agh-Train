package domain.raptor

import domain.historical.Schedule

fun interface TrainTripFactory {
    fun create(trainId: String, trainName: String, schedule: Schedule): TrainTrip
}