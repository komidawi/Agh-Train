package domain.rules

class RulesServiceImpl : RulesService {
    private var rules: Rules = Rules(
        emptyList(),
        listOf(
            GlobalRule.DisabledStop("Warszawa Centralna"),
            GlobalRule.DisabledStop("Warszawa Wschodnia")
        )
    )

    override fun getRules() = rules

    override fun storeRules(newRules: Rules) {
        rules = newRules
    }
}