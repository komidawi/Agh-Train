package domain.rules

import domain.train.bitmap.Field

class OnePersonInCompartmentApplier() : SeatScanner() {
    override fun TraverseInner(
        field: Field,
        x: Int,
        y: Int,
        fieldMatrix: List<List<Field>>,
        wrappedList: List<List<TraversableField>>
    ) {
        if (field !is Field.Seat || field.direction != Field.Seat.Direction.RIGHT || !field.enabled) {
            return
        }
        var xCursor = x;
        var yCursor = y + 1;
        x@ while (xCursor < fieldMatrix.first().size) {
            y@ while (yCursor < fieldMatrix.size) {
                val current = fieldMatrix[yCursor][xCursor];
                if (current is Field.Window || current is Field.Wall || current is Field.Corridor) {
                    break@y
                }
                if (current is Field.Seat) {
                    current.enabled = false;
                }
                yCursor++;
            }
            xCursor++
            // Each second line we need to scan from beginning.
            yCursor = y;
        }
    }
}