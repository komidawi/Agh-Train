package domain.rules

import domain.train.bitmap.Field

class TraversableField(val field: Field, var isTraversed: Boolean = false)