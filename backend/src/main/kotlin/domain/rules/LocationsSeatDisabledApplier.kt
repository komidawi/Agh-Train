package domain.rules

import domain.train.bitmap.Field
import domain.train.bitmap.FieldMatrix


class LocationsSeatDisabledApplier(private val locations: Set<Field.Seat.Location>) : RuleApplier {

    override fun apply(fieldMatrix: FieldMatrix) {
        fieldMatrix.flatten().forEach { field ->
            if (field is Field.Seat) {
                if (field.enabled && locations.contains(field.location)) {
                    field.enabled = false
                }
            }
        }
    }
}

