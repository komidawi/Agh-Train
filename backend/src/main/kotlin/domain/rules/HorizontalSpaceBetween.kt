package domain.rules

import domain.train.bitmap.Field

class HorizontalSpaceBetween(private val scanRange: UInt) : SeatScanner() {
    override fun TraverseInner(
        field: Field,
        x: Int,
        y: Int,
        fieldMatrix: List<List<Field>>,
        wrappedList: List<List<TraversableField>>
    ) {
        if (field !is Field.Seat || !field.enabled) {
            return
        }
        var cursor = x + 1;
        val adjustedOffset = x + scanRange.toInt()
        while (cursor < fieldMatrix[0].size && cursor <= adjustedOffset) {
            val wrapped = wrappedList[y][cursor];
            val unwrapped = wrapped.field;
            if (unwrapped is Field.Seat) {
                unwrapped.enabled = false;
            }
            cursor++;
        }
    }
}