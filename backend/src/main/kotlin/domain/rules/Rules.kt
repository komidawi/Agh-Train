package domain.rules

import domain.rules.train.TrainRule

data class Rules(
    val byTrain: List<TrainRule>,
    val globalRules: List<GlobalRule>
)