package domain.rules

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName
import com.papsign.ktor.openapigen.annotations.type.string.example.DiscriminatorAnnotation
import domain.raptor.TrainTrip
import domain.train.bitmap.Field
import domain.train.car.Car

const val globalRuleNameSpace = "PlEduAghDomainRulesGlobalRule";

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type",
    visible = true,
)
@JsonSubTypes(
    JsonSubTypes.Type(GlobalRule.SpaceBetween::class),
    JsonSubTypes.Type(GlobalRule.SeatLocationsDisabled::class),
    JsonSubTypes.Type(GlobalRule.SeatVisAVisDisabled::class),
    JsonSubTypes.Type(GlobalRule.HorizontalSpaceBetweenDisabled::class),
    JsonSubTypes.Type(GlobalRule.DisabledStop::class),
    JsonSubTypes.Type(GlobalRule.OnePersonInCompartment::class),
)
@DiscriminatorAnnotation
@Suppress("MemberVisibilityCanBePrivate")
sealed class GlobalRule {

    open fun apply(trips: List<TrainTrip>): List<TrainTrip> = trips

    @DiscriminatorAnnotation
    @JsonTypeName("${globalRuleNameSpace}SpaceBetween")
    class SpaceBetween(val requiredSpace: Number) : GlobalRule()

    @DiscriminatorAnnotation
    @JsonTypeName("${globalRuleNameSpace}SeatLocationsDisabled")
    class SeatLocationsDisabled(val locations: List<Field.Seat.Location>) : GlobalRule() {
        override fun apply(trips: List<TrainTrip>): List<TrainTrip> {
            val applier = LocationsSeatDisabledApplier(locations.toSet())
            trips.extractCars().forEach {
                applier.apply(it.fieldMatrix)
            }
            return trips
        }
    }

    @DiscriminatorAnnotation
    @JsonTypeName("${globalRuleNameSpace}SeatVisAVisDisabled")
    class SeatVisAVisDisabled(val spaceBetween: Int = 2) : GlobalRule() {
        override fun apply(trips: List<TrainTrip>): List<TrainTrip> {
            require(spaceBetween > 0)
            trips.extractCars().forEach {
                NotVisAVisApplier(
                    spaceBetween.toUInt()
                ).apply(it.fieldMatrix)
            }
            return trips
        }
    }

    @DiscriminatorAnnotation
    @JsonTypeName("${globalRuleNameSpace}OnePersonInCompartment")
    class OnePersonInCompartment : GlobalRule() {
        override fun apply(trips: List<TrainTrip>): List<TrainTrip> {
            trips.extractCars().forEach {
                OnePersonInCompartmentApplier().apply(it.fieldMatrix)
            }
            return trips
        }
    }

    @DiscriminatorAnnotation
    @JsonTypeName("${globalRuleNameSpace}HorizontalSpaceBetweenDisabled")
    class HorizontalSpaceBetweenDisabled(val spaceBetween: Int = 2) : GlobalRule() {
        override fun apply(trips: List<TrainTrip>): List<TrainTrip> {
            require(spaceBetween > 0)
            trips.extractCars().forEach {
                HorizontalSpaceBetween(spaceBetween.toUInt()).apply(it.fieldMatrix)
            }
            return trips
        }
    }

    @DiscriminatorAnnotation
    @JsonTypeName("${globalRuleNameSpace}DisabledStop")
    class DisabledStop(val excluded: String) : GlobalRule() {
        override fun apply(trips: List<TrainTrip>): List<TrainTrip> {
            val ruleApplier = DisabledStopRuleApplier(excluded)
            return ruleApplier.apply(trips)
        }
    }
}

fun List<TrainTrip>.extractCars(): List<Car> =
    flatMap { trainTrip ->
        trainTrip.segments.flatMap { edge ->
            edge.train.cars
        }
    }