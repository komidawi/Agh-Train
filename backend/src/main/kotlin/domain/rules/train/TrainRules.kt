package domain.rules.train

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName
import com.papsign.ktor.openapigen.annotations.type.string.example.DiscriminatorAnnotation
import domain.raptor.TrainTrip

const val trainRuleNameSpace = "PlEduAghDomainRulesTrainTrainRule"

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type",
    visible = true,
)
@JsonSubTypes(
    JsonSubTypes.Type(TrainRule.DisabledSeats::class)
)
@DiscriminatorAnnotation
@Suppress("MemberVisibilityCanBePrivate")
sealed class TrainRule {
    abstract fun apply(trips: List<TrainTrip>): List<TrainTrip>

    @DiscriminatorAnnotation
    @JsonTypeName("${trainRuleNameSpace}DisabledSeats")
    class DisabledSeats(val trainId: String, val carId: Int, val seatIds: List<Int>) : TrainRule() {
        override fun apply(trips: List<TrainTrip>): List<TrainTrip> {
            val applier = DisabledSeatRuleApplier(trainId, carId, seatIds)
            return applier.apply(trips)
        }
    }
}