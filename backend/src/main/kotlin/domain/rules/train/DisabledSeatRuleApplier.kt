package domain.rules.train

import domain.journey.guardNotNull
import domain.raptor.TrainTrip
import domain.train.bitmap.Field

class DisabledSeatRuleApplier(
    private val trainId: String,
    private val carId: Int,
    private val disabledSeatIds: List<Int>
) {

    fun apply(trips: List<TrainTrip>): List<TrainTrip> {
        trips.forEach { trip ->
            trip.segments.forEach { segment ->
                if (trainId == segment.train.id) {
                    val car = segment.train.cars.find { it.id == carId }.guardNotNull("CarId=$carId not exists!")

                    disabledSeatIds.forEach { disabledNumber ->
                        val seat = car.fieldMatrix.flatten()
                            .find { it is Field.Seat && it.number == disabledNumber } as Field.Seat
                        seat.enabled = false
                    }
                }
            }
        }
        return trips
    }
}