package domain.rules

import domain.train.bitmap.Field

class NotVisAVisApplier(private val scanRange: UInt) : SeatScanner() {
    override fun TraverseInner(
        field: Field,
        x: Int,
        y: Int,
        fieldMatrix: List<List<Field>>,
        wrappedList: List<List<TraversableField>>
    ) {
        if (field !is Field.Seat || field.direction != Field.Seat.Direction.RIGHT) {
            return
        }
        var cursor = y + 1;
        val adjustedOffset = y + scanRange.toInt()
        while (cursor < fieldMatrix.size && cursor <= adjustedOffset) {
            val wrapped = wrappedList[cursor][x];
            val unwrapped = wrapped.field;
            if (unwrapped is Field.Seat && unwrapped.direction == Field.Seat.Direction.LEFT) {
                unwrapped.enabled = false;
            }
            cursor++;
        }
    }
}



