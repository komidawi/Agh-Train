package domain.rules

import domain.train.bitmap.FieldMatrix

fun interface RuleApplier {
    fun apply(fieldMatrix: FieldMatrix)
}