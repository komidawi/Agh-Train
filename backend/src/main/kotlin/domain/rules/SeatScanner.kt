package domain.rules

import domain.train.bitmap.Field
import domain.train.bitmap.FieldMatrix
import domain.train.bitmap.mapItems

abstract class SeatScanner : RuleApplier {
    override fun apply(fieldMatrix: FieldMatrix) {
        val wrappedList = fieldMatrix.mapItems { TraversableField(it) }
        wrappedList.forEachIndexed y@{ y, row ->
            row.forEachIndexed x@{ x, item ->
                val field = item.field
                if (item.isTraversed) {
                    return@x
                }
                item.isTraversed = true;
                TraverseInner(field, x, y, fieldMatrix, wrappedList)
            }
        }
    }

    abstract fun TraverseInner(
        field: Field,
        x: Int,
        y: Int,
        fieldMatrix: List<List<Field>>,
        wrappedList: List<List<TraversableField>>
    )
}