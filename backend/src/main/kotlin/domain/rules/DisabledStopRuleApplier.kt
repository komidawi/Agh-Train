package domain.rules

import domain.raptor.Segment
import domain.raptor.StopTimes
import domain.raptor.TrainTrip

class DisabledStopRuleApplier(private val excluded: String) {
    fun apply(trips: List<TrainTrip>): List<TrainTrip> {
        return trips.map { trip ->
            val includedStops = filterStopTimes(trip.stopTimes)
            val includedSegments = rebuildSegments(trip.segments)
            TrainTrip(trip.trainId, trip.trainName, includedStops, includedSegments)
        }.filter { it.segments.isNotEmpty() }
    }

    private fun filterStopTimes(stopTimes: StopTimes): StopTimes {
        return stopTimes.filter { stop -> stop.stop.name != excluded }
    }

    private fun rebuildSegments(segments: List<Segment>): List<Segment> {
        val firstFromIndex = segments.indexOfFirst { it.from.station_name!! == excluded }
        val firstToIndex = segments.indexOfFirst { it.to.station_name!! == excluded }
        if (!isStopPartOfAnySegment(firstFromIndex, firstToIndex)) {
            return segments
        }
        if (isSourceOfFirstSegment(firstFromIndex, firstToIndex)) {
            return segments.subList(1, segments.size)
        }
        if (isDestinationOfLastSegment(firstFromIndex, firstToIndex)) {
            return segments.subList(0, segments.lastIndex)
        }
        return squashSegments(firstToIndex, firstFromIndex, segments)
    }

    private fun isStopPartOfAnySegment(firstFromIndex: Int, firstToIndex: Int): Boolean {
        return firstFromIndex != -1 || firstToIndex != -1
    }

    private fun isSourceOfFirstSegment(firstFromIndex: Int, firstToIndex: Int): Boolean {
        return firstFromIndex != -1 && firstToIndex == -1
    }

    private fun isDestinationOfLastSegment(firstFromIndex: Int, firstToIndex: Int): Boolean {
        return firstFromIndex == -1 && firstToIndex != -1
    }

    private fun squashSegments(
        firstToIndex: Int,
        firstFromIndex: Int,
        segments: List<Segment>
    ): List<Segment> {
        require(firstFromIndex == firstToIndex + 1) { "Station must be a part of two consecutive segments" }

        val train = segments.first().train
        val previousSegment = segments[firstToIndex]
        val nextSegment = segments[firstFromIndex]

        val preSquashedSegment = segments.subList(0, firstToIndex)
        val squashedSegment = listOf(Segment(previousSegment.from, nextSegment.to, train))
        val postSquashedSegment = segments.subList(firstFromIndex + 1, segments.size)

        return preSquashedSegment + squashedSegment + postSquashedSegment
    }
}