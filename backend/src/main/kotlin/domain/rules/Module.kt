package domain.rules

import com.papsign.ktor.openapigen.route.path.auth.OpenAPIAuthenticatedRoute
import com.papsign.ktor.openapigen.route.path.auth.get
import com.papsign.ktor.openapigen.route.path.auth.post
import com.papsign.ktor.openapigen.route.response.respond
import com.papsign.ktor.openapigen.route.route
import domain.rules.messages.GetRulesRequest
import domain.rules.messages.GetRulesResponse
import domain.rules.messages.StoreRulesResponse
import io.ktor.auth.jwt.*

fun OpenAPIAuthenticatedRoute<JWTPrincipal>.load(rulesService: RulesService) {
    route("get_rules")
        .get<GetRulesRequest, GetRulesResponse, JWTPrincipal> {
            respond(GetRulesResponse(rulesService.getRules()))
        }

    route("store_rules")
        .post<Unit, StoreRulesResponse, Rules, JWTPrincipal> { _, rules ->
            rulesService.storeRules(rules)
            respond(StoreRulesResponse())
        }
}