package domain.rules.messages

import com.papsign.ktor.openapigen.annotations.Response
import domain.rules.Rules

@Response()
data class GetRulesResponse(val rules: Rules)