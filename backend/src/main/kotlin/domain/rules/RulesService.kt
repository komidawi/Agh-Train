package domain.rules

interface RulesService {
    fun getRules(): Rules
    fun storeRules(newRules: Rules)
}