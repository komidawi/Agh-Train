package domain.historical

import java.util.*

data class Info(
    val arrival_time: Date?,
    val station_name: String?,
    val departure_time: Date?,
    val departure_delay: Number?,
    var arrival_delay: Number?,
)
