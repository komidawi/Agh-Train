package domain.historical

import app.JsonMapperInstance
import app.toDate
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.runBlocking
import java.io.File
import java.nio.file.Paths
import java.time.LocalDateTime

class TrainsLoaderImpl : TrainsLoader, StationsProvider {
    private fun <T> mapReduce(map: (Root) -> T, reduce: (T) -> Boolean): List<T> {
        val rootPath = Paths.get("").toAbsolutePath().toString()
        val files = File("$rootPath/test-data").listFiles();
        val res = runBlocking {
            return@runBlocking files?.map {
                async {
                    val root = JsonMapperInstance.readValue<Root>(it)
                    map(root);
                }
            }?.awaitAll()
        }
        val list = res?.toList() ?: listOf<T>();
        return list.filter { reduce(it) };
    }

    override fun accept(from: LocalDateTime, to: LocalDateTime): List<Root> {
        return mapReduce({
            val scoped = it.schedules.filter {
                it.schedule_date != null && it.schedule_date.after(from.toDate()) && it.schedule_date.before(to.toDate())
            }

            Root(normalize(scoped), it.train_id, it.train_name)
        }, {
            it.schedules.any()
        })
    }

    private fun normalize(schedules: List<Schedule>) =
        schedules.map { schedule ->
            Schedule(schedule.schedule_date, schedule.schedule_id, schedule.info.mapIndexed { index, it ->
                // In original data, disabled stops are represented with departure_time == null.
                // We normalize it, because a special rule exists for that purpose.
                val departureTime = if (index != schedule.info.lastIndex && it.departure_time == null) {
                    it.arrival_time
                } else {
                    it.departure_time
                }

                Info(it.arrival_time, it.station_name, departureTime, it.departure_delay, it.arrival_delay)
            })
        }

    override fun get(): List<Station> {
        return mapReduce({
            it.schedules.flatMap { it.info.map { it.station_name } }
        }, {
            it.any()
        }).flatten()
            .map { it as Station }
            .distinct()
    }

}

fun interface TrainsLoader {
    fun accept(from: LocalDateTime, to: LocalDateTime): List<Root>
}