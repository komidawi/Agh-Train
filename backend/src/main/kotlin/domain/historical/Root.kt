package domain.historical

data class Root(
    val schedules: List<Schedule>,
    val train_id: Number,
    val train_name: String
)