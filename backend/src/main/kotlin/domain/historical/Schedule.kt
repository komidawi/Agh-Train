package domain.historical

import java.util.*

data class Schedule(
    val schedule_date: Date?,
    val schedule_id: Number?,
    val info: List<Info>
)