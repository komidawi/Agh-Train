package domain.historical

import com.papsign.ktor.openapigen.route.path.auth.OpenAPIAuthenticatedRoute
import com.papsign.ktor.openapigen.route.path.auth.get
import com.papsign.ktor.openapigen.route.response.respond
import com.papsign.ktor.openapigen.route.route
import io.ktor.auth.jwt.*
import network.NullRequest

fun OpenAPIAuthenticatedRoute<JWTPrincipal>.load(stationsProvider: StationsProvider) {
    route("stations").get<NullRequest, List<Station>, JWTPrincipal> {
        respond(stationsProvider.get())
    }
}