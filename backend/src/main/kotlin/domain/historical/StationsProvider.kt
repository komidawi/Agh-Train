package domain.historical

typealias Station = String;

fun interface StationsProvider {
    fun get(): List<Station>
}