package pl.edu.agh

import app.Installers.installCors
import app.Installers.installJackson
import app.Installers.installJwt
import app.Installers.installOpenApiGen
import io.ktor.application.*
import io.ktor.features.*
import network.OpenApiSerializer
import network.Routing.configureRouting
import org.slf4j.event.Level

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@kotlin.jvm.JvmOverloads
fun Application.module(@Suppress("UNUSED_PARAMETER") testing: Boolean = false) {
    installJwt()
    installJackson()
    installCors()
    installOpenApiGen()
    configureRouting()
    install(CallLogging) {
        level = Level.DEBUG
    }
    install(StatusPages)
    OpenApiSerializer.serializeToFile(this)
}