package domain.raptor

import app.DI
import domain.historical.Root
import domain.journey.LegStop
import domain.raptor.preprocessing.*
import domain.rules.Rules
import domain.train.Train
import domain.train.TrainFactory
import domain.train.TrainType
import domain.train.car.CarType
import io.kotest.core.spec.style.BehaviorSpec
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import java.time.LocalDateTime
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class RaptorAlgorithmImplTest : BehaviorSpec({
    val rulesMock = mockk<Rules> {
        every { globalRules } returns emptyList()
        every { byTrain } returns emptyList()
    }

    val idSlot = slot<String>()
    val nameSlot = slot<String>()
    val trainFactoryMock = mockk<TrainFactory> {
        every { create(capture(idSlot), capture(nameSlot)) } answers {
            Train(
                idSlot.captured,
                nameSlot.captured,
                TrainType.T_1130_1_OLENKA,
                listOf(DI.carFactory.create(carType = CarType.COMPARTMENT_FIRST_CLASS, carId = 1))
            )
        }
    }

    val processingSteps = listOf(
        ApplyRulesStep(rulesMock),
        SplitToPassableTripsStep(),
        SplitToOptimalLegsWithSeatStep(),
        SortedByDepartureTimeStep()
    )

    val trainTripFactory = TrainTripFactoryImpl(trainFactoryMock)
    val baseStartDateTime = LocalDateTime.parse("2020-08-23T08:44:00")
    val raptorAlgorithm = RaptorAlgorithmImpl()

    given("[1] Raptor algorithm set up with a single train data") {
        val trips = prepareTrips(listOf(sampleTrain), trainTripFactory, processingSteps)

        `when`("Searching for nonexistent connection due to missing stop") {
            val journeys =
                raptorAlgorithm.getKBestJourneys(
                    trainTrips = trips,
                    k = 1,
                    source = "Warszawa Zachodnia",
                    destination = "Kraków Główny",
                    startDateTime = baseStartDateTime.plusMinutes(-14)
                ).journeys

            then("Raptor returns empty journeys") {
                assertTrue(journeys.isEmpty())
            }
        }

        `when`("Searching for nonexistent connection due to late time") {
            val journeys =
                raptorAlgorithm.getKBestJourneys(
                    trainTrips = trips,
                    k = 1,
                    source = "Warszawa Zachodnia",
                    destination = "Warszawa Wschodnia",
                    startDateTime = baseStartDateTime.plusMinutes(5)
                ).journeys

            then("Raptor returns empty journeys") {
                assertTrue(journeys.isEmpty())
            }
        }

        `when`("Searching for existent connection in valid time") {
            val journeys =
                raptorAlgorithm.getKBestJourneys(
                    trainTrips = trips,
                    k = 1,
                    source = "Warszawa Zachodnia",
                    destination = "Warszawa Wschodnia",
                    startDateTime = baseStartDateTime.plusMinutes(-15)
                ).journeys

            then("Raptor finds the best journey") {
                val expectedCountOfJourneys = 1
                assertEquals(expectedCountOfJourneys, journeys.size)
            }

            and("Journey consists of a single leg") {
                val expectedCountOfLegs = 1
                assertEquals(expectedCountOfLegs, journeys.first().legs.size)
            }

            and("Leg contains the expected train data") {
                val leg = journeys.first().legs.first()

                val expectedTrainName = "Sample train"
                assertEquals(expectedTrainName, leg.trainName)

                val expectedTrainNumber = "2709"
                assertEquals(expectedTrainNumber, leg.trainNumber)
            }

            and("Leg consists of the expected stops") {
                val expectedStops = listOf(
                    LegStop(
                        arrivalTime = null,
                        departureTime = baseStartDateTime.plusMinutes(1),
                        station = "Warszawa Zachodnia"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTime.plusMinutes(5),
                        departureTime = baseStartDateTime.plusMinutes(11),
                        station = "Warszawa Centralna"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTime.plusMinutes(17),
                        departureTime = null,
                        station = "Warszawa Wschodnia"
                    )
                )

                val actualStops = journeys.first().legs.first().stops
                assertEquals(expectedStops, actualStops)
            }
        }

        `when`("Searching for several journeys, but only one exists") {
            val journeys =
                raptorAlgorithm.getKBestJourneys(
                    trainTrips = trips,
                    k = 10,
                    source = "Warszawa Zachodnia",
                    destination = "Warszawa Wschodnia",
                    startDateTime = baseStartDateTime
                ).journeys

            then("Raptor returns only one journey") {
                val expectedCountOfJourneys = 1
                assertEquals(expectedCountOfJourneys, journeys.size)
            }
        }
    }

    given("Raptor algorithm is set up with a single train in different dates") {
        val trips = prepareTrips(listOf(sampleTrain, sampleTrain.shiftedByDays(1)), trainTripFactory, processingSteps)

        `when`("Searching for existent connection in valid time") {
            val journeys =
                raptorAlgorithm.getKBestJourneys(
                    trainTrips = trips,
                    k = 2,
                    source = "Warszawa Zachodnia",
                    destination = "Warszawa Wschodnia",
                    startDateTime = baseStartDateTime
                ).journeys

            then("Raptor finds both journeys") {
                val expectedCountOfJourneys = 2
                assertEquals(expectedCountOfJourneys, journeys.size)
            }

            and("Both journeys consist of a single leg") {
                val expectedCountOfLegs = 1
                val (firstJourney, secondJourney) = journeys

                assertEquals(expectedCountOfLegs, firstJourney.legs.size)
                assertEquals(expectedCountOfLegs, secondJourney.legs.size)
            }

            and("Leg of each of journeys contains the expected train data") {
                for (journey in journeys) {
                    val leg = journey.legs.first()

                    val expectedTrainName = "Sample train"
                    assertEquals(expectedTrainName, leg.trainName)

                    val expectedTrainNumber = "2709"
                    assertEquals(expectedTrainNumber, leg.trainNumber)
                }
            }

            and("Leg of each journey consists of the expected stops") {
                val expectedStopsOfFirstJourney = listOf(
                    LegStop(
                        arrivalTime = null,
                        departureTime = baseStartDateTime.plusMinutes(1),
                        station = "Warszawa Zachodnia"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTime.plusMinutes(5),
                        departureTime = baseStartDateTime.plusMinutes(11),
                        station = "Warszawa Centralna"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTime.plusMinutes(17),
                        departureTime = null,
                        station = "Warszawa Wschodnia"
                    )
                )

                val baseStartDateTimeOfSecondJourney = baseStartDateTime.plusDays(1)
                val expectedStopsOfSecondJourney = listOf(
                    LegStop(
                        arrivalTime = null,
                        departureTime = baseStartDateTimeOfSecondJourney.plusMinutes(1),
                        station = "Warszawa Zachodnia"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTimeOfSecondJourney.plusMinutes(5),
                        departureTime = baseStartDateTimeOfSecondJourney.plusMinutes(11),
                        station = "Warszawa Centralna"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTimeOfSecondJourney.plusMinutes(17),
                        departureTime = null,
                        station = "Warszawa Wschodnia"
                    )
                )

                val (firstJourney, secondJourney) = journeys

                val actualStopsOfFirstJourney = firstJourney.legs.first().stops
                assertEquals(expectedStopsOfFirstJourney, actualStopsOfFirstJourney)

                val actualStopsOfSecondJourney = secondJourney.legs.first().stops
                assertEquals(expectedStopsOfSecondJourney, actualStopsOfSecondJourney)
            }
        }
    }

    given("Raptor algorithm is set up with a single train with unordered different dates") {
        val trips = prepareTrips(listOf(sampleTrain.shiftedByDays(1), sampleTrain), trainTripFactory, processingSteps)

        `when`("Searching for one journey and several journeys exist") {
            val journeys =
                raptorAlgorithm.getKBestJourneys(
                    trainTrips = trips,
                    k = 1,
                    source = "Warszawa Zachodnia",
                    destination = "Warszawa Wschodnia",
                    startDateTime = baseStartDateTime
                ).journeys

            then("Raptor returns only one journey") {
                val expectedCountOfJourneys = 1
                assertEquals(expectedCountOfJourneys, journeys.size)
            }

            and("The fastest journey is returned") {
                val expectedStops = listOf(
                    LegStop(
                        arrivalTime = null,
                        departureTime = baseStartDateTime.plusMinutes(1),
                        station = "Warszawa Zachodnia"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTime.plusMinutes(5),
                        departureTime = baseStartDateTime.plusMinutes(11),
                        station = "Warszawa Centralna"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTime.plusMinutes(17),
                        departureTime = null,
                        station = "Warszawa Wschodnia"
                    )
                )

                val actualStops = journeys.first().legs.first().stops
                assertEquals(expectedStops, actualStops)
            }
        }
    }

    given("Raptor algorithm is set up with two trains (slower and faster)") {
        val trips = prepareTrips(listOf(sampleTrain, fasterTrain), trainTripFactory, processingSteps)

        `when`("Searching for one journey and several journeys exist") {
            val journeys =
                raptorAlgorithm.getKBestJourneys(
                    trainTrips = trips,
                    k = 1,
                    source = "Warszawa Zachodnia",
                    destination = "Warszawa Wschodnia",
                    startDateTime = baseStartDateTime
                ).journeys

            then("Raptor returns only one journey") {
                val expectedCountOfJourneys = 1
                assertEquals(expectedCountOfJourneys, journeys.size)
            }

            and("The fastest journey is returned") {
                val expectedStops = listOf(
                    LegStop(
                        arrivalTime = null,
                        departureTime = baseStartDateTime.plusMinutes(2),
                        station = "Warszawa Zachodnia"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTime.plusMinutes(3),
                        departureTime = baseStartDateTime.plusMinutes(4),
                        station = "Warszawa Centralna"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTime.plusMinutes(11),
                        departureTime = null,
                        station = "Warszawa Wschodnia"
                    )
                )

                val actualStops = journeys.first().legs.first().stops
                assertEquals(expectedStops, actualStops)
            }
        }
    }

    given("Raptor algorithm is set up with two trains (slower and faster but only on part of route)") {
        val trips = prepareTrips(listOf(sampleTrain, fasterTrainOnlyOnPartOfJourney), trainTripFactory, processingSteps)

        `when`("Searching for one journey and several journeys exist") {
            val journeys =
                raptorAlgorithm.getKBestJourneys(
                    trainTrips = trips,
                    k = 1,
                    source = "Warszawa Zachodnia",
                    destination = "Warszawa Wschodnia",
                    startDateTime = baseStartDateTime
                ).journeys

            then("Raptor returns two journeys") {
                val expectedCountOfJourneys = 2
                assertEquals(expectedCountOfJourneys, journeys.size)
            }

            and("A journey with the lowest number of legs is returned") {
                val expectedStops = listOf(
                    LegStop(
                        arrivalTime = null,
                        departureTime = baseStartDateTime.plusMinutes(1),
                        station = "Warszawa Zachodnia"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTime.plusMinutes(5),
                        departureTime = baseStartDateTime.plusMinutes(11),
                        station = "Warszawa Centralna"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTime.plusMinutes(17),
                        departureTime = null,
                        station = "Warszawa Wschodnia"
                    )
                )

                val expectedLegsSize = 1
                assertEquals(expectedLegsSize, journeys[0].legs.size)
                assertEquals(expectedStops, journeys[0].legs[0].stops)
            }

            and("The fastest journey with greater number of legs is returned") {
                val expectedStopsInFirstLeg = listOf(
                    LegStop(
                        arrivalTime = null,
                        departureTime = baseStartDateTime.plusMinutes(1),
                        station = "Warszawa Zachodnia"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTime.plusMinutes(5),
                        departureTime = null,
                        station = "Warszawa Centralna"
                    )
                )

                val expectedStopsInSecondLeg = listOf(
                    LegStop(
                        arrivalTime = null,
                        departureTime = baseStartDateTime.plusMinutes(9),
                        station = "Warszawa Centralna"
                    ),
                    LegStop(
                        arrivalTime = baseStartDateTime.plusMinutes(11),
                        departureTime = null,
                        station = "Warszawa Wschodnia"
                    )
                )

                val expectedLegsSize = 2
                assertEquals(expectedLegsSize, journeys[1].legs.size)
                assertEquals(expectedStopsInFirstLeg, journeys[1].legs[0].stops)
                assertEquals(expectedStopsInSecondLeg, journeys[1].legs[1].stops)
            }
        }
    }
})

private fun prepareTrips(
    trainsData: List<Root>,
    trainTripFactory: TrainTripFactoryImpl,
    steps: List<TrainTripsProcessingStep>
): List<TrainTrip> {
    val trainTrips = buildTripsFromTrainsData(trainsData, trainTripFactory)
    return steps.fold(trainTrips) { trips, processingStep -> processingStep.invoke(trips) }
}

private fun buildTripsFromTrainsData(trainsData: List<Root>, trainTripFactory: TrainTripFactoryImpl) =
    trainsData.flatMap { root ->
        root.schedules.map { schedule ->
            trainTripFactory.create(root.train_id.toString(), root.train_name, schedule)
        }
    }