package domain.raptor

import app.DI
import app.toDate
import com.raoulvdberge.raptor.model.StopTime
import domain.historical.Info
import domain.train.Train
import domain.train.TrainType
import domain.train.car.CarType
import java.time.LocalDateTime

object TestTrainTripFactory {

    private var day = 1L
    private val baseDate = LocalDateTime.parse("2020-08-23T08:44:00")

    fun createTrainTrip(trainId: String = "1", trainName: String = "t1", charRange: CharRange): TrainTrip {
        val stopTimes = charRange.map { consecutiveStopTime(it.toString()) }
        val segments = stopTimes.zipWithNext { source, destination ->
            Segment(
                from = Info(
                    arrival_time = source.arrivalTime.toDate(),
                    arrival_delay = 0,
                    departure_time = source.departureTime.toDate(),
                    departure_delay = 0,
                    station_name = source.stop.name
                ),
                to = Info(
                    arrival_time = destination.arrivalTime.toDate(),
                    arrival_delay = 0,
                    departure_time = destination.departureTime.toDate(),
                    departure_delay = 0,
                    station_name = destination.stop.name
                ),
                train = Train(
                    trainId,
                    trainName,
                    TrainType.T_1130_1_OLENKA,
                    listOf(DI.carFactory.create(CarType.COMPARTMENT_FIRST_CLASS, carId = 1))
                )
            )
        }
        return TrainTrip(trainId, trainName, stopTimes, segments)
    }

    private fun consecutiveStopTime(name: String): StopTime<TrainStop> =
        StopTime(
            TrainStop(name),
            baseDate.plusDays(day++),
            baseDate.plusDays(day++)
        )
}