package domain.raptor

import domain.raptor.TestTrainTripFactory.createTrainTrip
import domain.raptor.preprocessing.disableAllSeats
import domain.train.bitmap.Coord
import domain.train.bitmap.Field
import io.kotest.core.spec.style.BehaviorSpec
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

internal class SeatAvailabilityRangesBuilderTest : BehaviorSpec({

    given("A trip from A to G with all seats enabled") {
        val trainTrip = createTrainTrip(trainId = "2709-1729", trainName = "SUPER-FAST-TRAIN", charRange = ('A'..'G'))

        `when`("Availability ranges are built") {
            val seatAvailabilityRangesBuilder = SeatAvailabilityRangesBuilder()
            val result = seatAvailabilityRangesBuilder.buildSeatAvailabilityRanges(trainTrip.segments)

            then("For each seat availability ranges are built") {
                val expectedSeatsCount = 54
                assertEquals(expectedSeatsCount, result.size)
            }

            then("Each seat contains only one range, which covers the whole trip") {
                val rangeCoveringWholeTrip = SeatRange(0, trainTrip.segments.lastIndex)
                val eachSeatRangeCoversWholeTrip = result.all { (_, seatsRanges) ->
                    1 == seatsRanges.size && seatsRanges[0] == rangeCoveringWholeTrip
                }
                assertTrue(eachSeatRangeCoversWholeTrip)
            }
        }
    }

    given("A trip from A to H with one seat partially disabled") {
        val segmentsBetweenAC = listOf(0, 1)
        val segmentsBetweenEG = listOf(4, 5)
        val segmentsToDisable = segmentsBetweenAC + segmentsBetweenEG

        val carId = 1
        val seatCoords = Coord(x = 1, y = 2)
        val disabledSeatId = AbsoluteSeatId(carId, seatCoords)

        val trainTrip = createTrainTrip(trainId = "2709-1729", trainName = "SUPER-FAST-TRAIN", charRange = ('A'..'H'))
        disableSeatOnSegments(disabledSeatId, trainTrip, segmentsToDisable)

        `when`("Availability ranges are built") {
            val seatAvailabilityRangesBuilder = SeatAvailabilityRangesBuilder()
            val result = seatAvailabilityRangesBuilder.buildSeatAvailabilityRanges(trainTrip.segments)

            then("For each seat availability ranges are built") {
                val expectedSeatsCount = 54
                assertEquals(expectedSeatsCount, result.size)
            }

            then("Each seat except disabled one contains only one range, which covers the whole trip") {
                val enabledPlaces = result.filter { (absoluteSeatId, _) -> absoluteSeatId != disabledSeatId }
                val rangeCoveringWholeTrip = SeatRange(0, trainTrip.segments.lastIndex)
                val eachSeatRangeCoversWholeTrip = enabledPlaces.all { (_, seatsRanges) ->
                    1 == seatsRanges.size && seatsRanges[0] == rangeCoveringWholeTrip
                }
                assertTrue(eachSeatRangeCoversWholeTrip)
            }

            then("For partially disabled seat two availability ranges exist") {
                val partiallyDisabledSeatAvailability = result[disabledSeatId]
                assertNotNull(partiallyDisabledSeatAvailability)

                val expectedAvailabilityRanges = 2
                assertEquals(expectedAvailabilityRanges, partiallyDisabledSeatAvailability.size)

                val rangeBetweenCE = SeatRange(2, 3)
                assertEquals(rangeBetweenCE, partiallyDisabledSeatAvailability[0])

                val rangeBetweenGH = SeatRange(6, 6)
                assertEquals(rangeBetweenGH, partiallyDisabledSeatAvailability[1])
            }
        }
    }

    given("A trip from A to G with all seats disabled") {
        val trainTrip = createTrainTrip(trainId = "2709-1729", trainName = "SUPER-FAST-TRAIN", charRange = ('A'..'G'))
        disableAllSeats(trainTrip)

        `when`("Availability ranges are built") {
            val seatAvailabilityRangesBuilder = SeatAvailabilityRangesBuilder()
            val result = seatAvailabilityRangesBuilder.buildSeatAvailabilityRanges(trainTrip.segments)

            then("No ranges are present") {
                assertTrue(result.isEmpty())
            }
        }
    }
})

fun disableSeatOnSegments(absoluteSeatId: AbsoluteSeatId, trainTrip: TrainTrip, disabledOnSegments: List<Int>) {
    val (carId, seatCoords) = absoluteSeatId
    disabledOnSegments.forEach { index ->
        val car = trainTrip.segments[index].train.cars.find { it.id == carId }
        val seat = car!!.fieldMatrix[seatCoords.x][seatCoords.y] as Field.Seat

        seat.enabled = false
    }
}