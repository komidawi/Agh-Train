package domain.raptor.preprocessing

import domain.raptor.AbsoluteSeatId
import domain.raptor.TestTrainTripFactory
import domain.raptor.TrainTrip
import domain.train.bitmap.Coord
import domain.train.bitmap.Field
import io.kotest.core.spec.style.BehaviorSpec
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal class OptimalLegSplitterImplTest : BehaviorSpec({

    given("A trip from A to G with all seats enabled") {
        val trainTrip = TestTrainTripFactory.createTrainTrip(
            trainId = "2709-1729",
            trainName = "SUPER-FAST-TRAIN",
            charRange = ('A'..'G')
        )

        `when`("Splitting to optimal legs") {
            val optimalLegSplitStep = SplitToOptimalLegsWithSeatStep()
            val result = optimalLegSplitStep(listOf(trainTrip))

            then("Result contains only one trip") {
                assertEquals(1, result.size)
            }

            and("That trip should have a valid seat id") {
                val invalidSeatId = AbsoluteSeatId(-1, Coord(-1, -1))
                assertNotEquals(invalidSeatId, result[0].usedSeatId)
            }

            and("That trip should cover whole trip passed as input") {
                assertEquals(trainTrip.trainId, result[0].trainId)
                assertEquals(trainTrip.trainName, result[0].trainName)
                assertEquals(trainTrip.stopTimes, result[0].stopTimes)
                assertEquals(trainTrip.segments, result[0].segments)
            }
        }
    }

    given("A trip from A to G with some places enabled on some segments") {
        val trainTrip = TestTrainTripFactory.createTrainTrip(
            trainId = "2709-1729",
            trainName = "SUPER-FAST-TRAIN",
            charRange = ('A'..'G')
        )
        disableAllSeats(trainTrip)

        val carId = 1
        val segmentsBetweenAE = (0..3).toList()
        val segmentsBetweenCG = (2..5).toList()

        val firstSeatCoords = Coord(x = 1, y = 1)
        val firstEnabledSeatId = AbsoluteSeatId(carId, firstSeatCoords)
        enableSeatOnSegments(firstEnabledSeatId, trainTrip, segmentsBetweenAE)

        val secondSeatCoords = Coord(x = 1, y = 2)
        val secondEnabledSeatId = AbsoluteSeatId(carId, secondSeatCoords)
        enableSeatOnSegments(secondEnabledSeatId, trainTrip, segmentsBetweenCG)

        `when`("Splitting to optimal legs") {
            val optimalLegSplitStep = SplitToOptimalLegsWithSeatStep()
            val result = optimalLegSplitStep(listOf(trainTrip))

            then("Result contains two trips") {
                assertEquals(2, result.size)
            }

            and("First trip is associated with preconfigured seatId") {
                assertEquals(firstEnabledSeatId, result[0].usedSeatId)
            }

            and("First trip covers expected segments") {
                segmentsBetweenAE.forEach { index ->
                    assertEquals(trainTrip.segments[index], result[0].segments[index])
                }
            }

            and("First trip covers only those segments") {
                assertEquals(segmentsBetweenAE.size, result[0].segments.size)
            }

            and("Second trip is associated with preconfigured seatId") {
                assertEquals(secondEnabledSeatId, result[1].usedSeatId)
            }

            and("Second trip covers expected segments") {
                assertEquals(trainTrip.segments[4], result[1].segments[0])
                assertEquals(trainTrip.segments[5], result[1].segments[1])
            }

            and("Second trip covers only those segments") {
                val expectedSegmentsCount = 2
                assertEquals(expectedSegmentsCount, result[1].segments.size)
            }
        }

        `when`("Additional place is enabled and covers the whole trip and splitting to optimal legs") {
            val additionalSeatCoords = Coord(x = 1, y = 3)
            val additionalEnabledSeatId = AbsoluteSeatId(carId, additionalSeatCoords)
            val allSegments = (0..trainTrip.segments.lastIndex).toList()
            enableSeatOnSegments(additionalEnabledSeatId, trainTrip, allSegments)

            val optimalLegSplitStep = SplitToOptimalLegsWithSeatStep()
            val result = optimalLegSplitStep(listOf(trainTrip))

            then("Result contains only one trip") {
                assertEquals(1, result.size)
            }

            and("That trip is associated with additional enabled seat") {
                assertEquals(additionalEnabledSeatId, result[0].usedSeatId)
            }

            and("That trip should cover whole trip passed as input") {
                assertEquals(trainTrip.trainId, result[0].trainId)
                assertEquals(trainTrip.trainName, result[0].trainName)
                assertEquals(trainTrip.stopTimes, result[0].stopTimes)
                assertEquals(trainTrip.segments, result[0].segments)
            }
        }
    }
})

fun disableAllSeats(trainTrip: TrainTrip) {
    trainTrip.segments.forEach { segment ->
        segment.train.cars.forEach { car ->
            car.fieldMatrix.flatten().forEach { field ->
                if (field is Field.Seat) {
                    field.enabled = false
                }
            }
        }
    }
}

fun enableSeatOnSegments(absoluteSeatId: AbsoluteSeatId, trainTrip: TrainTrip, enabledOnSegments: List<Int>) {
    val (carId, seatCoords) = absoluteSeatId
    enabledOnSegments.forEach { index ->
        val car = trainTrip.segments[index].train.cars.find { it.id == carId }
        val seat = car!!.fieldMatrix[seatCoords.x][seatCoords.y] as Field.Seat

        seat.enabled = true
    }
}