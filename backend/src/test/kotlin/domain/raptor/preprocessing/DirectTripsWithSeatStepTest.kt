package domain.raptor.preprocessing

import domain.raptor.AbsoluteSeatId
import domain.raptor.TestTrainTripFactory
import domain.train.bitmap.Coord
import io.kotest.core.spec.style.BehaviorSpec
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class DirectTripsWithSeatStepTest : BehaviorSpec({

    given("A trip from A to G and trip from G to I with all seats enabled") {
        val trainTripAG = TestTrainTripFactory.createTrainTrip(
            trainId = "2709-1729",
            trainName = "SUPER-FAST-TRAIN",
            charRange = ('A'..'G')
        )

        val trainTripGI = TestTrainTripFactory.createTrainTrip(
            trainId = "2809-1829",
            trainName = "SUPER-FAST-TRAIN-2",
            charRange = ('G'..'I')
        )

        `when`("Preparing trips with seats for leg [F, H]") {
            val processingStep = DirectTripsWithSeatStep(source = "F", destination = "H")
            val result = processingStep(listOf(trainTripAG, trainTripGI))

            then("Result is empty, because direct leg does not exist") {
                assertTrue { result.isEmpty() }
            }
        }
    }

    given("A trip from A to G all seats disabled") {
        val trainTrip = TestTrainTripFactory.createTrainTrip(
            trainId = "2709-1729",
            trainName = "SUPER-FAST-TRAIN",
            charRange = ('A'..'G')
        )
        disableAllSeats(trainTrip)

        `when`("Preparing trips with seats for leg [B, D]") {
            val processingStep = DirectTripsWithSeatStep(source = "B", destination = "D")
            val result = processingStep(listOf(trainTrip))

            then("Result is empty, because seats are not available") {
                assertTrue { result.isEmpty() }
            }
        }
    }

    given("A trip from A to G with one seat enabled on segments [B, D]") {
        val trainTrip = TestTrainTripFactory.createTrainTrip(
            trainId = "2709-1729",
            trainName = "SUPER-FAST-TRAIN",
            charRange = ('A'..'G')
        )
        disableAllSeats(trainTrip)

        val carId = 1
        val seatCoords = Coord(x = 1, y = 1)
        val enabledSeatId = AbsoluteSeatId(carId, seatCoords)

        val segmentsBetweenBD = listOf(1, 2)
        enableSeatOnSegments(enabledSeatId, trainTrip, segmentsBetweenBD)

        `when`("Preparing trips with seats for leg [B, D]") {
            val processingStep = DirectTripsWithSeatStep(source = "B", destination = "D")
            val result = processingStep(listOf(trainTrip))

            then("Result has one trip") {
                assertEquals(1, result.size)
            }

            and("Used seat id matches enabled one") {
                assertEquals(enabledSeatId, result[0].usedSeatId)
            }
        }
    }
})