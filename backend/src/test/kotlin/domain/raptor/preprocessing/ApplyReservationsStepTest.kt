package domain.raptor.preprocessing

import domain.journey.BookedSeat
import domain.journey.Journey
import domain.journey.JourneyLeg
import domain.journey.LegStop
import domain.raptor.AbsoluteSeatId
import domain.raptor.TestTrainTripFactory
import domain.raptor.TrainTrip
import domain.train.bitmap.Coord
import domain.train.bitmap.Field
import domain.train.car.CarType
import io.kotest.core.spec.style.BehaviorSpec
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ApplyReservationsStepTest : BehaviorSpec({

    given("A trip from A to K with all seats enabled") {
        val trainTrip = TestTrainTripFactory.createTrainTrip(
            trainId = "2709-1729",
            trainName = "SUPER-FAST-TRAIN",
            charRange = ('A'..'K')
        )

        `when`("There is reservation with two legs with different seats") {
            val carId = 1

            val firstSeatSegmentsRange = (0..1)
            val firstSeatCoord = Coord(1, 1)
            val firstLegSeatId = AbsoluteSeatId(carId, firstSeatCoord)

            val secondSeatSegmentsRange = (2..4)
            val secondSeatCoord = Coord(1, 2)
            val secondLegSeatId = AbsoluteSeatId(carId, secondSeatCoord)

            val reservation =
                buildReservation(
                    trainTrip,
                    listOf(firstLegSeatId to firstSeatSegmentsRange, secondLegSeatId to secondSeatSegmentsRange)
                )

            val applyReservationsStep = ApplyReservationsStep(listOf(reservation))
            val result = applyReservationsStep(listOf(trainTrip))

            then("Result has 1 trip") {
                assertEquals(1, result.size)
            }

            and("Given seats are disabled only on given segments") {
                assertSeatEnabledOnGivenSegments(result, carId, firstSeatCoord, firstSeatSegmentsRange)
                assertSeatEnabledOnGivenSegments(result, carId, secondSeatCoord, secondSeatSegmentsRange)
            }

            and("Remaining seats are enabled") {
                val remainingSeats = result[0].segments.flatMap { segment ->
                    segment.train.cars.flatMap { car ->
                        car.fieldMatrix.flatten().filterIsInstance<Field.Seat>().filter {
                            val isNotFirstSeat = it.coord != firstSeatCoord
                            val isNotSecondSeat = it.coord != secondSeatCoord

                            car.id != carId || (isNotFirstSeat && isNotSecondSeat)
                        }
                    }
                }

                remainingSeats.forEach { seat ->
                    assertTrue(seat.enabled, "Seat[${seat.coord.x}, ${seat.coord.y}] should be enabled!")
                }
            }
        }
    }
})

private fun assertSeatEnabledOnGivenSegments(
    result: List<TrainTrip>,
    carId: Int,
    seatCoord: Coord,
    seatSegmentsRange: IntRange
) {
    result[0].segments.forEachIndexed { index, segment ->
        val car = segment.train.cars.find { it.id == carId }!!
        val seat = car.fieldMatrix[seatCoord.x][seatCoord.y] as Field.Seat

        val shouldBeEnabled = index !in seatSegmentsRange
        assertEquals(shouldBeEnabled, seat.enabled, "Seat[${seatCoord.x}, ${seatCoord.y}] is not $shouldBeEnabled")
    }
}

fun buildReservation(trainTrip: TrainTrip, seatsToDisable: List<Pair<AbsoluteSeatId, IntRange>>): Journey {
    val legs = seatsToDisable.map { (seatId, segmentsRange) ->
        val stopsRange = segmentsRange.first..segmentsRange.last + 1
        val (carId, seatCoord) = seatId
        val seat =
            trainTrip.segments[0].train.cars.find { it.id == carId }!!.fieldMatrix[seatCoord.x][seatCoord.y] as Field.Seat

        JourneyLeg(
            trainName = trainTrip.trainName,
            trainNumber = trainTrip.trainId,
            segments = emptyList(),
            stops = stopsRange.map {
                val stop = trainTrip.stopTimes[it]
                LegStop(stop.departureTime, stop.arrivalTime, stop.stop.name)
            },
            seats = listOf(BookedSeat(carId, seat.number, CarType.COMPARTMENT_FIRST_CLASS))
        )
    }

    return Journey(legs)
}