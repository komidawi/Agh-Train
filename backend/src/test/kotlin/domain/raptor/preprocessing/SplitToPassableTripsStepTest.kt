package domain.raptor.preprocessing

import domain.raptor.SegmentIndex
import domain.raptor.TestTrainTripFactory.createTrainTrip
import domain.raptor.TrainTrip
import domain.train.bitmap.Field
import io.kotest.core.spec.style.BehaviorSpec
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class PassableTripSplitterTest : BehaviorSpec({

    given("A single train trip without blocked segments") {
        val trainTrip = createTrainTrip(trainId = "2709-1729", trainName = "SUPER-FAST-TRAIN", charRange = ('A'..'G'))

        `when`("Splitting by blocked segments") {
            val passableTripsSplitStep = SplitToPassableTripsStep()
            val result = passableTripsSplitStep(listOf(trainTrip))

            then("The trip remains unchanged") {
                val expectedTripsSize = 1
                assertEquals(expectedTripsSize, result.size)

                assertEquals(trainTrip.trainId, result[0].trainId)
                assertEquals(trainTrip.trainName, result[0].trainName)
                assertEquals(trainTrip.stopTimes, result[0].stopTimes)
                assertEquals(trainTrip.segments, result[0].segments)
            }
        }
    }

    given("A single train trip with first blocked segment") {
        val trainTrip = createTrainTrip(trainId = "2709-1729", trainName = "SUPER-FAST-TRAIN", charRange = ('A'..'G'))
        blockSegment(SegmentIndex(0), trainTrip)

        `when`("Splitting by blocked segments") {
            val passableTripsSplitStep = SplitToPassableTripsStep()
            val result = passableTripsSplitStep(listOf(trainTrip))

            then("The first segment is cut") {
                val expectedTripsSize = 1
                assertEquals(expectedTripsSize, result.size)

                assertEquals(trainTrip.trainId, result[0].trainId)
                assertEquals(trainTrip.trainName, result[0].trainName)

                val expectedStopTimes = trainTrip.stopTimes.subList(1, trainTrip.stopTimes.size)
                assertEquals(expectedStopTimes, result[0].stopTimes)

                val expectedSegments = trainTrip.segments.subList(1, trainTrip.segments.size)
                assertEquals(expectedSegments, result[0].segments)
            }
        }
    }

    given("A single train trip with two first blocked segments") {
        val trainTrip = createTrainTrip(trainId = "2709-1729", trainName = "SUPER-FAST-TRAIN", charRange = ('A'..'G'))

        (0..1).forEach { index ->
            blockSegment(SegmentIndex(index), trainTrip)
        }

        `when`("Splitting by blocked segments") {
            val passableTripsSplitStep = SplitToPassableTripsStep()
            val result = passableTripsSplitStep(listOf(trainTrip))

            then("The two first segments are cut") {
                val expectedTripsSize = 1
                assertEquals(expectedTripsSize, result.size)

                assertEquals(trainTrip.trainId, result[0].trainId)
                assertEquals(trainTrip.trainName, result[0].trainName)

                val expectedStopTimes = trainTrip.stopTimes.subList(2, trainTrip.stopTimes.size)
                assertEquals(expectedStopTimes, result[0].stopTimes)

                val expectedSegments = trainTrip.segments.subList(2, trainTrip.segments.size)
                assertEquals(expectedSegments, result[0].segments)
            }
        }
    }

    given("A single train trip with first and third blocked segments") {
        val trainTrip = createTrainTrip(trainId = "2709-1729", trainName = "SUPER-FAST-TRAIN", charRange = ('A'..'G'))

        blockSegment(SegmentIndex(0), trainTrip)
        blockSegment(SegmentIndex(2), trainTrip)

        `when`("Splitting by blocked segments") {
            val passableTripsSplitStep = SplitToPassableTripsStep()
            val result = passableTripsSplitStep(listOf(trainTrip))

            then("Trip is split into two sub-trips") {
                val expectedTripsSize = 2
                assertEquals(expectedTripsSize, result.size)
            }

            then("The first sub-trip contains only the second segment") {
                assertEquals(trainTrip.trainId, result[0].trainId)
                assertEquals(trainTrip.trainName, result[0].trainName)

                val expectedStopTimes = trainTrip.stopTimes.subList(1, 3)
                assertEquals(expectedStopTimes, result[0].stopTimes)

                val expectedSegments = trainTrip.segments.subList(1, 2)
                assertEquals(expectedSegments, result[0].segments)
            }

            then("The second sub-trip contains segments from fourth to Nth") {
                assertEquals(trainTrip.trainId, result[1].trainId)
                assertEquals(trainTrip.trainName, result[1].trainName)

                val expectedStopTimes = trainTrip.stopTimes.subList(3, trainTrip.stopTimes.size)
                assertEquals(expectedStopTimes, result[1].stopTimes)

                val expectedSegments = trainTrip.segments.subList(3, trainTrip.segments.size)
                assertEquals(expectedSegments, result[1].segments)
            }
        }
    }

    given("A single train trip with first and fourth blocked segments") {
        val trainTrip = createTrainTrip(trainId = "2709-1729", trainName = "SUPER-FAST-TRAIN", charRange = ('A'..'G'))

        blockSegment(SegmentIndex(0), trainTrip)
        blockSegment(SegmentIndex(3), trainTrip)

        `when`("Splitting by blocked segments") {
            val passableTripsSplitStep = SplitToPassableTripsStep()
            val result = passableTripsSplitStep(listOf(trainTrip))

            then("Trip is split into two sub-trips") {
                val expectedTripsSize = 2
                assertEquals(expectedTripsSize, result.size)
            }

            then("The first sub-trip contains second and third segments") {
                assertEquals(trainTrip.trainId, result[0].trainId)
                assertEquals(trainTrip.trainName, result[0].trainName)

                val expectedStopTimes = trainTrip.stopTimes.subList(1, 4)
                assertEquals(expectedStopTimes, result[0].stopTimes)

                val expectedSegments = trainTrip.segments.subList(1, 3)
                assertEquals(expectedSegments, result[0].segments)
            }

            then("The second sub-trip contains segments from fifth to Nth") {
                assertEquals(trainTrip.trainId, result[1].trainId)
                assertEquals(trainTrip.trainName, result[1].trainName)

                val expectedStopTimes = trainTrip.stopTimes.subList(4, trainTrip.stopTimes.size)
                assertEquals(expectedStopTimes, result[1].stopTimes)

                val expectedSegments = trainTrip.segments.subList(4, trainTrip.segments.size)
                assertEquals(expectedSegments, result[1].segments)
            }
        }
    }

    given("A single train trip with first and last blocked segments") {
        val trainTrip = createTrainTrip(trainId = "2709-1729", trainName = "SUPER-FAST-TRAIN", charRange = ('A'..'G'))

        blockSegment(SegmentIndex(0), trainTrip)
        blockSegment(SegmentIndex(trainTrip.segments.lastIndex), trainTrip)

        `when`("Splitting by blocked segments") {
            val passableTripsSplitStep = SplitToPassableTripsStep()
            val result = passableTripsSplitStep(listOf(trainTrip))

            then("Trip is split into single sub-trip") {
                val expectedTripsSize = 1
                assertEquals(expectedTripsSize, result.size)
            }

            then("The sub-trip contains all segments except first and last") {
                assertEquals(trainTrip.trainId, result[0].trainId)
                assertEquals(trainTrip.trainName, result[0].trainName)

                val expectedStopTimes = trainTrip.stopTimes.subList(1, trainTrip.stopTimes.lastIndex)
                assertEquals(expectedStopTimes, result[0].stopTimes)

                val expectedSegments = trainTrip.segments.subList(1, trainTrip.segments.lastIndex)
                assertEquals(expectedSegments, result[0].segments)
            }
        }
    }

    given("A single train trip with all blocked segments") {
        val trainTrip = createTrainTrip(trainId = "2709-1729", trainName = "SUPER-FAST-TRAIN", charRange = ('A'..'G'))

        (0..trainTrip.segments.lastIndex).forEach { index ->
            blockSegment(SegmentIndex(index), trainTrip)
        }

        `when`("Splitting by blocked segments") {
            val passableTripsSplitStep = SplitToPassableTripsStep()
            val result = passableTripsSplitStep(listOf(trainTrip))

            then("The result does not contain any trip") {
                assertTrue(result.isEmpty())
            }
        }
    }
})

fun blockSegment(segmentIndex: SegmentIndex, trip: TrainTrip) {
    trip.segments[segmentIndex.value].train.cars.forEach { car ->
        car.fieldMatrix.flatten().forEach { field ->
            if (field is Field.Seat) {
                field.enabled = false
            }
        }
    }
}