package domain.raptor

import domain.raptor.TestTrainTripFactory.createTrainTrip
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldNotBeSameInstanceAs
import kotlin.test.assertEquals

internal class TrainTripTest : BehaviorSpec({
    val asValueType = " should act as value type";
    given("List of trips") {
        val trainTrip = createTrainTrip(trainId = "2709-1729", trainName = "SUPER-FAST-TRAIN", charRange = ('A'..'G'))
        val listOfTrain = listOf(trainTrip)
        `when`("Copy is performed") {
            val copy = listOfTrain.copy()
            then("Collections $asValueType") {
                listOfTrain shouldBe copy
                listOfTrain shouldNotBeSameInstanceAs copy
            }
            then("Train trip $asValueType") {
                val firstTrain = listOfTrain.first()
                val firstTrainCopy = copy.first()
                firstTrain shouldBe firstTrainCopy
                firstTrain shouldNotBeSameInstanceAs firstTrainCopy

                then("Segments $asValueType") {
                    val segments = listOfTrain.first().segments
                    val segmentsCopy = copy.first().segments
                    segments shouldBe segmentsCopy
                    segments shouldNotBeSameInstanceAs segmentsCopy

                    then("Segment $asValueType") {
                        val segment = segments.first()
                        val segmentCopy = segmentsCopy.first()
                        segment shouldBe segmentCopy
                        segment shouldNotBeSameInstanceAs segmentCopy
                        then("Train $asValueType") {
                            val train = segment.train
                            val trainCopy = segmentCopy.train
                            train shouldBe trainCopy
                            train shouldNotBeSameInstanceAs trainCopy
                            then("Cars $asValueType") {
                                val cars = train.cars
                                val carsCopy = trainCopy.cars
                                cars shouldBe carsCopy
                                cars shouldNotBeSameInstanceAs carsCopy
                                then("Car $asValueType") {
                                    val car = cars.first()
                                    val carCopy = carsCopy.first()
                                    car shouldBe carCopy
                                    car shouldNotBeSameInstanceAs carCopy
                                    then("FieldMatrix $asValueType") {
                                        val fieldMatrix = car.fieldMatrix
                                        val fieldMatrixCopy = carCopy.fieldMatrix
                                        fieldMatrix shouldBe fieldMatrixCopy
                                        fieldMatrix shouldNotBeSameInstanceAs fieldMatrixCopy
                                        then("Field $asValueType") {
                                            val field = fieldMatrix.first().first()
                                            val fieldCopy = fieldMatrixCopy.first().first()
                                            field shouldBe fieldCopy
                                            field shouldNotBeSameInstanceAs fieldCopy
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


        }
    }

    given("A valid train trip") {
        val trainTrip = createTrainTrip(trainId = "2709-1729", trainName = "SUPER-FAST-TRAIN", charRange = ('A'..'G'))

        `when`("Trying to get sub-trip for valid range of segments") {
            val result = trainTrip.getSubTripBetween(first = SegmentIndex(0), lastExclusive = SegmentIndex(2))

            then("Sub-trip contains 3 consecutive stops") {
                val expectedSubTripStopsSize = 3
                assertEquals(expectedSubTripStopsSize, result.stopTimes.size)
            }

            then("Consecutive stops are taken correctly") {
                val expectedStopTimes = trainTrip.stopTimes.subList(0, 3)
                assertEquals(expectedStopTimes, result.stopTimes)
            }

            then("Sub-trip contains 2 segments") {
                val expectedSubTripSegmentsSize = 2
                assertEquals(expectedSubTripSegmentsSize, result.segments.size)
            }

            then("Segments are taken correctly") {
                val expectedSubTripSegments = trainTrip.segments.subList(0, 2)
                assertEquals(expectedSubTripSegments, result.segments)
            }
        }

        `when`("Trying to get stops in valid range") {
            val result = trainTrip.getStopTimesBetween(source = TrainStop("A"), destination = TrainStop("D"))

            then("Consecutive stops are returned") {
                val expectedStopTimesSize = 4
                assertEquals(expectedStopTimesSize, result.size)

                val expectedStopTimes = trainTrip.stopTimes.subList(0, 4)
                assertEquals(expectedStopTimes, result)
            }
        }

        `when`("Trying to get stops between invalid start stop and valid end stop") {
            then("Exception is thrown") {
                shouldThrow<IllegalArgumentException> {
                    trainTrip.getStopTimesBetween(source = TrainStop("H"), destination = TrainStop("A"))
                }
            }
        }

        `when`("Trying to get stops between valid start stop and invalid end stop") {
            then("Exception is thrown") {
                shouldThrow<IllegalArgumentException> {
                    trainTrip.getStopTimesBetween(source = TrainStop("A"), destination = TrainStop("H"))
                }
            }
        }

        `when`("Trying to get stops, when end stop is before start stop") {
            then("Exception is thrown") {
                shouldThrow<IllegalArgumentException> {
                    trainTrip.getStopTimesBetween(source = TrainStop("C"), destination = TrainStop("A"))
                }
            }
        }

        `when`("Trying to get sub-trip and start segment index is less than zero") {
            then("Exception is thrown") {
                shouldThrow<IllegalArgumentException> {
                    trainTrip.getSubTripBetween(first = SegmentIndex(-3), lastExclusive = SegmentIndex(3))
                }
            }
        }

        `when`("Trying to get sub-trip and exclusive end segment index is greater than number of segments") {
            then("Exception is thrown") {
                shouldThrow<IllegalArgumentException> {
                    trainTrip.getSubTripBetween(first = SegmentIndex(0), lastExclusive = SegmentIndex(10))
                }
            }
        }

        `when`("Trying to get sub-trip and first segment is less equal to exclusive last index") {
            then("Exception is thrown") {
                shouldThrow<IllegalArgumentException> {
                    trainTrip.getSubTripBetween(first = SegmentIndex(1), lastExclusive = SegmentIndex(1))
                }
            }
        }
    }
})