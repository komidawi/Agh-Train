package domain.raptor

import domain.historical.Info
import domain.historical.Root
import domain.historical.Schedule
import java.text.SimpleDateFormat
import java.util.*

fun Date.addMinutes(minutesCount: Int): Date {
    return add(Calendar.MINUTE, minutesCount)
}

fun Date.addDays(daysCount: Int): Date {
    return add(Calendar.DAY_OF_MONTH, daysCount)
}

fun Date.add(unit: Int, count: Int): Date {
    val calendar = Calendar.getInstance()
    calendar.time = this
    calendar.add(unit, count)

    return calendar.time
}

fun Root.shiftedByDays(daysCount: Int): Root {
    return Root(
        train_id = train_id,
        train_name = train_name,
        schedules = schedules.map { (scheduleDate, scheduleId, info) ->
            Schedule(
                schedule_date = scheduleDate?.addDays(daysCount),
                schedule_id = scheduleId,
                info = info.map { (arrivalTime, stationName, departureTime, departureDelay, arrivalDelay) ->
                    Info(
                        arrivalTime?.addDays(daysCount),
                        stationName,
                        departureTime?.addDays(daysCount),
                        departureDelay,
                        arrivalDelay
                    )
                }
            )
        }
    )
}

val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
val baseScheduleDate: Date = dateFormat.parse("2020-08-23 00:00:00")
val baseStopDate: Date = dateFormat.parse("2020-08-23 08:45:00")

val sampleTrain = Root(
    train_id = 2709,
    train_name = "Sample train",
    schedules = listOf(
        Schedule(
            schedule_date = baseScheduleDate,
            schedule_id = 1,
            info = listOf(
                Info(
                    arrival_delay = null,
                    arrival_time = null,
                    departure_delay = 0,
                    departure_time = baseStopDate,
                    station_name = "Warszawa Zachodnia"
                ),
                Info(
                    arrival_delay = 0,
                    arrival_time = baseStopDate.addMinutes(4),
                    departure_delay = 0,
                    departure_time = baseStopDate.addMinutes(10),
                    station_name = "Warszawa Centralna"
                ),
                Info(
                    arrival_delay = 0,
                    arrival_time = baseStopDate.addMinutes(16),
                    departure_delay = 0,
                    departure_time = baseStopDate.addMinutes(19),
                    station_name = "Warszawa Wschodnia"
                ),
                Info(
                    arrival_delay = 0,
                    arrival_time = baseStopDate.addMinutes(24),
                    departure_delay = 0,
                    departure_time = baseStopDate.addMinutes(28),
                    station_name = "Tłuszcz"
                )
            )
        )
    )
)

val fasterTrain = Root(
    train_id = 2798,
    train_name = "Faster train",
    schedules = listOf(
        Schedule(
            schedule_date = baseScheduleDate,
            schedule_id = 1,
            info = listOf(
                Info(
                    arrival_delay = null,
                    arrival_time = null,
                    departure_delay = 0,
                    departure_time = baseStopDate.addMinutes(1),
                    station_name = "Warszawa Zachodnia"
                ),
                Info(
                    arrival_delay = 0,
                    arrival_time = baseStopDate.addMinutes(2),
                    departure_delay = 0,
                    departure_time = baseStopDate.addMinutes(3),
                    station_name = "Warszawa Centralna"
                ),
                Info(
                    arrival_delay = 0,
                    arrival_time = baseStopDate.addMinutes(10),
                    departure_delay = 0,
                    departure_time = baseStopDate.addMinutes(15),
                    station_name = "Warszawa Wschodnia"
                ),
                Info(
                    arrival_delay = 0,
                    arrival_time = baseStopDate.addMinutes(19),
                    departure_delay = 0,
                    departure_time = baseStopDate.addMinutes(24),
                    station_name = "Tłuszcz"
                )
            )
        )
    )
)

val fasterTrainOnlyOnPartOfJourney = Root(
    train_id = 9827,
    train_name = "Faster train but only on part of journey",
    schedules = listOf(
        Schedule(
            schedule_date = baseScheduleDate,
            schedule_id = 1,
            info = listOf(
                Info(
                    arrival_delay = null,
                    arrival_time = null,
                    departure_delay = 0,
                    departure_time = baseStopDate.addMinutes(-15),
                    station_name = "Warszawa Zachodnia"
                ),
                Info(
                    arrival_delay = 0,
                    arrival_time = baseStopDate.addMinutes(7),
                    departure_delay = 0,
                    departure_time = baseStopDate.addMinutes(8),
                    station_name = "Warszawa Centralna"
                ),
                Info(
                    arrival_delay = 0,
                    arrival_time = baseStopDate.addMinutes(10),
                    departure_delay = 0,
                    departure_time = baseStopDate.addMinutes(19),
                    station_name = "Warszawa Wschodnia"
                ),
                Info(
                    arrival_delay = 0,
                    arrival_time = baseStopDate.addMinutes(24),
                    departure_delay = 0,
                    departure_time = baseStopDate.addMinutes(28),
                    station_name = "Tłuszcz"
                )
            )
        )
    )
)