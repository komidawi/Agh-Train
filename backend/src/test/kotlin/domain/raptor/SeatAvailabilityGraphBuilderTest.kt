package domain.raptor

import domain.raptor.SeatAvailabilityGraphBuilder.Companion.ephemeralStartVertex
import domain.train.bitmap.Coord
import io.kotest.core.spec.style.BehaviorSpec
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class SeatAvailabilityGraphBuilderTest : BehaviorSpec({

    given("Simple consecutive availability ranges for segments [0, 6]") {
        val availabilityRanges = mapOf(
            AbsoluteSeatId(1, Coord(1, 1)) to listOf(SeatRange(0, 1)),
            AbsoluteSeatId(2, Coord(2, 2)) to listOf(SeatRange(2, 3)),
            AbsoluteSeatId(3, Coord(3, 3)) to listOf(SeatRange(4, 5)),
            AbsoluteSeatId(4, Coord(4, 4)) to listOf(SeatRange(6, 6)),
        )

        `when`("Availability graph is built") {
            val graphBuilder = SeatAvailabilityGraphBuilder(availabilityRanges)
            val result = graphBuilder.buildGraph()

            then("Graph contains ephemeral start vertex") {
                assertTrue(result.containsVertex(ephemeralStartVertex))
            }

            then("Ephemeral start is connected with real start") {
                val realStartVertex = Pair(AbsoluteSeatId(1, Coord(1, 1)), SeatRange(0, 1))
                assertTrue(result.containsEdge(ephemeralStartVertex, realStartVertex))
            }

            then("Graph contains vertices for all ranges") {
                availabilityRanges.forEach { (absoluteSeatId, ranges) ->
                    ranges.forEach { range ->
                        val expectedVertexId = Pair(absoluteSeatId, range)
                        assertTrue(result.containsVertex(expectedVertexId))
                    }
                }
            }

            then("Path between consecutive ranges exists") {
                availabilityRanges.toList().zipWithNext().forEach { (previous, next) ->
                    val (previousId, previousRanges) = previous
                    val previousVertex = Pair(previousId, previousRanges.first())

                    val (nextId, nextRanges) = next
                    val nextVertex = Pair(nextId, nextRanges.first())

                    assertTrue(result.containsEdge(previousVertex, nextVertex))
                }
            }
        }
    }

    given("Two overlapping ranges, when one extends another") {
        val availabilityRanges = mapOf(
            AbsoluteSeatId(1, Coord(1, 1)) to listOf(SeatRange(0, 2)),
            AbsoluteSeatId(2, Coord(2, 2)) to listOf(SeatRange(1, 5)),
        )

        `when`("Availability graph is built") {
            val graphBuilder = SeatAvailabilityGraphBuilder(availabilityRanges)
            val result = graphBuilder.buildGraph()

            val sourceVertex = AbsoluteSeatId(1, Coord(1, 1)) to SeatRange(0, 2)
            val destinationVertex = AbsoluteSeatId(2, Coord(2, 2)) to SeatRange(1, 5)

            then("Connection between ranges exists and extension is the destination vertex") {
                assertTrue(result.containsEdge(sourceVertex, destinationVertex))
            }

            then("Reversed connection does not exist") {
                assertFalse(result.containsEdge(destinationVertex, sourceVertex))
            }
        }
    }

    given("Two identical ranges starting at first segment") {
        val availabilityRanges = mapOf(
            AbsoluteSeatId(1, Coord(1, 1)) to listOf(SeatRange(0, 2)),
            AbsoluteSeatId(2, Coord(2, 2)) to listOf(SeatRange(0, 2)),
        )

        `when`("Availability graph is built") {
            val graphBuilder = SeatAvailabilityGraphBuilder(availabilityRanges)
            val result = graphBuilder.buildGraph()

            val firstVertex = Pair(AbsoluteSeatId(1, Coord(1, 1)), SeatRange(0, 2))
            val secondVertex = Pair(AbsoluteSeatId(2, Coord(2, 2)), SeatRange(0, 2))

            then("Ephemeral start is connected to both of them") {
                assertTrue(result.containsEdge(ephemeralStartVertex, firstVertex))
                assertTrue(result.containsEdge(ephemeralStartVertex, secondVertex))
            }

            then("Connection between them does not exist") {
                assertFalse(result.containsEdge(firstVertex, secondVertex))
                assertFalse(result.containsEdge(secondVertex, firstVertex))
            }
        }
    }
})