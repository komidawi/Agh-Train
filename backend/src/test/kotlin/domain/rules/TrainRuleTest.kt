package domain.rules

import domain.raptor.TestTrainTripFactory.createTrainTrip
import domain.raptor.TrainTrip
import domain.raptor.preprocessing.ApplyRulesStep
import domain.rules.train.TrainRule
import domain.train.bitmap.Field
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

internal class TrainRuleTest : StringSpec({

    "Disable single seat" {
        // given
        val trainId = "1130"
        val carId = 1
        val disabledSeat = 11

        val disabledSeatsRule = TrainRule.DisabledSeats(trainId, carId, listOf(disabledSeat))

        val rules = Rules(listOf(disabledSeatsRule), emptyList())
        val trips = listOf(createTrainTrip(trainId = trainId, charRange = 'A'..'B'))

        // when
        val applyRulesStep = ApplyRulesStep(rules)
        val result = applyRulesStep(trips)

        // then
        assertSeatsAreDisabled(result, trainId, carId, disabledSeat)
    }
})

private fun assertSeatsAreDisabled(trips: List<TrainTrip>, trainId: String, carId: Int, disabledSeat: Int) {
    trips.forEach { trainTrip ->
        trainTrip.segments.forEach { segment ->
            segment.train.let {
                if (it.id == trainId) {
                    val car = it.cars.find { it.id == carId }!!
                    val seat =
                        car.fieldMatrix.flatten().filterIsInstance<Field.Seat>().find { it.number == disabledSeat }

                    seat!!.enabled shouldBe false
                }
            }
        }
    }
}