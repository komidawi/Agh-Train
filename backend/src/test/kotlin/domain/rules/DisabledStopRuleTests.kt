package domain.rules

import domain.raptor.TestTrainTripFactory
import domain.raptor.TrainTrip
import io.kotest.core.spec.style.BehaviorSpec
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class DisabledStopRuleTests : BehaviorSpec({

    given("Train trip with stops from A to E") {
        val trainTrips = createTrainTrips('A'..'E')

        and("Train trip with stops from B to F") {
            val twoTrainTrips = trainTrips + createTrainTrips('B'..'F')

            `when`("Stop C is disabled") {
                val disabledRule = GlobalRule.DisabledStop("C")
                val result = disabledRule.apply(twoTrainTrips)

                then("We got back two trips") {
                    val expectedTripsCount = 2
                    assertEquals(expectedTripsCount, result.size)
                }

                then("Neither first nor second trip contains stop C") {
                    assertFalse(result.anyTripContainsStop("C"))
                }

                then("Returned trips do not contain any segment with stop C") {
                    assertFalse(result.anyTripContainsSegmentWithStop("C"))
                }
            }
        }

        `when`("Stop C is disabled") {
            val disabledRule = GlobalRule.DisabledStop("C")
            val result = disabledRule.apply(trainTrips)

            then("Single trip is returned") {
                val expectedTripsCount = 1
                assertEquals(expectedTripsCount, result.size)
            }

            then("Returned trip contains stops A-B-D-E") {
                val expectedStops = listOf("A", "B", "D", "E")
                assertEquals(expectedStops.size, result[0].stopTimes.size)

                val resultStops = result[0].stopTimes.map { it.stop.name }
                assertEquals(expectedStops, resultStops)
            }

            then("Returned trip contains segments A-B, B-D, D-E") {
                val expectedSegments = listOf("A", "B", "D", "E").zipWithNext()
                assertEquals(expectedSegments.size, result[0].segments.size)

                val resultsSegments = result[0].segments.map { it.from.station_name!! to it.to.station_name!! }
                assertEquals(expectedSegments, resultsSegments)
            }
        }
    }

    given("Train trip with stops from B to C") {
        val trainTrips = createTrainTrips('B'..'C')

        `when`("Stop C is disabled") {
            val disabledRule = GlobalRule.DisabledStop("C")
            val result = disabledRule.apply(trainTrips)

            then("We aren't getting any trips") {
                assertTrue(result.isEmpty())
            }
        }
    }

    given("Train trip with stops from A to C") {
        val trainTrips = createTrainTrips('A'..'C')

        `when`("Stop C is disabled") {
            val disabledRule = GlobalRule.DisabledStop("C")
            val result = disabledRule.apply(trainTrips)

            then("Single trip is returned") {
                val expectedTripsCount = 1
                assertEquals(expectedTripsCount, result.size)
            }

            then("Returned trip contains stops A-B") {
                val expectedStops = listOf("A", "B")
                assertEquals(expectedStops.size, result[0].stopTimes.size)

                val resultStops = result[0].stopTimes.map { it.stop.name }
                assertEquals(expectedStops, resultStops)
            }

            then("Returned trip contains only segment A-B") {
                val expectedSegments = listOf("A" to "B")
                assertEquals(expectedSegments.size, result[0].segments.size)

                val resultsSegments = result[0].segments.map { it.from.station_name!! to it.to.station_name!! }
                assertEquals(expectedSegments, resultsSegments)
            }
        }

        `when`("Stop A is disabled") {
            val disabledRule = GlobalRule.DisabledStop("A")
            val result = disabledRule.apply(trainTrips)

            then("Single trip is returned") {
                val expectedTripsCount = 1
                assertEquals(expectedTripsCount, result.size)
            }

            then("Returned trip contains stops B-C") {
                val expectedStops = listOf("B", "C")
                assertEquals(expectedStops.size, result[0].stopTimes.size)

                val resultStops = result[0].stopTimes.map { it.stop.name }
                assertEquals(expectedStops, resultStops)
            }

            then("Returned trip contains only segment B-C") {
                val expectedSegments = listOf("B" to "C")
                assertEquals(expectedSegments.size, result[0].segments.size)

                val resultsSegments = result[0].segments.map { it.from.station_name!! to it.to.station_name!! }
                assertEquals(expectedSegments, resultsSegments)
            }
        }

        `when`("Non-existent stop is disabled") {
            val disabledRule = GlobalRule.DisabledStop("D")
            val result = disabledRule.apply(trainTrips)

            then("Single trip is returned") {
                val expectedTripsCount = 1
                assertEquals(expectedTripsCount, result.size)
            }

            then("Stops remain unchanged") {
                val expectedStops = listOf("A", "B", "C")
                assertEquals(expectedStops.size, result[0].stopTimes.size)

                val resultStops = result[0].stopTimes.map { it.stop.name }
                assertEquals(expectedStops, resultStops)
            }

            then("Segments remain unchanged") {
                val expectedSegments = listOf("A", "B", "C").zipWithNext()
                assertEquals(expectedSegments.size, result[0].segments.size)

                val resultsSegments = result[0].segments.map { it.from.station_name!! to it.to.station_name!! }
                assertEquals(expectedSegments, resultsSegments)
            }
        }
    }
})

private fun createTrainTrips(charRange: CharRange): List<TrainTrip> =
    listOf(TestTrainTripFactory.createTrainTrip(charRange = charRange))

private fun List<TrainTrip>.anyTripContainsStop(stopName: String): Boolean =
    any { trip -> trip.stopTimes.any { it.stop.name == stopName } }

private fun List<TrainTrip>.anyTripContainsSegmentWithStop(stopName: String): Boolean =
    any { trip -> trip.segments.any { it.from.station_name == stopName || it.to.station_name == stopName } }