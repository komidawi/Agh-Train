package domain.algorithm.data

import app.DI
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

internal class MappingServiceImplTest : StringSpec({

    val fieldMatrixFactory = DI.fieldMatrixFactory

    val input = """1,2,3,4
            |5,6,7,8
            |9,10,11,12"""
        .trimMargin()


    "Fields in matrix are arranged with Cartesian Coordinates" {
        // when
        val matrix = fieldMatrixFactory.map(input)

        // then
        val leftBottomCornerField = matrix[0][0]
        leftBottomCornerField.letterCode shouldBe "9"

        val rightTopCornerField = matrix[3][2]
        rightTopCornerField.letterCode shouldBe "4"

        val middleRightField = matrix[2][1]
        middleRightField.letterCode shouldBe "7"
    }
})