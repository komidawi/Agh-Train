package domain.algorithm

import app.DI
import app.DI.carFactory
import domain.rules.NotVisAVisApplier
import domain.train.bitmap.Field
import domain.train.car.CarType
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldBeSameSizeAs
import io.kotest.matchers.shouldBe

class VisAVisDisablingTest : StringSpec({

    "WillDisableHorizontally"    {
        val data = "→SLW[11],T,←SRW[12],|,→SLW[21],T,←SRW[22],"
        val matrix = DI.fieldMatrixFactory.map(data);
        val ruleApplier = NotVisAVisApplier(2.toUInt());
        ruleApplier.apply(matrix);
        (matrix[0][0] as Field.Seat).enabled shouldBe true
        (matrix[2][0] as Field.Seat).enabled shouldBe false
        (matrix[4][0] as Field.Seat).enabled shouldBe true
        (matrix[6][0] as Field.Seat).enabled shouldBe false
    }

    "WillDisableInFirstDepartment"    {
        val fieldMatrix = carFactory.create(CarType.COMPARTMENT_FIRST_CLASS, carId = 1).fieldMatrix
        val ruleApplier = NotVisAVisApplier(2.toUInt())
        ruleApplier.apply(fieldMatrix)

        val allSeats = fieldMatrix.flatten().filterIsInstance<Field.Seat>()
        allSeats.filter { it.enabled } shouldBeSameSizeAs
                allSeats.filter { !it.enabled }

        val matrix = createEnableMatrix(
            ", ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ",
            ",O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X",
            ",O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X",
            ",O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X",
            ", ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ",
        )

        assertTestMatrixIsEnabledEqualToFieldMatrix(matrix, fieldMatrix)
    }

    "WillDisableInSecondDepartment"    {
        val fieldMatrix = carFactory.create(CarType.COMPARTMENT_SECOND_CLASS, carId = 2).fieldMatrix
        val ruleApplier = NotVisAVisApplier(2.toUInt())
        ruleApplier.apply(fieldMatrix)

        val allSeats = fieldMatrix.flatten().filterIsInstance<Field.Seat>()
        allSeats.filter { it.enabled } shouldBeSameSizeAs allSeats.filter { !it.enabled }

        val matrix = createEnableMatrix(
            ", ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,,",
            ",O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X",
            ",O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X",
            ",O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X",
            ",O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X",
            ", ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,,",
        )

        assertTestMatrixIsEnabledEqualToFieldMatrix(matrix, fieldMatrix)
    }
})

