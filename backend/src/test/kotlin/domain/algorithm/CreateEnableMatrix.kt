package domain.algorithm

import app.DI
import domain.train.bitmap.FieldFactoryImpl
import domain.train.bitmap.FieldMatrix
import domain.train.bitmap.FieldMatrixFactoryImpl
import domain.train.bitmap.FieldMatrixMapperImpl

fun createEnableMatrix(vararg data: String): FieldMatrix {
    val matrix = FieldMatrixFactoryImpl(
        DI.csvParser,
        FieldMatrixMapperImpl(
            FieldFactoryImpl(
                listOf(
                    TestEnabledFieldFactory()
                )
            )
        )
    ).map(data.reversed().joinToString("\n"));
    return matrix
}