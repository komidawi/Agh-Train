package domain.algorithm

import domain.train.bitmap.Coord
import domain.train.bitmap.Field
import domain.train.bitmap.FieldMapper
import domain.train.bitmap.getOptional

class TestEnabledFieldFactory : FieldMapper {
    private val disabledMap = mapOf(
        ('X' to false),
        ('O' to true),
    )

    override fun tryMap(description: String, coord: Coord): Field {
        val isEnabled = getOptional(disabledMap, description, null)
        return if (isEnabled != null)
            Field.Seat(
                description,
                1,
                Field.Seat.Location.CORRIDOR,
                coord,
                Field.Seat.Direction.RIGHT,
                enabled = isEnabled
            )
        else
            Field.UnknownField(description)
    }
}