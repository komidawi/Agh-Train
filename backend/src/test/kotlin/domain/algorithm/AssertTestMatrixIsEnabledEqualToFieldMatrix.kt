package domain.algorithm

import domain.train.bitmap.Field
import domain.train.bitmap.FieldMatrix
import io.kotest.matchers.shouldBe

fun assertTestMatrixIsEnabledEqualToFieldMatrix(
    testMatrix: FieldMatrix,
    fieldMatrix: FieldMatrix
) {
    testMatrix.forEachIndexed { y, row ->
        row.forEachIndexed { x, expected ->
            val current = fieldMatrix[y][x]
            if (expected is Field.Seat) {
                if (current is Field.Seat) {
                    current.enabled shouldBe expected.enabled
                } else {
                    throw IllegalArgumentException()
                }
            } else if (current is Field.Seat) {
                throw IllegalArgumentException()
            }
        }
    }
}

