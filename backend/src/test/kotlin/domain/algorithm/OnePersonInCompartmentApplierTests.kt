package domain.algorithm

import app.DI
import app.toJson
import domain.rules.OnePersonInCompartmentApplier
import domain.train.car.CarType
import io.kotest.core.spec.style.BehaviorSpec

class OnePersonInCompartmentApplierTests : BehaviorSpec({
    val simpleData = "→SLW[15],T,←SRW[16],|,→SLW[25],T,←SRW[26]\n" +
            "→SLW[11],T,←SRW[12],|,→SLW[21],T,←SRW[22]"
    val ruleApplier = OnePersonInCompartmentApplier()

    given(simpleData) {
        `when`(ruleApplier::class.simpleName!!) {
            val matrix = DI.fieldMatrixFactory.map(simpleData)
            ruleApplier.apply(matrix)
            val expected = createEnableMatrix(
                "O,,X,,O,,X",
                "X,,X,,X,,X",
            )
            then("should match ${expected.toJson()}") {
                assertTestMatrixIsEnabledEqualToFieldMatrix(matrix, expected)
            }
        }
    }

    given("First class compartment") {
        `when`(ruleApplier::class.simpleName!!) {
            val matrix = DI.carFactory.create(CarType.COMPARTMENT_FIRST_CLASS, carId = 1).fieldMatrix
            ruleApplier.apply(matrix)
            val expected = createEnableMatrix(
                ", ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,",
                ",O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,",
                ",X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,",
                ",X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,",
                ", ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,",
            )
            then("should match ${expected.toJson()}") {
                assertTestMatrixIsEnabledEqualToFieldMatrix(matrix, expected)
            }
        }
    }

    given("Second class compartment") {
        `when`(ruleApplier::class.simpleName!!) {
            val matrix = DI.carFactory.create(CarType.COMPARTMENT_SECOND_CLASS, carId = 2).fieldMatrix
            ruleApplier.apply(matrix)
            val expected = createEnableMatrix(
                ", ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,",
                ",O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,,O,,X,",
                ",X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,",
                ",X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,",
                ",X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,",
                ", ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,",
            )
            then("should match ${expected.toJson()}") {
                assertTestMatrixIsEnabledEqualToFieldMatrix(matrix, expected)
            }
        }
    }
})
