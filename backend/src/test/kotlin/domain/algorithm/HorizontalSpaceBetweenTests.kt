package domain.algorithm

import app.DI
import app.DI.carFactory
import domain.rules.HorizontalSpaceBetween
import domain.train.bitmap.Field
import domain.train.car.CarType
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

@kotlin.ExperimentalUnsignedTypes
class HorizontalSpaceBetweenTests : StringSpec({

    "WillDisableHorizontally"    {
        val data = "→SLW[15],T,←SRW[16],|,→SLW[25],T,←SRW[26]\n" +
                "→SLW[11],T,←SRW[12],|,→SLW[21],T,←SRW[22]"
        val matrix = DI.fieldMatrixFactory.map(data)
        val ruleApplier = HorizontalSpaceBetween(1.toUInt())
        ruleApplier.apply(matrix)
        (matrix[0][0] as Field.Seat).enabled shouldBe true
        (matrix[2][0] as Field.Seat).enabled shouldBe true
        (matrix[4][0] as Field.Seat).enabled shouldBe true
        (matrix[6][0] as Field.Seat).enabled shouldBe true
        (matrix[0][1] as Field.Seat).enabled shouldBe false
        (matrix[2][1] as Field.Seat).enabled shouldBe false
        (matrix[4][1] as Field.Seat).enabled shouldBe false
        (matrix[6][1] as Field.Seat).enabled shouldBe false
    }

    "WillDisableInFirstDepartment"    {
        val fieldMatrix = carFactory.create(CarType.COMPARTMENT_FIRST_CLASS, carId = 1).fieldMatrix
        val ruleApplier = HorizontalSpaceBetween(1.toUInt())
        ruleApplier.apply(fieldMatrix)
        val matrix = createEnableMatrix(
            ", ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ",
            ",O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O",
            ",X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X",
            ",O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O",
            ", ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ",
        )
        assertTestMatrixIsEnabledEqualToFieldMatrix(matrix, fieldMatrix)
    }

    "WillDisableInSecondDepartment"    {
        val fieldMatrix = carFactory.create(CarType.COMPARTMENT_SECOND_CLASS, carId = 1).fieldMatrix
        val ruleApplier = HorizontalSpaceBetween(2.toUInt())
        ruleApplier.apply(fieldMatrix)
        val matrix = createEnableMatrix(
            ", ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,,",
            ",O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O",
            ",X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X",
            ",X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X,,X",
            ",O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O,,O",
            ", ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,, ,,",
        )
        assertTestMatrixIsEnabledEqualToFieldMatrix(matrix, fieldMatrix)
    }
})

