package domain.algorithm

import app.DI
import domain.rules.LocationsSeatDisabledApplier
import domain.train.bitmap.Field
import domain.train.car.CarFactory
import domain.train.car.CarType
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class SeatDisablingTest : StringSpec({

    val carFactory: CarFactory = DI.carFactory

    "LocationsSeatDisabledRule for Middle Seats is applied" {
        // given
        val fieldMatrix = carFactory.create(carType = CarType.COMPARTMENT_FIRST_CLASS, carId = 1).fieldMatrix
        val ruleApplier = LocationsSeatDisabledApplier(setOf(Field.Seat.Location.MIDDLE))

        // when
        ruleApplier.apply(fieldMatrix)

        // then
        fieldMatrix.flatten().filter {
            it is Field.Seat && it.location == Field.Seat.Location.MIDDLE && it.enabled
        }.size shouldBe 0
    }
})


