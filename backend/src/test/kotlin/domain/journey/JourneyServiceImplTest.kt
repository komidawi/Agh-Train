package domain.journey

import domain.historical.TrainsLoader
import domain.journey.cost.JourneyCostCalculator
import domain.raptor.RaptorAlgorithmFactory
import domain.raptor.TrainTripFactory
import domain.raptor.preprocessing.TrainTripsProcessingStepsFactory
import domain.rules.Rules
import domain.user.orders.ReservationService
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.mockk.mockk
import java.time.format.DateTimeParseException
import kotlin.test.assertTrue

internal class JourneyServiceImplTest : BehaviorSpec({
    val trainsLoaderDummy = mockk<TrainsLoader>()
    val trainTripFactoryDummy = mockk<TrainTripFactory>()
    val raptorAlgorithmFactoryDummy = mockk<RaptorAlgorithmFactory>()
    val trainTripsProcessingStepsFactoryDummy = mockk<TrainTripsProcessingStepsFactory>()
    val reservationsServiceDummy = mockk<ReservationService>()
    val journeyCostCalculatorDummy = mockk<JourneyCostCalculator>()

    val rulesMock = mockk<Rules>()

    val isDirect = false
    val dateTimeFormatDifferentThanISO = "JAN-2020-02"
    val startDateTime = "2021-07-01T07:00:00.000Z"
    val endDateTime = "2021-08-14T22:00:00.000Z"
    val sourceStation = "Warszawa Zachodnia"
    val destinationStation = "Warszawa Wschodnia"

    given("Configured journey service implementation") {
        val journeyService = JourneyServiceImpl(
            trainsLoaderDummy,
            trainTripFactoryDummy,
            raptorAlgorithmFactoryDummy,
            trainTripsProcessingStepsFactoryDummy,
            reservationsServiceDummy,
            journeyCostCalculatorDummy
        )

        `when`("Start date time is not in ISO format") {
            val query = JourneyQuery(
                startTimeRange = dateTimeFormatDifferentThanISO,
                endTimeRange = endDateTime,
                startStation = sourceStation,
                endStation = destinationStation,
                isDirect = isDirect
            )

            then("Exception is thrown") {
                shouldThrow<DateTimeParseException> {
                    journeyService.findJourney(query, rulesMock)
                }
            }
        }

        `when`("End date time is not in ISO format") {
            val query = JourneyQuery(
                startTimeRange = startDateTime,
                endTimeRange = dateTimeFormatDifferentThanISO,
                startStation = sourceStation,
                endStation = destinationStation,
                isDirect = isDirect
            )

            then("Exception is thrown") {
                shouldThrow<DateTimeParseException> {
                    journeyService.findJourney(query, rulesMock)
                }
            }
        }

        `when`("Start date time is after end date time") {
            val query = JourneyQuery(
                startTimeRange = endDateTime,
                endTimeRange = startDateTime,
                startStation = sourceStation,
                endStation = destinationStation,
                isDirect = isDirect
            )

            then("Exception is thrown") {
                shouldThrow<IllegalArgumentException> {
                    journeyService.findJourney(query, rulesMock)
                }
            }
        }

        `when`("Source is the same station as destination") {
            val query = JourneyQuery(
                startTimeRange = startDateTime,
                endTimeRange = endDateTime,
                startStation = sourceStation,
                endStation = sourceStation,
                isDirect = isDirect
            )
            val queryResult = journeyService.findJourney(query, rulesMock)

            then("Empty Journeys object is returned") {
                assertTrue { queryResult.journeys.isEmpty() }
            }
        }
    }
})