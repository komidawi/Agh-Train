package domain.journey.cost

import domain.train.car.CarType
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe

internal class SegmentCostProviderImplTest : BehaviorSpec({

    given("Configured segment cost provider") {
        val segmentCostProvider = SegmentCostProviderImpl()

        `when`("Cost of known segment is queried") {
            val cost = segmentCostProvider(
                SegmentCostId(
                    source = "Białystok",
                    destination = "Łapy",
                    trainId = "2183",
                    carType = CarType.COMPARTMENT_FIRST_CLASS
                )
            )

            then("Expected cost is returned") {
                cost shouldBe 9.30
            }
        }

        `when`("Cost of unknown segment is queried") {
            val cost = segmentCostProvider(
                SegmentCostId(
                    source = "Kraków Główny",
                    destination = "Łapy",
                    trainId = "2183",
                    carType = CarType.COMPARTMENT_FIRST_CLASS
                )
            )

            then("Default cost is returned") {
                cost shouldBe 10.20
            }
        }
    }
})