package domain.journey.cost

import domain.journey.BookedSeat
import domain.journey.Journey
import domain.journey.JourneyLeg
import domain.journey.LegStop
import domain.train.car.CarType
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe

internal class JourneyCostCalculatorImplTest : BehaviorSpec({

    val firstClass = CarType.COMPARTMENT_FIRST_CLASS
    val costs = mapOf(
        SegmentCostId(source = "A", destination = "B", trainId = "2709", carType = firstClass) to 2.5,
        SegmentCostId(source = "B", destination = "C", trainId = "2709", carType = firstClass) to 3.5,
        SegmentCostId(source = "C", destination = "D", trainId = "7777", carType = firstClass) to 5.5
    )
    val segmentCostProviderStub = SegmentCostProvider { costs.getOrDefault(it, 10.0) }

    given("Configured journey cost calculator") {
        val journeyCostCalculator = JourneyCostCalculatorImpl(segmentCostProviderStub)

        `when`("Cost of journey is queried") {
            val journey = Journey(
                legs = listOf(
                    JourneyLeg(
                        trainName = "SuperTrain_1",
                        trainNumber = "2709",
                        segments = emptyList(),
                        stops = listOf(
                            LegStop(null, null, "A"),
                            LegStop(null, null, "B"),
                            LegStop(null, null, "C")
                        ),
                        seats = listOf(BookedSeat(1, 1, firstClass))
                    ),
                    JourneyLeg(
                        trainName = "SuperTrain_2",
                        trainNumber = "7777",
                        segments = emptyList(),
                        stops = listOf(
                            LegStop(null, null, "C"),
                            LegStop(null, null, "D"),
                            LegStop(null, null, "E")
                        ),
                        seats = listOf(BookedSeat(1, 1, firstClass))
                    )
                )
            )

            val cost = journeyCostCalculator(journey)

            then("Expected cost is returned") {
                cost shouldBe 21.5
            }
        }

    }
})