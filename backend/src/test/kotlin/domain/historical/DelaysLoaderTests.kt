import domain.historical.TrainsLoaderImpl
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.ints.shouldBeGreaterThan
import io.kotest.matchers.shouldBe
import java.time.LocalDateTime

class DelaysLoaderTests : StringSpec({
    val trainsLoader = TrainsLoaderImpl()

    "Loader works" {
        trainsLoader.accept(
            LocalDateTime.of(2000, 1, 1, 0, 0, 0),
            LocalDateTime.of(2020, 1, 1, 0, 0, 0)
        ).size shouldBeGreaterThan 0
    }

    "Loader load by date" {
        trainsLoader.accept(
            LocalDateTime.of(2000, 1, 1, 0, 0, 0),
            LocalDateTime.of(2021, 1, 1, 0, 0, 0)
        ).size shouldBe 24
    }

    "Loader2 will load multiple files" {
        trainsLoader.accept(
            LocalDateTime.of(2020, 12, 8, 0, 0, 0),
            LocalDateTime.of(2020, 12, 11, 0, 0, 0)
        ).size shouldBe 7
    }

    "Loader2 will load all" {
        trainsLoader.accept(
            LocalDateTime.of(2019, 12, 8, 0, 0, 0),
            LocalDateTime.of(2020, 12, 30, 0, 0, 0)
        ).size shouldBe 24
    }
})