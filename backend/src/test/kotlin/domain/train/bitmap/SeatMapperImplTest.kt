package domain.train.bitmap

import app.DI
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.shouldBe
import kotlin.test.assertEquals

internal class SeatMapperImplTest : StringSpec({

    val seatMapper = DI.seatMapperImpl

    "Seat contains power socket" {
        // given
        val description = "→SLW⏻[11]"
        val coord = Coord(1, 1)

        // when
        val mappedSeat = seatMapper.tryMap(description, coord) as Field.Seat

        // then
        mappedSeat.properties shouldContain Field.Seat.Property.POWER_SOCKET
    }

    "Seat is restaurant seat" {
        // given
        val description = "→SLW⏣[11]"
        val coord = Coord(1, 1)

        // when
        val mappedSeat = seatMapper.tryMap(description, coord) as Field.Seat

        // then
        mappedSeat.properties shouldContain Field.Seat.Property.RESTAURANT
    }

    "Seat has extended leg space" {
        // given
        val description = "→SLWɺ[11]"
        val coord = Coord(1, 1)

        // when
        val mappedSeat = seatMapper.tryMap(description, coord) as Field.Seat

        // then
        mappedSeat.properties shouldContain Field.Seat.Property.EXTENDED_LEG_SPACE
    }

    "Seat is adjusted for disabled people" {
        // given
        val description = "→SLW♿[11]"
        val coord = Coord(1, 1)

        // when
        val mappedSeat = seatMapper.tryMap(description, coord) as Field.Seat

        // then
        mappedSeat.properties shouldContain Field.Seat.Property.ADJUSTED_FOR_DISABLED
    }

    "Seat has expected coordinates" {
        // given
        val description = "→SLW♿[11]"
        val coord = Coord(1, 1)

        // when
        val mappedSeat = seatMapper.tryMap(description, coord) as Field.Seat

        // then
        assertEquals(mappedSeat.coord, coord)
    }

    "Seat number is properly retrieved" {
        // given
        val seatNumber = 25
        val description = "→SLW⏻[$seatNumber]"
        val coord = Coord(1, 1)

        // when
        val mappedSeat = seatMapper.tryMap(description, coord) as Field.Seat

        // then
        mappedSeat.number shouldBe seatNumber
    }
})